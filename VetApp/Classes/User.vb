﻿Imports MySql.Data.MySqlClient 'Imports the downloaded MySQL libraries
Imports System.Security.Cryptography 'Imports the Cryptography libraries
Imports System.Text 'Imports the system text libraries

Public Class User
    Dim MySQLConnection As New MySqlConnection(ConnectionString()) ' defines a new mysql connection with the Connectionstring from the function "Connectionstring"


    Public Sub SQLConn() 'Defines the sub

        Try 'Attempts to run the following code (error handling)
            MySQLConnection.Open() 'Opens a connection to the database using the string defined above
            MySQLConnection.Close() 'Closes the open SQL connection

        Catch ex As Exception ' If the connection generates an error, it runs the following

            MessageBox.Show("An error occured connecting to the database and a connection could not be established. This program may not function correctly until the connection is restored." + vbCrLf + vbCrLf + vbCrLf + "Error:" + vbCrLf + ex.Message, "Connection Error", MessageBoxButtons.OK, MessageBoxIcon.Error) 'Generates a messagebox with the error thrown by the program

        End Try 'Ends the "try"
    End Sub ' Ends the sub


    Public Function ConnectionString() 'Defines the Function

        Dim cs As String = String.Format("Server={0}; userid={1}; password={2}; database={3};Convert Zero Datetime=True", My.Settings.DBHostname, My.Settings.DBUsername, My.Settings.DBPassword, My.Settings.DBDatabase) 'Defines the connection string to be used to connect to the database, using the settings stored within the program

        Return cs 'Returns the Connectionstring
    End Function ' Ends the sub


    Public Sub DBConnTest(ByVal server As String, ByVal user As String, ByVal pass As String, ByVal db As String)

        MySQLConnection.ConnectionString = String.Format("Server={0}; userid={1}; password={2}; database={3};", server, user, pass, db)

        MySQLConnection.Open()
        MessageBox.Show("Test Success!", "Information")
        MySQLConnection.Close()
    End Sub


    Public Function Login(ByVal Email As String, ByVal Password As String)

        Dim OccurrenceCount As Integer = EmailExists(Email)

        If (OccurrenceCount = 0) Or (OccurrenceCount > 1) Then
            Return False
        Else
            MySQLConnection.Open() 'Opens a connection to the database using the string defined above



            Dim query As New MySqlCommand("SELECT UserID, Salt, Hash FROM User_Details WHERE Email=@email;", MySQLConnection)

            query.Parameters.AddWithValue("@email", Email)

            Dim UserID As Integer = Nothing
            Dim Salt As String = Nothing
            Dim StoredHash As String = Nothing

            Dim reader As MySqlDataReader = query.ExecuteReader()

            Do While reader.Read()
                UserID = reader.GetString(0)
                Salt = reader.GetString(1)
                StoredHash = reader.GetString(2)
            Loop
            reader.Close()
            MySQLConnection.Close()

            Dim calculatedHash As String = GenerateHash(Salt, Email, Password)


            If CalculatedHash = StoredHash Then
                LoginLogger(Email, True)
                Return UserID
            Else
                LoginLogger(Email, False)
                Return False
            End If
        End If


    End Function

    Public Function LoginTest(ByVal Email As String, ByVal Password As String)

        Dim OccurrenceCount As Integer = EmailExists(Email)

        If (OccurrenceCount = 0) Or (OccurrenceCount > 1) Then
            Return False
        Else
            MySQLConnection.Open() 'Opens a connection to the database using the string defined above



            Dim query As New MySqlCommand("SELECT UserID, Salt, Hash FROM User_Details WHERE Email=@email;", MySQLConnection)

            query.Parameters.AddWithValue("@email", Email)

            Dim UserID As Integer = Nothing
            Dim Salt As String = Nothing
            Dim StoredHash As String = Nothing

            Dim reader As MySqlDataReader = query.ExecuteReader()

            Do While reader.Read()
                UserID = reader.GetString(0)
                Salt = reader.GetString(1)
                StoredHash = reader.GetString(2)
            Loop
            reader.Close()
            MySQLConnection.Close()

            Dim calculatedHash As String = GenerateHash(Salt, Email, Password)


            If calculatedHash = StoredHash Then
                Return True
            Else
                Return False
            End If
        End If
    End Function





    Public Function GenerateSalt()

        Dim AvailableChars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+=][}{<>" 'Defines AvailableChars as a string, storing the given value
        Dim salt As String = Nothing 'Defines a variable "salt" as string, storing no text 
        Dim rand As New Random ' defines a new instance of the system class "Random"


        For i As Integer = 1 To 255 'For Loop, repeating 255 times. 255 = Length of the salt
            Dim x As Integer = rand.Next(0, AvailableChars.Length - 1) ' defines x as an integer, picking a random number between 0 and 80 (the number of characters in AvailableChars is 81)
            salt &= (AvailableChars.Substring(x, 1)) ' links the contents of the variable "salt" to a substring created from the string AvailableChars, using the index x, continuing for 1 character
        Next 'restarts the loop by returning to "For"

        Dim FullSalt As String = salt.ToString 'Defines the variable "FullSalt" as a string, storing the contents of "salt" converted to a string
        Return FullSalt 'Returns the salt
    End Function



    Public Function GenerateHash(ByVal Salt As String, ByVal Email As String, ByVal Password As String)

        Dim combinedString As String 'Defines the variable "combinedString" as a string

        combinedString = (Salt + Email.ToLower + Salt + Password + Salt) 'Combines the strings "Fullsalt", "Email" and "Password" to form a longer string, stored in the variable "combinedString"

        Dim sha As New SHA256CryptoServiceProvider 'Defines a new instance of the SHA256 Libraries
        Dim Hash() As Byte 'Defines the array "hash" with datatype Byte
        Dim bytes() As Byte = ASCIIEncoding.ASCII.GetBytes(combinedString) 'Creates byte array from combinedString

        Hash = sha.ComputeHash(bytes) 'Generates a hash using the byte array "Bytes", storing it in the array "Hash"

        Dim FinalHash As String = Nothing 'Defines a variable "finalHash" storing nothing to prevent an error below
        For Each bt As Byte In Hash 'For loop for each value in the Hash array
            FinalHash &= bt.ToString("x2") 'Converts BT to a string, with ("x2") printing the byte in hexadecimal format.
        Next 'Restarts the loop

        Return FinalHash 'Returns the final generated hash
    End Function




    Public Function ReverseDate(ByVal DOB As String)
        Dim year As String = DOB.Substring(6, 4)
        Dim month As String = DOB.Substring(3, 2)
        Dim day As String = DOB.Substring(0, 2)
        Return (year + "-" + month + "-" + day)
    End Function





    Public Sub ChangeUserPass(ByRef Email As String, ByRef Password As String)

        Dim salt As String = GenerateSalt()

        Dim hash As String = GenerateHash(salt, Email, Password)



        Dim command As New MySqlCommand("UPDATE User_Details SET Email=@email, Salt=@salt, Hash=@hash WHERE UserID = @id", MySQLConnection)
        command.Parameters.AddWithValue("@email", Email)
        command.Parameters.AddWithValue("@salt", salt)
        command.Parameters.AddWithValue("@hash", hash)
        command.Parameters.AddWithValue("@id", Globals.ModifyID)
        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()


        MessageBox.Show("Details have been updated")


    End Sub



    Public Function EmailExists(ByRef Email As String)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT count(UserID) from User_Details WHERE Email=@email", MySQLConnection)
        query.Parameters.AddWithValue("@email", Email)


        Dim count As Integer = Nothing


        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            count = reader.GetString(0)
        Loop
        reader.Close()
        MySQLConnection.Close()

        Return count
    End Function

    Public Function GetAL(ByRef UserID As Integer)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT AccessLevel from User_Details WHERE UserID=@id", MySQLConnection)
        query.Parameters.AddWithValue("@id", UserID)


        Dim AL As Integer = Nothing


        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            AL = reader.GetString(0)
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return AL
    End Function



    Private Function AWSIP()
        Using IPWebClient As New Net.WebClient
            Return (Encoding.ASCII.GetString(IPWebClient.DownloadData("http://checkip.amazonaws.com/"))).ToString
        End Using
    End Function


    Private Sub LoginLogger(ByVal Email As String, ByVal Success As Boolean)
        Dim UserIP As String = AWSIP()

        MySQLConnection.Open()
        Dim command As New MySqlCommand("INSERT INTO login_attempt_history (AttemptedEmail, OriginIPAddress, Timestamp, LoginSuccess) VALUES (@email, @ip, now(), @s);", MySQLConnection)
        command.Parameters.AddWithValue("@email", Email)
        command.Parameters.AddWithValue("@ip", UserIP)
        command.Parameters.AddWithValue("@s", Success)

        command.ExecuteNonQuery()
        MySQLConnection.Close()
    End Sub



    Public Function GetName(ByVal ID As Integer)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above



        Dim query As New MySqlCommand("SELECT FirstName FROM User_Details WHERE UserID=@id;", MySQLConnection)

        query.Parameters.AddWithValue("@id", ID)

        Dim FirstName As String = Nothing

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            FirstName = reader.GetString(0)
        Loop
        reader.Close()
        MySQLConnection.Close()

        Return (FirstName)
    End Function

    Public Function GetEmail()
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Email FROM User_Details WHERE UserID=@id;", MySQLConnection)

        query.Parameters.AddWithValue("@id", Globals.ModifyID)

        Dim Email As String = Nothing

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Email = reader.GetString(0)
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return (Email)
    End Function






    Public Function GetPetList() As DataTable
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above

        Dim MySqlDataAdapter As New MySqlDataAdapter(String.Format("SELECT PetID, PetName, Animal, Breed, MicrochipNumber, Deceased from Pets WHERE OwnerID={0}", Globals.UserID), MySQLConnection)

        Dim ds As New DataTable

        MySqlDataAdapter.Fill(ds)
        MySQLConnection.Close()
        Return (ds)

    End Function

    Public Function GetAnimal(ByRef Animal As Integer)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT AnimalType FROM Animals WHERE AnimalID=@id;", MySQLConnection)
        query.Parameters.AddWithValue("@id", Animal)
        Dim Name As String = Nothing
        Dim reader As MySqlDataReader = query.ExecuteReader()
        Do While reader.Read()
            Name = reader.GetString(0)
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return (Name)
    End Function

    Public Function GetBreed(ByRef Breed As Integer)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Breed FROM Breeds WHERE BreedID=@id;", MySQLConnection)
        query.Parameters.AddWithValue("@id", Breed)
        Dim Name As String = Nothing
        Dim reader As MySqlDataReader = query.ExecuteReader()
        Do While reader.Read()
            Name = reader.GetString(0)
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return (Name)
    End Function

    Public Function GetUserDetails()

        Dim Details(14) As String

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Email, Title, FirstName, LastName, DateOfBirth, HomePhone, MobilePhone, WorkPhone, AddressLine1, AddressLine2, City, County, Country, PostCode, AccessLevel, HasPets from User_Details WHERE UserID=@id", MySQLConnection)
        query.Parameters.AddWithValue("@id", Globals.ModifyID)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()

            For i = 0 To 14
                If reader.IsDBNull(i) Then
                    Details(i) = ""
                Else
                    Details(i) = reader.GetString(i)
                End If
            Next
        Loop
        reader.Close()
        MySQLConnection.Close()


        Return Details

    End Function


    Public Function GetPetDetails()

        Dim Details(7) As String

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT OwnerID, PetName, PetDateOfBirth, PetDescription, Animal, Breed, MicrochipNumber, Deceased from Pets WHERE PetID=@id", MySQLConnection)
        query.Parameters.AddWithValue("@id", Globals.PetID)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()

            For i = 0 To 7
                If reader.IsDBNull(i) Then
                    Details(i) = ""
                Else
                    Details(i) = reader.GetString(i)
                End If
            Next
        Loop
        reader.Close()
        MySQLConnection.Close()

        Return Details

    End Function



    Public Sub UpdateMyAccount(ByVal Details() As String)
        Dim command As New MySqlCommand("UPDATE User_Details SET Title=@title, FirstName=@fname, LastName=@lname, DateOfBirth=@dob, HomePhone=@hp, MobilePhone=@mp, WorkPhone=@wp, AddressLine1=@al1, AddressLine2=@al2, City=@city, County=@county, Country=@country, PostCode=@pc WHERE UserID = @id", MySQLConnection)

        command.Parameters.AddWithValue("@title", Details(0))
        command.Parameters.AddWithValue("@fname", Details(1))
        command.Parameters.AddWithValue("@lname", Details(2))
        command.Parameters.AddWithValue("@dob", Details(3))
        command.Parameters.AddWithValue("@hp", Details(4))
        command.Parameters.AddWithValue("@mp", Details(5))
        command.Parameters.AddWithValue("@wp", Details(6))
        command.Parameters.AddWithValue("@al1", Details(7))
        command.Parameters.AddWithValue("@al2", Details(8))
        command.Parameters.AddWithValue("@city", Details(9))
        command.Parameters.AddWithValue("@county", Details(10))
        command.Parameters.AddWithValue("@country", Details(11))
        command.Parameters.AddWithValue("@pc", Details(12))
        command.Parameters.AddWithValue("@id", Globals.ModifyID)

        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()

    End Sub


    Public Function GetAnimalList()

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT AnimalType from Animals", MySQLConnection)
        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("AnimalType").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function


    Public Function GetBreedList(ByRef Animal As String)

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Breed from Breeds WHERE Animals_AnimalID=@Animal ", MySQLConnection)

        query.Parameters.AddWithValue("@Animal", Animal)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("Breed").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function





    Public Function UserCreatePet(ByVal Details() As String)

        Dim command As New MySqlCommand("INSERT INTO Pets (OwnerID, PetName, PetDateOfBirth, PetDescription, Animal, Breed, MicrochipNumber, Deceased) VALUES (@id, @PetName, @dob, @desc, @animal, @breed, @mcn, @d);", MySQLConnection)

        command.Parameters.AddWithValue("@id", Details(0))
        command.Parameters.AddWithValue("@PetName", Details(1))
        command.Parameters.AddWithValue("@dob", Details(2))
        command.Parameters.AddWithValue("@desc", Details(3))
        command.Parameters.AddWithValue("@animal", Details(4))
        command.Parameters.AddWithValue("@breed", Details(5))
        command.Parameters.AddWithValue("@mcn", Details(6))
        command.Parameters.AddWithValue("@d", Details(7))

        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True
    End Function






    Public Function ModifyPet(ByVal Details() As String)


        Dim command As New MySqlCommand("UPDATE Pets Set PetName=@pn, PetDateOfBirth=@pdob, PetDescription=@pdesc, Animal=@a, Breed=@b, MicrochipNumber=@mcn, Deceased=@d WHERE PetID = @pid;", MySQLConnection)

        command.Parameters.AddWithValue("@pn", Details(1))
        command.Parameters.AddWithValue("@pdob", Details(2))
        command.Parameters.AddWithValue("@pdesc", Details(3))
        command.Parameters.AddWithValue("@a", Details(4))
        command.Parameters.AddWithValue("@b", Details(5))
        command.Parameters.AddWithValue("@mcn", Details(6))
        command.Parameters.AddWithValue("@d", Details(7))

        command.Parameters.AddWithValue("@pid", Globals.PetID)

        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True

    End Function






    Public Function GetWeights()

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Timestamp from Weights WHERE Pets_PetID=@id", MySQLConnection)

        query.Parameters.AddWithValue("@id", Globals.PetID)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("Timestamp").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function


    Public Function GetWeightDetails(ByVal Timestamp As String)

        Dim Details(4) As String
        Dim time As DateTime = DateTime.Parse(Timestamp)
        time.ToString("yyyy-MM-dd HH:mm:ss")
        Dim query As New MySqlCommand("SELECT WeightReadingID, Weight, AddedBy, Units, ComparableVar from Weights WHERE Pets_PetID = @id AND Timestamp=@ts", MySQLConnection)
        query.Parameters.AddWithValue("id", Globals.PetID)
        query.Parameters.AddWithValue("@ts", time)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim reader As MySqlDataReader = query.ExecuteReader()
        Do While reader.Read()
            For i = 0 To 4
                If reader.IsDBNull(i) Then
                    Details(i) = ""
                Else
                    Details(i) = reader.GetString(i)
                End If
            Next
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function

    Public Function AddWeight(ByVal AddedDate As String, ByVal Weight As String, ByVal Units As String)


        Dim WeightInKg = UnitToKG(Weight, Units)


        Dim command As New MySqlCommand("INSERT INTO Weights (Pets_PetID, Timestamp, Weight, Units, AddedBy, ComparableVar) VALUES (@id, @ts, @w, @u, @ab, @cv);", MySQLConnection)

        command.Parameters.AddWithValue("@id", Globals.PetID)
        command.Parameters.AddWithValue("@ts", AddedDate)
        command.Parameters.AddWithValue("@w", Weight)
        command.Parameters.AddWithValue("@u", Units)
        command.Parameters.AddWithValue("@ab", Globals.UserID)
        command.Parameters.AddWithValue("@cv", WeightInKg)
        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True
    End Function


    Private Function UnitToKG(ByVal Weight As String, ByVal Unit As String)
        If Unit = "Tonne" Then
            Weight = Weight / 0.001
        ElseIf Unit = "Kilogram" Then
            Weight = Weight
        ElseIf Unit = "Gram" Then
            Weight = Weight / 1000
        ElseIf Unit = "Milligram" Then
            Weight = Weight / 1000000
        ElseIf Unit = "Stone" Then
            Weight = Weight / 0.15747
        ElseIf Unit = "Pound" Then
            Weight = Weight * 0.45359237
        ElseIf Unit = "Ounce" Then
            Weight = Weight * 0.02834952
        End If
        Return Weight
    End Function

    Public Function GetFullname(ByRef ID As Integer)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT FirstName, LastName FROM User_Details WHERE UserID=@id;", MySQLConnection)
        query.Parameters.AddWithValue("@id", ID)
        Dim Name As String = Nothing
        Dim reader As MySqlDataReader = query.ExecuteReader()
        Do While reader.Read()
            Name = String.Format(reader.GetString(0) + " " + reader.GetString(1))
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return (Name)
    End Function









    Public Function GetSizes()

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Timestamp from Sizes WHERE Pets_PetID=@id", MySQLConnection)

        query.Parameters.AddWithValue("@id", Globals.PetID)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("Timestamp").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function


    Public Function GetSizeDetails(ByVal Timestamp As String)

        Dim Details(10) As String
        Dim time As DateTime = DateTime.Parse(Timestamp)
        time.ToString("yyyy-MM-dd HH:mm:ss")
        Dim query As New MySqlCommand("SELECT SizeReadingID, Length, LengthUnit, Width, WidthUnit, Height, HeightUnit, AddedBy, ComparableLength, ComparableWidth, ComparableHeight from Sizes WHERE Pets_PetID = @id AND Timestamp=@ts", MySQLConnection)
        query.Parameters.AddWithValue("id", Globals.PetID)
        query.Parameters.AddWithValue("@ts", time)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim reader As MySqlDataReader = query.ExecuteReader()
        Do While reader.Read()
            For i = 0 To 10
                If reader.IsDBNull(i) Then
                    Details(i) = ""
                Else
                    Details(i) = reader.GetString(i)
                End If
            Next
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function

    Public Function AddSize(ByVal Details() As String)


        Dim LengthToMetres As String = UnitToM(Details(2), Details(6))
        Dim WidthToMetres As String = UnitToM(Details(3), Details(8))
        Dim HeightToMetres As String = UnitToM(Details(4), Details(7))

        Dim command As New MySqlCommand("INSERT INTO Sizes (Pets_PetID, Timestamp, Width, Length, Height, AddedBy, LengthUnit, HeightUnit, WidthUnit, ComparableLength, ComparableWidth, ComparableHeight) VALUES (@id, @ts, @w, @l, @h, @ab, @lu, @hu, @wu, @cl, @cw, @ch);", MySQLConnection)

        command.Parameters.AddWithValue("@id", Globals.PetID) 'done
        command.Parameters.AddWithValue("@ts", Details(1)) ' done
        command.Parameters.AddWithValue("@w", Details(3)) 'done
        command.Parameters.AddWithValue("@l", Details(2)) 'done
        command.Parameters.AddWithValue("@h", Details(4)) 'done
        command.Parameters.AddWithValue("@ab", Details(5)) 'done
        command.Parameters.AddWithValue("@lu", Details(6)) 'done
        command.Parameters.AddWithValue("@hu", Details(7)) 'done
        command.Parameters.AddWithValue("@wu", Details(8)) 'done
        command.Parameters.AddWithValue("@cl", LengthToMetres) 'done
        command.Parameters.AddWithValue("@cw", WidthToMetres) 'done
        command.Parameters.AddWithValue("@ch", HeightToMetres) 'done)

        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True
    End Function


    Private Function UnitToM(ByVal value As String, ByVal Unit As String)
        If Unit = "Inch" Then
            value = value / 39.37
        ElseIf Unit = "Feet" Then
            value = value / 3.2808
        ElseIf Unit = "Millimetre" Then
            value = value / 1000
        ElseIf Unit = "Centimetre" Then
            value = value / 100
        ElseIf Unit = "Metre" Then
            value = value

        End If
        Return value
    End Function





    Public Function GetComments()

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT Timestamp from Comments WHERE Pets_PetID=@id", MySQLConnection)

        query.Parameters.AddWithValue("@id", Globals.PetID)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("Timestamp").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function






    Public Function GetCommentDetails(ByVal Timestamp As String)

        Dim Comment(2) As String
        Dim time As DateTime = DateTime.Parse(Timestamp)
        time.ToString("yyyy-MM-dd HH:mm:ss")
        Dim query As New MySqlCommand("SELECT CommentID, Comment, AddedBy from Comments WHERE Pets_PetID = @id AND Timestamp=@ts", MySQLConnection)
        query.Parameters.AddWithValue("id", Globals.PetID)
        query.Parameters.AddWithValue("@ts", time)
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim reader As MySqlDataReader = query.ExecuteReader()
        Do While reader.Read()

            Comment(0) = reader.GetString(0)
            Comment(1) = reader.GetString(1)
            Comment(2) = reader.GetString(2)
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Comment
    End Function




    Public Function AddComment(ByVal Comment As String, ByVal Timestamp As String)


        Dim command As New MySqlCommand("INSERT INTO Comments (Comment, AddedBy, Pets_PetID, Timestamp) VALUES (@comment, @ab, @id, @ts);", MySQLConnection)


        command.Parameters.AddWithValue("@comment", Comment) ' done
        command.Parameters.AddWithValue("@ab", Globals.UserID) 'done
        command.Parameters.AddWithValue("@id", Globals.PetID) 'done
        command.Parameters.AddWithValue("@ts", Timestamp)


        MySQLConnection.Open()
        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True
    End Function


    Public Function GetAnimals()

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT AnimalType from Animals", MySQLConnection)

        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("AnimalType").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function




End Class
