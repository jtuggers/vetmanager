﻿Imports MySql.Data.MySqlClient 'Imports the downloaded MySQL libraries
Imports System.Security.Cryptography 'Imports the Cryptography libraries
Imports System.Text 'Imports the system text libraries
Public Class Staff
    Inherits User
    Dim MySQLConnection As New MySqlConnection(ConnectionString()) ' defines a new mysql connection with the Connectionstring from the function "Connectionstring"


    Public Function GetUserList() As DataTable
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above

        Dim MySqlDataAdapter As New MySqlDataAdapter("SELECT UserID, Email, Title, FirstName, LastName, DateOfBirth, AccessLevel from User_Details", MySQLConnection)
        Dim ds As New DataTable

        MySqlDataAdapter.Fill(ds)
        MySQLConnection.Close()
        Return (ds)

    End Function

    Public Function ClientListSearch()
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above

        Dim MySqlDataAdapter As New MySqlDataAdapter("SELECT UserID, Title, FirstName, LastName, DateOfBirth from User_Details WHERE (AccessLevel = 0) OR (HasPets = 1)", MySQLConnection)
        Dim ds As New DataTable
        MySqlDataAdapter.Fill(ds)
        MySQLConnection.Close()
        Return (ds)
    End Function

    Public Function GetSearchByType(ByVal Selected As String)
        Select Case Selected
            Case "User ID"
                Return "UserID"
            Case "Title"
                Return "Title"
            Case "First Name"
                Return "FirstName"
            Case "Last Name"
                Return "LastName"
            Case Else
                Return "DateOfBirth"
        End Select
    End Function


    Public Function CreateUser(ByVal Details() As String)

        Dim salt As String = GenerateSalt()

        Dim hash As String = GenerateHash(salt, Details(0), Details(8))


        MySQLConnection.Open()




        Dim command As New MySqlCommand("INSERT INTO User_Details (Email, Salt, Hash, Title, FirstName, LastName, DateOfBirth, HomePhone, MobilePhone, WorkPhone, AddressLine1, AddressLine2, City, County, Country, PostCode, AccessLevel, HasPets) VALUES (@email, @salt, @hash, @title, @fname, @lname, @dob, @hp, @mp, @wp, @al1, @al2, @city, @county, @country, @pc, @al, @pets);", MySQLConnection)


        command.Parameters.AddWithValue("@email", Details(0))
        command.Parameters.AddWithValue("@salt", salt)
        command.Parameters.AddWithValue("@hash", hash)
        command.Parameters.AddWithValue("@title", Details(1))
        command.Parameters.AddWithValue("@fname", Details(2))
        command.Parameters.AddWithValue("@lname", Details(3))
        command.Parameters.AddWithValue("@dob", Details(4))
        command.Parameters.AddWithValue("@hp", Details(5))
        command.Parameters.AddWithValue("@mp", Details(6))
        command.Parameters.AddWithValue("@wp", Details(7))
        command.Parameters.AddWithValue("@al1", Details(9))
        command.Parameters.AddWithValue("@al2", Details(10))
        command.Parameters.AddWithValue("@city", Details(11))
        command.Parameters.AddWithValue("@county", Details(12))
        command.Parameters.AddWithValue("@country", Details(13))
        command.Parameters.AddWithValue("@pc", Details(14))
        command.Parameters.AddWithValue("@al", Details(15))
        command.Parameters.AddWithValue("@pets", "0")

        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True
    End Function







    Public Sub UpdateDetails(ByVal Details() As String)
        Dim command As New MySqlCommand("UPDATE User_Details SET Title=@title, FirstName=@fname, LastName=@lname, DateOfBirth=@dob, HomePhone=@hp, MobilePhone=@mp, WorkPhone=@wp, AddressLine1=@al1, AddressLine2=@al2, City=@city, County=@county, Country=@country, PostCode=@pc, AccessLevel=@al WHERE UserID = @id", MySQLConnection)


        command.Parameters.AddWithValue("@title", Details(0))
        Command.Parameters.AddWithValue("@fname", Details(1))
        Command.Parameters.AddWithValue("@lname", Details(2))
        Command.Parameters.AddWithValue("@dob", Details(3))
        Command.Parameters.AddWithValue("@hp", Details(4))
        Command.Parameters.AddWithValue("@mp", Details(5))
        Command.Parameters.AddWithValue("@wp", Details(6))
        Command.Parameters.AddWithValue("@al1", Details(7))
        Command.Parameters.AddWithValue("@al2", Details(8))
        Command.Parameters.AddWithValue("@city", Details(9))
        Command.Parameters.AddWithValue("@county", Details(10))
        Command.Parameters.AddWithValue("@country", Details(11))
        Command.Parameters.AddWithValue("@pc", Details(12))
        Command.Parameters.AddWithValue("@al", Details(13))
        Command.Parameters.AddWithValue("@id", Globals.ModifyID)

        MySQLConnection.Open()
        Command.ExecuteNonQuery()
        MySQLConnection.Close()

    End Sub





    Public Function GetAllPets() As DataTable
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above

        Dim MySqlDataAdapter As New MySqlDataAdapter(String.Format("SELECT PetID, PetName, Animal, Breed, MicrochipNumber, Deceased from Pets"), MySQLConnection)

        Dim ds As New DataTable

        MySqlDataAdapter.Fill(ds)
        MySQLConnection.Close()
        Return (ds)

    End Function





    Public Function GetClientPetList(ByVal ID As String) As DataTable
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above

        Dim MySqlDataAdapter As New MySqlDataAdapter(String.Format("SELECT PetID, PetName, Animal, Breed, MicrochipNumber, Deceased from Pets WHERE OwnerID={0}", ID), MySQLConnection)

        Dim ds As New DataTable

        MySqlDataAdapter.Fill(ds)
        MySQLConnection.Close()
        Return (ds)

    End Function






End Class
