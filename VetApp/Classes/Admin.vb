﻿Imports MySql.Data.MySqlClient


Public Class Admin

    Inherits User

    Dim MySQLConnection As New MySqlConnection(ConnectionString())

    Public Sub GetConfigPass()
        Try
            MySQLConnection.Open()
            Dim query As New MySqlCommand("SELECT PassID, Password FROM ConfigPass WHERE PassID=(SELECT max(PassID) FROM ConfigPass);", MySQLConnection)
            Dim PasswordVersion As String = Nothing 'Needs to be string, as stored variable is string
            Dim password As String = Nothing
            Dim reader As MySqlDataReader = query.ExecuteReader()

            Do While reader.Read()
                PasswordVersion = reader.GetString(0)
                password = reader.GetString(1)
            Loop
            reader.Close()
            MySQLConnection.Close()

            If (My.Settings.PasswordVersion) IsNot (PasswordVersion) Then
                My.Settings.PasswordVersion = PasswordVersion
                My.Settings.ConfigPassword = password
            End If
        Catch
            Exit Sub
        End Try
    End Sub




    Public Sub DisableUser()
        If Globals.UserID = Globals.ModifyID Then
            MessageBox.Show("You cannot disable your own account.")
        Else

            Dim command As New MySqlCommand("UPDATE User_Details SET AccessLevel=@al WHERE UserID = @id", MySQLConnection)
            command.Parameters.AddWithValue("@al", "3")
            command.Parameters.AddWithValue("@id", Globals.ModifyID)
            MySQLConnection.Open()
            command.ExecuteNonQuery()
            MySQLConnection.Close()

            MessageBox.Show("Account disabled.")
        End If
    End Sub



    Public Function GetPasses()

        Dim Details As New List(Of String)()

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT PassID from ConfigPass ORDER BY PassID DESC;", MySQLConnection)
        Dim reader As MySqlDataReader = query.ExecuteReader()

        Do While reader.Read()
            Details.Add(reader("PasSID").ToString())
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function

    Public Function GetPassDetails(ByVal value As String)


        Dim Details(2) As String

        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        Dim query As New MySqlCommand("SELECT DateSet, Password from ConfigPass WHERE PassID=@id;", MySQLConnection)
        query.Parameters.AddWithValue("@id", value) 'done

        Dim reader As MySqlDataReader = query.ExecuteReader()




        Do While reader.Read()
            For i = 0 To 1
                If reader.IsDBNull(i) Then
                    Details(i) = ""
                Else
                    Details(i) = reader.GetString(i)
                End If
            Next
        Loop
        reader.Close()
        MySQLConnection.Close()
        Return Details
    End Function






    Public Function UpdatePass(ByVal Password As String)



        Dim command As New MySqlCommand("INSERT INTO ConfigPass (Password, DateSet) VALUES (@p, @ds);", MySQLConnection)

        command.Parameters.AddWithValue("@p", Password)
        command.Parameters.AddWithValue("@ds", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        MySQLConnection.Open() 'Opens a connection to the database using the string defined above
        command.ExecuteNonQuery()
        MySQLConnection.Close()
        Return True
    End Function







End Class
