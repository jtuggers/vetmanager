﻿Imports System.Text.RegularExpressions

Public Class Validation

    Public Function Email(ByVal value As String)

        Dim pattern As String = "^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(value, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function Name(ByVal value As String)
        'DECIDED TO LIMIT TO NO SPACES IN FIRSTNAME / LASTNAME

        Dim pattern As String = "^[a-zA-Z-']+$"
        Dim Match As Match = Regex.Match(value, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Function DOB(ByVal value As String)
        Dim Year As Integer
        Dim CurrentYear As Integer = Date.Today.Year

        Try
            DateTime.ParseExact(value, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture)
            MessageBox.Show(value)
            Year = Integer.Parse(value.Substring(6, 4))

            If (Year > CurrentYear) Or (Year < (CurrentYear - 100)) Then
                Return False
            End If
            Return True
        Catch ex As FormatException
            Return False
        End Try

    End Function


    Public Function Phone(ByVal value As String)


        Dim pattern As String = "^([0]{1})([0-9]{10})$"

        value = value.Replace(" ", "") 'Removes all spaces

        Dim Match As Match = Regex.Match(value, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If

    End Function


    Public Function Postcode(ByVal value As String)


        Dim pattern As String = "^(([gG][iI][rR] {0,}0[aA]{2})|((([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y]?[0-9][0-9]?)|(([a-pr-uwyzA-PR-UWYZ][0-9][a-hjkstuwA-HJKSTUW])|([a-pr-uwyzA-PR-UWYZ][a-hk-yA-HK-Y][0-9][abehmnprv-yABEHMNPRV-Y]))) {0,}[0-9][abd-hjlnp-uw-zABD-HJLNP-UW-Z]{2}))$"


        Dim Match As Match = Regex.Match(value, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If

    End Function



    Public Function CheckString(ByVal StringToCheck As String)
        Dim pattern As String = "^[a-zA-Z\-\s]+$"


        Dim Match As Match = Regex.Match(StringToCheck, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function CheckEmail(ByVal StringToCheck As String)
        Dim pattern As String = "^[a-zA-Z0-9\w\.-@~]+$"


        Dim Match As Match = Regex.Match(StringToCheck, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If
    End Function



    Public Function CheckInt(ByVal IntToCheck As String)
        Dim pattern As String = "^[1-9]+$"
        Dim Match As Match = Regex.Match(IntToCheck, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If
    End Function




    Public Function PetName(ByVal value As String)


        Dim pattern As String = "^[a-zA-Z1-9-'~,.]+$"
        Dim Match As Match = Regex.Match(value, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If

    End Function


    Public Function PetVals(ByVal value As String)


        Dim pattern As String = "^[0-9\.]+$"

        value = value.Replace(" ", "") 'Removes all spaces

        Dim Match As Match = Regex.Match(value, pattern)
        If Match.Success Then
            Return True
        Else
            Return False
        End If

    End Function













    '    Dim FullText As String = TextBox1.Text
    '    Dim SeperatedWords As New List(Of String)
    '' ToList function converts the array to a list.
    'SeperatedWords = FullText.Split(",").ToList
    '' Reset the text for re-presentation.
    'FullText = ""
    '' Goes through all the seperated words and assign them to FullText with a new line.
    'For Each Word As String In SeperatedWords
    '    FullText = FullText & Word & Environment.NewLine
    'Next
    '' Present the new list in the text box.
    'TextBox1.Text = FullText
End Class
'http://stackoverflow.com/questions/11175442/installation-path-of-clickonce-apps