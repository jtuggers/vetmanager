﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddComment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtBxComment = New System.Windows.Forms.TextBox()
        Me.BtnAddWeight = New System.Windows.Forms.Button()
        Me.LblAddComment = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblCharCount = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TxtBxComment
        '
        Me.TxtBxComment.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TxtBxComment.Location = New System.Drawing.Point(34, 99)
        Me.TxtBxComment.MaxLength = 4000
        Me.TxtBxComment.Multiline = True
        Me.TxtBxComment.Name = "TxtBxComment"
        Me.TxtBxComment.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxtBxComment.Size = New System.Drawing.Size(358, 149)
        Me.TxtBxComment.TabIndex = 138
        '
        'BtnAddWeight
        '
        Me.BtnAddWeight.Location = New System.Drawing.Point(202, 44)
        Me.BtnAddWeight.Name = "BtnAddWeight"
        Me.BtnAddWeight.Size = New System.Drawing.Size(92, 35)
        Me.BtnAddWeight.TabIndex = 136
        Me.BtnAddWeight.Text = "Save Comment"
        Me.BtnAddWeight.UseVisualStyleBackColor = True
        '
        'LblAddComment
        '
        Me.LblAddComment.AutoSize = True
        Me.LblAddComment.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddComment.Location = New System.Drawing.Point(29, 44)
        Me.LblAddComment.Name = "LblAddComment"
        Me.LblAddComment.Size = New System.Drawing.Size(135, 26)
        Me.LblAddComment.TabIndex = 135
        Me.LblAddComment.Text = "Add Comment"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(300, 44)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 35)
        Me.BtnClose.TabIndex = 134
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(418, 24)
        Me.MenuStrip1.TabIndex = 144
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(34, 80)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 13)
        Me.Label1.TabIndex = 145
        Me.Label1.Text = "Characters Remaining:"
        '
        'LblCharCount
        '
        Me.LblCharCount.AutoSize = True
        Me.LblCharCount.Location = New System.Drawing.Point(143, 81)
        Me.LblCharCount.Name = "LblCharCount"
        Me.LblCharCount.Size = New System.Drawing.Size(31, 13)
        Me.LblCharCount.TabIndex = 146
        Me.LblCharCount.Text = "4000"
        '
        'AddComment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 273)
        Me.Controls.Add(Me.LblCharCount)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.TxtBxComment)
        Me.Controls.Add(Me.BtnAddWeight)
        Me.Controls.Add(Me.LblAddComment)
        Me.Controls.Add(Me.BtnClose)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "AddComment"
        Me.Text = "Add Pet Comment"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtBxComment As TextBox
    Friend WithEvents BtnAddWeight As Button
    Friend WithEvents LblAddComment As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label1 As Label
    Friend WithEvents LblCharCount As Label
End Class
