﻿Public Class AddComment
    Dim User As New User
    Private Sub TxtBxWeight_TextChanged(sender As Object, e As EventArgs) Handles TxtBxComment.TextChanged
        LblCharCount.Text = 4000 - Len(TxtBxComment.Text)
    End Sub

    Private Sub TxtBxComment_KeyDown(sender As Object, e As KeyEventArgs) Handles TxtBxComment.KeyDown
        If (e.Control And e.KeyCode = Keys.A) Then TxtBxComment.SelectAll()
    End Sub

    Private Sub BtnAddWeight_Click(sender As Object, e As EventArgs) Handles BtnAddWeight.Click

        If Len(TxtBxComment.Text) = 0 Then
            MessageBox.Show("Please enter a comment.")
            Return
        End If

        Dim Time As String = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")



        Try
            User.AddComment(TxtBxComment.Text, Time)
            MessageBox.Show("Comment added successfully!")
            Me.Close()
        Catch
            MessageBox.Show("An error occured and the comment could not be added. Please try again later.")
        End Try
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub
End Class

