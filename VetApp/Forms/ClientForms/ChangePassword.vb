﻿Imports System.ComponentModel

Public Class ChangePassword
    Dim User As New User
    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click

        Dim UserID As Integer = Globals.ModifyID
        Dim Email As String = User.GetEmail()
        Dim OldPass As String = TxtBxOldPass.Text
        Dim NewPass As String = TxtBxNewPass.Text
        Dim ConfNewPass As String = TxtBxConfNewPass.Text


        TxtBxOldPass.Focus()
        TxtBxNewPass.Focus()
        TxtBxConfNewPass.Focus()

        For Each control As Control In Me.Controls
            If TypeName(control) = "TextBox" Then
                Dim txtbx As TextBox = CType(control, TextBox)
                If Not ErrorProvider.GetError(txtbx) = "" Then
                    MessageBox.Show("Validation Errors found, please correct before continuing.", "Validation Errors", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If
            End If
        Next



        If User.LoginTest(Email, OldPass) = True Then
            Try
                User.ChangeUserPass(Email, NewPass)
            Catch
                MessageBox.Show("Password could not be changed.")
            End Try

        Else
            MessageBox.Show("Authentication failed, invalid password provided.")
        End If


        ModifyUser.Close()
        UserManager.Show()
        Me.Close()
    End Sub



    Private Sub TxtBxOldPass_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxOldPass.Validating
        If Len(TxtBxOldPass.Text) = 0 Then
            ErrorProvider.SetError(TxtBxOldPass, "Old Password is required.")
        Else
            ErrorProvider.SetError(TxtBxOldPass, "")
        End If
    End Sub

    Private Sub TxtBxNewPass_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxNewPass.Validating
        If Len(TxtBxNewPass.Text) = 0 Then
            ErrorProvider.SetError(TxtBxNewPass, "Password is required.")
        Else
            ErrorProvider.SetError(TxtBxNewPass, "")
        End If
    End Sub

    Private Sub TxtBxConfNewPass_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxConfNewPass.Validating
        If Len(TxtBxConfNewPass.Text) = 0 Then
            ErrorProvider.SetError(TxtBxConfNewPass, "Password is required.")
        ElseIf Not TxtBxConfNewPass.Text = TxtBxNewPass.Text Then
            ErrorProvider.SetError(TxtBxConfNewPass, "Passwords Must match")
        Else
            ErrorProvider.SetError(TxtBxConfNewPass, "")
        End If

    End Sub
End Class