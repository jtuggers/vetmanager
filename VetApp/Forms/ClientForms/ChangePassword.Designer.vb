﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChangePassword
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.BtnSubmit = New System.Windows.Forms.Button()
        Me.TxtBxConfNewPass = New System.Windows.Forms.TextBox()
        Me.TxtBxNewPass = New System.Windows.Forms.TextBox()
        Me.TxtBxOldPass = New System.Windows.Forms.TextBox()
        Me.LblConfNewPass = New System.Windows.Forms.Label()
        Me.LblNewPass = New System.Windows.Forms.Label()
        Me.LblOldPass = New System.Windows.Forms.Label()
        Me.LblModifyUser = New System.Windows.Forms.Label()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'BtnSubmit
        '
        Me.BtnSubmit.Location = New System.Drawing.Point(156, 184)
        Me.BtnSubmit.Name = "BtnSubmit"
        Me.BtnSubmit.Size = New System.Drawing.Size(189, 23)
        Me.BtnSubmit.TabIndex = 90
        Me.BtnSubmit.Text = "Submit"
        Me.BtnSubmit.UseVisualStyleBackColor = True
        '
        'TxtBxConfNewPass
        '
        Me.TxtBxConfNewPass.Location = New System.Drawing.Point(156, 142)
        Me.TxtBxConfNewPass.Name = "TxtBxConfNewPass"
        Me.TxtBxConfNewPass.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxConfNewPass.TabIndex = 89
        Me.TxtBxConfNewPass.UseSystemPasswordChar = True
        '
        'TxtBxNewPass
        '
        Me.TxtBxNewPass.Location = New System.Drawing.Point(156, 100)
        Me.TxtBxNewPass.Name = "TxtBxNewPass"
        Me.TxtBxNewPass.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxNewPass.TabIndex = 88
        '
        'TxtBxOldPass
        '
        Me.TxtBxOldPass.Location = New System.Drawing.Point(156, 58)
        Me.TxtBxOldPass.Name = "TxtBxOldPass"
        Me.TxtBxOldPass.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxOldPass.TabIndex = 87
        '
        'LblConfNewPass
        '
        Me.LblConfNewPass.AutoSize = True
        Me.LblConfNewPass.Location = New System.Drawing.Point(34, 145)
        Me.LblConfNewPass.Name = "LblConfNewPass"
        Me.LblConfNewPass.Size = New System.Drawing.Size(116, 13)
        Me.LblConfNewPass.TabIndex = 86
        Me.LblConfNewPass.Text = "Confirm New Password"
        '
        'LblNewPass
        '
        Me.LblNewPass.AutoSize = True
        Me.LblNewPass.Location = New System.Drawing.Point(72, 103)
        Me.LblNewPass.Name = "LblNewPass"
        Me.LblNewPass.Size = New System.Drawing.Size(78, 13)
        Me.LblNewPass.TabIndex = 85
        Me.LblNewPass.Text = "New Password"
        '
        'LblOldPass
        '
        Me.LblOldPass.AutoSize = True
        Me.LblOldPass.Location = New System.Drawing.Point(78, 61)
        Me.LblOldPass.Name = "LblOldPass"
        Me.LblOldPass.Size = New System.Drawing.Size(72, 13)
        Me.LblOldPass.TabIndex = 84
        Me.LblOldPass.Text = "Old Password"
        '
        'LblModifyUser
        '
        Me.LblModifyUser.AutoSize = True
        Me.LblModifyUser.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblModifyUser.Location = New System.Drawing.Point(12, 9)
        Me.LblModifyUser.Name = "LblModifyUser"
        Me.LblModifyUser.Size = New System.Drawing.Size(159, 26)
        Me.LblModifyUser.TabIndex = 83
        Me.LblModifyUser.Text = "Modify Password"
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'ChangePassword
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(427, 261)
        Me.Controls.Add(Me.BtnSubmit)
        Me.Controls.Add(Me.TxtBxConfNewPass)
        Me.Controls.Add(Me.TxtBxNewPass)
        Me.Controls.Add(Me.TxtBxOldPass)
        Me.Controls.Add(Me.LblConfNewPass)
        Me.Controls.Add(Me.LblNewPass)
        Me.Controls.Add(Me.LblOldPass)
        Me.Controls.Add(Me.LblModifyUser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "ChangePassword"
        Me.Text = "Change Password"
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnSubmit As Button
    Friend WithEvents TxtBxConfNewPass As TextBox
    Friend WithEvents TxtBxNewPass As TextBox
    Friend WithEvents TxtBxOldPass As TextBox
    Friend WithEvents LblConfNewPass As Label
    Friend WithEvents LblNewPass As Label
    Friend WithEvents LblOldPass As Label
    Friend WithEvents LblModifyUser As Label
    Friend WithEvents ErrorProvider As ErrorProvider
End Class
