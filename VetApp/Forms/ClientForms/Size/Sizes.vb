﻿Public Class Sizes
    Dim User As New User
    Private Sub Sizes_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Details As List(Of String) = User.GetSizes()
        For Each Item In Details
            ListBxHistory.Items.Add(Item)
        Next
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub ListBxHistory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBxHistory.SelectedIndexChanged
        Dim Selected As String = ListBxHistory.GetItemText(ListBxHistory.SelectedItem)

        Dim Details() As String = User.GetSizeDetails(Selected)

        TxtBxReadingID.Text = Details(0)
        TxtBxLength.Text = Details(1)
        TxtBxLengthUnit.Text = Details(2)
        TxtBxWidth.Text = Details(3)
        TxtBxWidthUnit.Text = Details(4)
        TxtBxHeight.Text = Details(5)
        TxtBxHeightUnit.Text = Details(6)
        TxtBxAddedBy.Text = User.GetFullname(Details(7))
        TxtBxComparableLength.Text = Details(8)
        TxtBxComparableWidth.Text = Details(9)
        TxtBxComparableHeight.Text = Details(10)
    End Sub

    Private Sub BtnAddSize_Click(sender As Object, e As EventArgs) Handles BtnAddSize.Click
        AddSize.ShowDialog()
    End Sub
End Class