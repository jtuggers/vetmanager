﻿Public Class AddSize
    Dim Validation As New Validation
    Dim User As New User


    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub AddSize_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RdBtnNow.Checked = True
        DatePicker.Enabled = False
    End Sub

    Private Sub RdBtnNow_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnNow.CheckedChanged
        DatePicker.Enabled = False
    End Sub

    Private Sub RdBtnBackdate_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnBackdate.CheckedChanged
        DatePicker.Enabled = True
    End Sub

    Private Sub CboxLengthUnit_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CboxLengthUnit.SelectedIndexChanged
        CboxHeightUnit.SelectedIndex = CboxLengthUnit.SelectedIndex
        CboxWidthUnit.SelectedIndex = CboxLengthUnit.SelectedIndex
    End Sub


    Private Sub BtnAddSize_Click(sender As Object, e As EventArgs) Handles BtnAddSize.Click

        If Len(TxtBxLength.Text) = 0 Then
            ErrorProvider.SetError(CboxLengthUnit, "Please enter Length value.")
            Return
        ElseIf CboxLengthUnit.SelectedIndex = -1 Then
            ErrorProvider.SetError(CboxLengthUnit, "Please select units.")
            Return
        Else
            ErrorProvider.SetError(CboxLengthUnit, "")
        End If


        If Len(TxtBxWidth.Text) = 0 Then
            ErrorProvider.SetError(CboxWidthUnit, "Please enter Width value.")
            Return
        ElseIf CboxWidthUnit.SelectedIndex = -1 Then
            ErrorProvider.SetError(CboxWidthUnit, "Please select units.")
            Return
        Else
            ErrorProvider.SetError(CboxWidthUnit, "")
        End If


        If Len(TxtBxHeight.Text) = 0 Then
            ErrorProvider.SetError(CboxHeightUnit, "Please enter Height value.")
            Return
        ElseIf CboxHeightUnit.SelectedIndex = -1 Then
            ErrorProvider.SetError(CboxHeightUnit, "Please select units.")
            Return
        Else
            ErrorProvider.SetError(CboxHeightUnit, "")
        End If


        Dim AddedDate As String = Nothing
        If RdBtnNow.Checked = True Then
            AddedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        ElseIf RdBtnBackdate.Checked = True Then
            AddedDate = DatePicker.Value.ToString()
        End If


        If Validation.PetVals(TxtBxLength.Text) = False Then
            ErrorProvider.SetError(CboxLengthUnit, "Invalid Length. Please ensure only numbers and periods are used.")
            Return
        ElseIf Validation.PetVals(TxtBxLength.Text) = True Then
            ErrorProvider.SetError(CboxLengthUnit, "")
        End If

        If Validation.PetVals(TxtBxWidth.Text) = False Then
            ErrorProvider.SetError(CboxWidthUnit, "Invalid Width. Please ensure only numbers and periods are used.")
            Return
        ElseIf Validation.PetVals(TxtBxWidth.Text) = True Then
            ErrorProvider.SetError(CboxWidthUnit, "")
        End If

        If Validation.PetVals(TxtBxHeight.Text) = False Then
            ErrorProvider.SetError(CboxHeightUnit, "Invalid Length. Please ensure only numbers and periods are used.")
            Return
        ElseIf Validation.PetVals(TxtBxHeight.Text) = True Then
            ErrorProvider.SetError(CboxHeightUnit, "")
        End If

        Dim Details(8) As String

        Details(0) = Globals.PetID
        Details(1) = AddedDate
        Details(2) = TxtBxLength.Text
        Details(3) = TxtBxWidth.Text
        Details(4) = TxtBxHeight.Text
        Details(5) = Globals.UserID
        Details(6) = CboxLengthUnit.Text
        Details(7) = CboxHeightUnit.Text
        Details(8) = CboxWidthUnit.Text




        Try
            User.AddSize(Details)
            MessageBox.Show("Weight recording added successfully!")
            Me.Close()
        Catch
            MessageBox.Show("An error occured and the record could not be added. Please try again later.")
        End Try
    End Sub


End Class