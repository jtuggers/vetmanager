﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Sizes
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtBxAddedBy = New System.Windows.Forms.TextBox()
        Me.LblAddedUser = New System.Windows.Forms.Label()
        Me.TxtBxLengthUnit = New System.Windows.Forms.TextBox()
        Me.TxtBxWidth = New System.Windows.Forms.TextBox()
        Me.LblKG = New System.Windows.Forms.Label()
        Me.TxtBxLength = New System.Windows.Forms.TextBox()
        Me.LblRecordedLength = New System.Windows.Forms.Label()
        Me.TxtBxReadingID = New System.Windows.Forms.TextBox()
        Me.LblWeightID = New System.Windows.Forms.Label()
        Me.BtnAddSize = New System.Windows.Forms.Button()
        Me.LblSizeHistory = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListBxHistory = New System.Windows.Forms.ListBox()
        Me.TxtBxHeight = New System.Windows.Forms.TextBox()
        Me.LblRecordedHeight = New System.Windows.Forms.Label()
        Me.TxtBxWidthUnit = New System.Windows.Forms.TextBox()
        Me.TxtBxHeightUnit = New System.Windows.Forms.TextBox()
        Me.TxtBxComparableHeight = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.TxtBxComparableWidth = New System.Windows.Forms.TextBox()
        Me.TxtBxComparableLength = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TxtBxAddedBy
        '
        Me.TxtBxAddedBy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddedBy.Location = New System.Drawing.Point(425, 85)
        Me.TxtBxAddedBy.Name = "TxtBxAddedBy"
        Me.TxtBxAddedBy.ReadOnly = True
        Me.TxtBxAddedBy.Size = New System.Drawing.Size(162, 20)
        Me.TxtBxAddedBy.TabIndex = 137
        '
        'LblAddedUser
        '
        Me.LblAddedUser.AutoSize = True
        Me.LblAddedUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddedUser.Location = New System.Drawing.Point(381, 81)
        Me.LblAddedUser.Name = "LblAddedUser"
        Me.LblAddedUser.Size = New System.Drawing.Size(38, 26)
        Me.LblAddedUser.TabIndex = 136
        Me.LblAddedUser.Text = "Added" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "By"
        '
        'TxtBxLengthUnit
        '
        Me.TxtBxLengthUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxLengthUnit.Location = New System.Drawing.Point(364, 121)
        Me.TxtBxLengthUnit.Name = "TxtBxLengthUnit"
        Me.TxtBxLengthUnit.ReadOnly = True
        Me.TxtBxLengthUnit.Size = New System.Drawing.Size(55, 20)
        Me.TxtBxLengthUnit.TabIndex = 135
        '
        'TxtBxWidth
        '
        Me.TxtBxWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxWidth.Location = New System.Drawing.Point(259, 159)
        Me.TxtBxWidth.Name = "TxtBxWidth"
        Me.TxtBxWidth.ReadOnly = True
        Me.TxtBxWidth.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxWidth.TabIndex = 134
        '
        'LblKG
        '
        Me.LblKG.AutoSize = True
        Me.LblKG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKG.Location = New System.Drawing.Point(203, 155)
        Me.LblKG.Name = "LblKG"
        Me.LblKG.Size = New System.Drawing.Size(54, 26)
        Me.LblKG.TabIndex = 133
        Me.LblKG.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Width"
        '
        'TxtBxLength
        '
        Me.TxtBxLength.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxLength.Location = New System.Drawing.Point(259, 121)
        Me.TxtBxLength.Name = "TxtBxLength"
        Me.TxtBxLength.ReadOnly = True
        Me.TxtBxLength.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxLength.TabIndex = 132
        '
        'LblRecordedLength
        '
        Me.LblRecordedLength.AutoSize = True
        Me.LblRecordedLength.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedLength.Location = New System.Drawing.Point(203, 117)
        Me.LblRecordedLength.Name = "LblRecordedLength"
        Me.LblRecordedLength.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedLength.TabIndex = 131
        Me.LblRecordedLength.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Length"
        '
        'TxtBxReadingID
        '
        Me.TxtBxReadingID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxReadingID.Location = New System.Drawing.Point(259, 85)
        Me.TxtBxReadingID.Name = "TxtBxReadingID"
        Me.TxtBxReadingID.ReadOnly = True
        Me.TxtBxReadingID.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxReadingID.TabIndex = 130
        '
        'LblWeightID
        '
        Me.LblWeightID.AutoSize = True
        Me.LblWeightID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWeightID.Location = New System.Drawing.Point(205, 81)
        Me.LblWeightID.Name = "LblWeightID"
        Me.LblWeightID.Size = New System.Drawing.Size(47, 26)
        Me.LblWeightID.TabIndex = 129
        Me.LblWeightID.Text = "Reading" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ID"
        '
        'BtnAddSize
        '
        Me.BtnAddSize.Location = New System.Drawing.Point(380, 30)
        Me.BtnAddSize.Name = "BtnAddSize"
        Me.BtnAddSize.Size = New System.Drawing.Size(92, 35)
        Me.BtnAddSize.TabIndex = 128
        Me.BtnAddSize.Text = "Add Size Update"
        Me.BtnAddSize.UseVisualStyleBackColor = True
        '
        'LblSizeHistory
        '
        Me.LblSizeHistory.AutoSize = True
        Me.LblSizeHistory.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSizeHistory.Location = New System.Drawing.Point(36, 32)
        Me.LblSizeHistory.Name = "LblSizeHistory"
        Me.LblSizeHistory.Size = New System.Drawing.Size(111, 26)
        Me.LblSizeHistory.TabIndex = 127
        Me.LblSizeHistory.Text = "Size History"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(495, 30)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 35)
        Me.BtnClose.TabIndex = 126
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(609, 24)
        Me.MenuStrip1.TabIndex = 125
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'ListBxHistory
        '
        Me.ListBxHistory.FormattingEnabled = True
        Me.ListBxHistory.Location = New System.Drawing.Point(24, 78)
        Me.ListBxHistory.Name = "ListBxHistory"
        Me.ListBxHistory.ScrollAlwaysVisible = True
        Me.ListBxHistory.Size = New System.Drawing.Size(173, 147)
        Me.ListBxHistory.TabIndex = 124
        '
        'TxtBxHeight
        '
        Me.TxtBxHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxHeight.Location = New System.Drawing.Point(259, 197)
        Me.TxtBxHeight.Name = "TxtBxHeight"
        Me.TxtBxHeight.ReadOnly = True
        Me.TxtBxHeight.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxHeight.TabIndex = 139
        '
        'LblRecordedHeight
        '
        Me.LblRecordedHeight.AutoSize = True
        Me.LblRecordedHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedHeight.Location = New System.Drawing.Point(203, 192)
        Me.LblRecordedHeight.Name = "LblRecordedHeight"
        Me.LblRecordedHeight.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedHeight.TabIndex = 140
        Me.LblRecordedHeight.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Height"
        '
        'TxtBxWidthUnit
        '
        Me.TxtBxWidthUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxWidthUnit.Location = New System.Drawing.Point(364, 159)
        Me.TxtBxWidthUnit.Name = "TxtBxWidthUnit"
        Me.TxtBxWidthUnit.ReadOnly = True
        Me.TxtBxWidthUnit.Size = New System.Drawing.Size(55, 20)
        Me.TxtBxWidthUnit.TabIndex = 141
        '
        'TxtBxHeightUnit
        '
        Me.TxtBxHeightUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxHeightUnit.Location = New System.Drawing.Point(364, 197)
        Me.TxtBxHeightUnit.Name = "TxtBxHeightUnit"
        Me.TxtBxHeightUnit.ReadOnly = True
        Me.TxtBxHeightUnit.Size = New System.Drawing.Size(55, 20)
        Me.TxtBxHeightUnit.TabIndex = 142
        '
        'TxtBxComparableHeight
        '
        Me.TxtBxComparableHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxComparableHeight.Location = New System.Drawing.Point(444, 197)
        Me.TxtBxComparableHeight.Name = "TxtBxComparableHeight"
        Me.TxtBxComparableHeight.ReadOnly = True
        Me.TxtBxComparableHeight.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxComparableHeight.TabIndex = 146
        '
        'TextBox7
        '
        Me.TextBox7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(549, 121)
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.ReadOnly = True
        Me.TextBox7.Size = New System.Drawing.Size(38, 20)
        Me.TextBox7.TabIndex = 145
        Me.TextBox7.Text = "Metres"
        '
        'TxtBxComparableWidth
        '
        Me.TxtBxComparableWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxComparableWidth.Location = New System.Drawing.Point(444, 159)
        Me.TxtBxComparableWidth.Name = "TxtBxComparableWidth"
        Me.TxtBxComparableWidth.ReadOnly = True
        Me.TxtBxComparableWidth.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxComparableWidth.TabIndex = 144
        '
        'TxtBxComparableLength
        '
        Me.TxtBxComparableLength.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxComparableLength.Location = New System.Drawing.Point(444, 121)
        Me.TxtBxComparableLength.Name = "TxtBxComparableLength"
        Me.TxtBxComparableLength.ReadOnly = True
        Me.TxtBxComparableLength.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxComparableLength.TabIndex = 143
        '
        'TextBox4
        '
        Me.TextBox4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox4.Location = New System.Drawing.Point(549, 159)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        Me.TextBox4.Size = New System.Drawing.Size(38, 20)
        Me.TextBox4.TabIndex = 147
        Me.TextBox4.Text = "Metres"
        '
        'TextBox5
        '
        Me.TextBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox5.Location = New System.Drawing.Point(549, 197)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.ReadOnly = True
        Me.TextBox5.Size = New System.Drawing.Size(38, 20)
        Me.TextBox5.TabIndex = 148
        Me.TextBox5.Text = "Metres"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(422, 124)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 13)
        Me.Label1.TabIndex = 149
        Me.Label1.Text = "=>"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(422, 162)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(19, 13)
        Me.Label2.TabIndex = 150
        Me.Label2.Text = "=>"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(422, 200)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(19, 13)
        Me.Label3.TabIndex = 151
        Me.Label3.Text = "=>"
        '
        'Sizes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(609, 241)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TxtBxComparableHeight)
        Me.Controls.Add(Me.TextBox7)
        Me.Controls.Add(Me.TxtBxComparableWidth)
        Me.Controls.Add(Me.TxtBxComparableLength)
        Me.Controls.Add(Me.TxtBxHeightUnit)
        Me.Controls.Add(Me.TxtBxWidthUnit)
        Me.Controls.Add(Me.LblRecordedHeight)
        Me.Controls.Add(Me.TxtBxHeight)
        Me.Controls.Add(Me.TxtBxAddedBy)
        Me.Controls.Add(Me.LblAddedUser)
        Me.Controls.Add(Me.TxtBxLengthUnit)
        Me.Controls.Add(Me.TxtBxWidth)
        Me.Controls.Add(Me.LblKG)
        Me.Controls.Add(Me.TxtBxLength)
        Me.Controls.Add(Me.LblRecordedLength)
        Me.Controls.Add(Me.TxtBxReadingID)
        Me.Controls.Add(Me.LblWeightID)
        Me.Controls.Add(Me.BtnAddSize)
        Me.Controls.Add(Me.LblSizeHistory)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.ListBxHistory)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Sizes"
        Me.Text = "Sizes"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TxtBxAddedBy As TextBox
    Friend WithEvents LblAddedUser As Label
    Friend WithEvents TxtBxLengthUnit As TextBox
    Friend WithEvents TxtBxWidth As TextBox
    Friend WithEvents LblKG As Label
    Friend WithEvents TxtBxLength As TextBox
    Friend WithEvents LblRecordedLength As Label
    Friend WithEvents TxtBxReadingID As TextBox
    Friend WithEvents LblWeightID As Label
    Friend WithEvents BtnAddSize As Button
    Friend WithEvents LblSizeHistory As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListBxHistory As ListBox
    Friend WithEvents TxtBxHeight As TextBox
    Friend WithEvents LblRecordedHeight As Label
    Friend WithEvents TxtBxWidthUnit As TextBox
    Friend WithEvents TxtBxHeightUnit As TextBox
    Friend WithEvents TxtBxComparableHeight As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents TxtBxComparableWidth As TextBox
    Friend WithEvents TxtBxComparableLength As TextBox
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents TextBox5 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
End Class
