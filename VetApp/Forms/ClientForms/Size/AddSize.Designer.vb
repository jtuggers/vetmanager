﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddSize
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.RdBtnBackdate = New System.Windows.Forms.RadioButton()
        Me.RdBtnNow = New System.Windows.Forms.RadioButton()
        Me.CboxLengthUnit = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DatePicker = New System.Windows.Forms.DateTimePicker()
        Me.TxtBxLength = New System.Windows.Forms.TextBox()
        Me.LblRecordedLength = New System.Windows.Forms.Label()
        Me.BtnAddSize = New System.Windows.Forms.Button()
        Me.LblAddSize = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CboxHeightUnit = New System.Windows.Forms.ComboBox()
        Me.TxtBxHeight = New System.Windows.Forms.TextBox()
        Me.LblRecordedHeight = New System.Windows.Forms.Label()
        Me.CboxWidthUnit = New System.Windows.Forms.ComboBox()
        Me.TxtBxWidth = New System.Windows.Forms.TextBox()
        Me.LblRecordedWidth = New System.Windows.Forms.Label()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.MenuStrip1.SuspendLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'RdBtnBackdate
        '
        Me.RdBtnBackdate.AutoSize = True
        Me.RdBtnBackdate.Location = New System.Drawing.Point(136, 95)
        Me.RdBtnBackdate.Name = "RdBtnBackdate"
        Me.RdBtnBackdate.Size = New System.Drawing.Size(79, 17)
        Me.RdBtnBackdate.TabIndex = 144
        Me.RdBtnBackdate.TabStop = True
        Me.RdBtnBackdate.Text = "Backdating"
        Me.RdBtnBackdate.UseVisualStyleBackColor = True
        '
        'RdBtnNow
        '
        Me.RdBtnNow.AutoSize = True
        Me.RdBtnNow.Location = New System.Drawing.Point(83, 95)
        Me.RdBtnNow.Name = "RdBtnNow"
        Me.RdBtnNow.Size = New System.Drawing.Size(47, 17)
        Me.RdBtnNow.TabIndex = 143
        Me.RdBtnNow.TabStop = True
        Me.RdBtnNow.Text = "Now"
        Me.RdBtnNow.UseVisualStyleBackColor = True
        '
        'CboxLengthUnit
        '
        Me.CboxLengthUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!)
        Me.CboxLengthUnit.FormattingEnabled = True
        Me.CboxLengthUnit.Items.AddRange(New Object() {"Inch", "Feet", "Millimetre", "Centimetre", "Metre"})
        Me.CboxLengthUnit.Location = New System.Drawing.Point(194, 161)
        Me.CboxLengthUnit.Name = "CboxLengthUnit"
        Me.CboxLengthUnit.Size = New System.Drawing.Size(79, 21)
        Me.CboxLengthUnit.TabIndex = 142
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(29, 105)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 26)
        Me.Label1.TabIndex = 141
        Me.Label1.Text = "Reading" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Date"
        '
        'DatePicker
        '
        Me.DatePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DatePicker.Location = New System.Drawing.Point(83, 120)
        Me.DatePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.DatePicker.Name = "DatePicker"
        Me.DatePicker.Size = New System.Drawing.Size(190, 21)
        Me.DatePicker.TabIndex = 140
        '
        'TxtBxLength
        '
        Me.TxtBxLength.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TxtBxLength.Location = New System.Drawing.Point(83, 161)
        Me.TxtBxLength.Name = "TxtBxLength"
        Me.TxtBxLength.Size = New System.Drawing.Size(112, 21)
        Me.TxtBxLength.TabIndex = 139
        '
        'LblRecordedLength
        '
        Me.LblRecordedLength.AutoSize = True
        Me.LblRecordedLength.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedLength.Location = New System.Drawing.Point(29, 156)
        Me.LblRecordedLength.Name = "LblRecordedLength"
        Me.LblRecordedLength.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedLength.TabIndex = 138
        Me.LblRecordedLength.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Length"
        '
        'BtnAddSize
        '
        Me.BtnAddSize.Location = New System.Drawing.Point(83, 286)
        Me.BtnAddSize.Name = "BtnAddSize"
        Me.BtnAddSize.Size = New System.Drawing.Size(190, 35)
        Me.BtnAddSize.TabIndex = 137
        Me.BtnAddSize.Text = "Add Size Update"
        Me.BtnAddSize.UseVisualStyleBackColor = True
        '
        'LblAddSize
        '
        Me.LblAddSize.AutoSize = True
        Me.LblAddSize.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddSize.Location = New System.Drawing.Point(27, 42)
        Me.LblAddSize.Name = "LblAddSize"
        Me.LblAddSize.Size = New System.Drawing.Size(84, 26)
        Me.LblAddSize.TabIndex = 136
        Me.LblAddSize.Text = "Add Size"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(181, 42)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 35)
        Me.BtnClose.TabIndex = 135
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(301, 24)
        Me.MenuStrip1.TabIndex = 134
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'CboxHeightUnit
        '
        Me.CboxHeightUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!)
        Me.CboxHeightUnit.FormattingEnabled = True
        Me.CboxHeightUnit.Items.AddRange(New Object() {"Inch", "Feet", "Millimetre", "Centimetre", "Metre"})
        Me.CboxHeightUnit.Location = New System.Drawing.Point(194, 241)
        Me.CboxHeightUnit.Name = "CboxHeightUnit"
        Me.CboxHeightUnit.Size = New System.Drawing.Size(79, 21)
        Me.CboxHeightUnit.TabIndex = 147
        '
        'TxtBxHeight
        '
        Me.TxtBxHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TxtBxHeight.Location = New System.Drawing.Point(83, 241)
        Me.TxtBxHeight.Name = "TxtBxHeight"
        Me.TxtBxHeight.Size = New System.Drawing.Size(112, 21)
        Me.TxtBxHeight.TabIndex = 146
        '
        'LblRecordedHeight
        '
        Me.LblRecordedHeight.AutoSize = True
        Me.LblRecordedHeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedHeight.Location = New System.Drawing.Point(29, 236)
        Me.LblRecordedHeight.Name = "LblRecordedHeight"
        Me.LblRecordedHeight.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedHeight.TabIndex = 145
        Me.LblRecordedHeight.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Height"
        '
        'CboxWidthUnit
        '
        Me.CboxWidthUnit.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!)
        Me.CboxWidthUnit.FormattingEnabled = True
        Me.CboxWidthUnit.Items.AddRange(New Object() {"Inch", "Feet", "Millimetre", "Centimetre", "Metre"})
        Me.CboxWidthUnit.Location = New System.Drawing.Point(194, 201)
        Me.CboxWidthUnit.Name = "CboxWidthUnit"
        Me.CboxWidthUnit.Size = New System.Drawing.Size(79, 21)
        Me.CboxWidthUnit.TabIndex = 150
        '
        'TxtBxWidth
        '
        Me.TxtBxWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TxtBxWidth.Location = New System.Drawing.Point(83, 201)
        Me.TxtBxWidth.Name = "TxtBxWidth"
        Me.TxtBxWidth.Size = New System.Drawing.Size(112, 21)
        Me.TxtBxWidth.TabIndex = 149
        '
        'LblRecordedWidth
        '
        Me.LblRecordedWidth.AutoSize = True
        Me.LblRecordedWidth.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedWidth.Location = New System.Drawing.Point(29, 196)
        Me.LblRecordedWidth.Name = "LblRecordedWidth"
        Me.LblRecordedWidth.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedWidth.TabIndex = 148
        Me.LblRecordedWidth.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Width"
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'AddSize
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(301, 342)
        Me.Controls.Add(Me.LblRecordedLength)
        Me.Controls.Add(Me.CboxWidthUnit)
        Me.Controls.Add(Me.TxtBxWidth)
        Me.Controls.Add(Me.LblRecordedWidth)
        Me.Controls.Add(Me.CboxHeightUnit)
        Me.Controls.Add(Me.TxtBxHeight)
        Me.Controls.Add(Me.LblRecordedHeight)
        Me.Controls.Add(Me.RdBtnBackdate)
        Me.Controls.Add(Me.RdBtnNow)
        Me.Controls.Add(Me.CboxLengthUnit)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DatePicker)
        Me.Controls.Add(Me.TxtBxLength)
        Me.Controls.Add(Me.BtnAddSize)
        Me.Controls.Add(Me.LblAddSize)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "AddSize"
        Me.Text = "Add Pet Size Reading"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents RdBtnBackdate As RadioButton
    Friend WithEvents RdBtnNow As RadioButton
    Friend WithEvents CboxLengthUnit As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents DatePicker As DateTimePicker
    Friend WithEvents TxtBxLength As TextBox
    Friend WithEvents LblRecordedLength As Label
    Friend WithEvents BtnAddSize As Button
    Friend WithEvents LblAddSize As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CboxHeightUnit As ComboBox
    Friend WithEvents TxtBxHeight As TextBox
    Friend WithEvents LblRecordedHeight As Label
    Friend WithEvents CboxWidthUnit As ComboBox
    Friend WithEvents TxtBxWidth As TextBox
    Friend WithEvents LblRecordedWidth As Label
    Friend WithEvents ErrorProvider As ErrorProvider
End Class
