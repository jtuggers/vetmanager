﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ClientHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RdBtnAllAnimals = New System.Windows.Forms.RadioButton()
        Me.LblRowCount = New System.Windows.Forms.Label()
        Me.BtnRefresh = New System.Windows.Forms.Button()
        Me.RdBtnDeceased = New System.Windows.Forms.RadioButton()
        Me.RdBtnLiving = New System.Windows.Forms.RadioButton()
        Me.BtnViewPet = New System.Windows.Forms.Button()
        Me.DGVPetManager = New System.Windows.Forms.DataGridView()
        Me.LblClientHome = New System.Windows.Forms.Label()
        Me.BtnLogOut = New System.Windows.Forms.Button()
        Me.LblSearch = New System.Windows.Forms.Label()
        Me.CBSearchFor = New System.Windows.Forms.ComboBox()
        Me.BtnSearch = New System.Windows.Forms.Button()
        Me.TxtBxSearch = New System.Windows.Forms.TextBox()
        Me.BtnAccount = New System.Windows.Forms.Button()
        CType(Me.DGVPetManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(614, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(57, 20)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'RdBtnAllAnimals
        '
        Me.RdBtnAllAnimals.AutoSize = True
        Me.RdBtnAllAnimals.Checked = True
        Me.RdBtnAllAnimals.Location = New System.Drawing.Point(23, 66)
        Me.RdBtnAllAnimals.Name = "RdBtnAllAnimals"
        Me.RdBtnAllAnimals.Size = New System.Drawing.Size(60, 17)
        Me.RdBtnAllAnimals.TabIndex = 17
        Me.RdBtnAllAnimals.TabStop = True
        Me.RdBtnAllAnimals.Text = "All Pets"
        Me.RdBtnAllAnimals.UseVisualStyleBackColor = True
        '
        'LblRowCount
        '
        Me.LblRowCount.AutoSize = True
        Me.LblRowCount.Location = New System.Drawing.Point(20, 150)
        Me.LblRowCount.Name = "LblRowCount"
        Me.LblRowCount.Size = New System.Drawing.Size(64, 13)
        Me.LblRowCount.TabIndex = 26
        Me.LblRowCount.Text = "RowsFound"
        '
        'BtnRefresh
        '
        Me.BtnRefresh.Location = New System.Drawing.Point(373, 39)
        Me.BtnRefresh.Name = "BtnRefresh"
        Me.BtnRefresh.Size = New System.Drawing.Size(87, 35)
        Me.BtnRefresh.TabIndex = 27
        Me.BtnRefresh.Text = "Refresh"
        Me.BtnRefresh.UseVisualStyleBackColor = True
        '
        'RdBtnDeceased
        '
        Me.RdBtnDeceased.AutoSize = True
        Me.RdBtnDeceased.Location = New System.Drawing.Point(157, 66)
        Me.RdBtnDeceased.Name = "RdBtnDeceased"
        Me.RdBtnDeceased.Size = New System.Drawing.Size(74, 17)
        Me.RdBtnDeceased.TabIndex = 19
        Me.RdBtnDeceased.Text = "Deceased"
        Me.RdBtnDeceased.UseVisualStyleBackColor = True
        '
        'RdBtnLiving
        '
        Me.RdBtnLiving.AutoSize = True
        Me.RdBtnLiving.Location = New System.Drawing.Point(95, 66)
        Me.RdBtnLiving.Name = "RdBtnLiving"
        Me.RdBtnLiving.Size = New System.Drawing.Size(48, 17)
        Me.RdBtnLiving.TabIndex = 18
        Me.RdBtnLiving.Text = "Alive"
        Me.RdBtnLiving.UseVisualStyleBackColor = True
        '
        'BtnViewPet
        '
        Me.BtnViewPet.Location = New System.Drawing.Point(373, 87)
        Me.BtnViewPet.Name = "BtnViewPet"
        Me.BtnViewPet.Size = New System.Drawing.Size(87, 59)
        Me.BtnViewPet.TabIndex = 29
        Me.BtnViewPet.Text = "View Details"
        Me.BtnViewPet.UseVisualStyleBackColor = True
        '
        'DGVPetManager
        '
        Me.DGVPetManager.AllowUserToAddRows = False
        Me.DGVPetManager.AllowUserToDeleteRows = False
        Me.DGVPetManager.AllowUserToResizeColumns = False
        Me.DGVPetManager.AllowUserToResizeRows = False
        Me.DGVPetManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVPetManager.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGVPetManager.Location = New System.Drawing.Point(20, 167)
        Me.DGVPetManager.MultiSelect = False
        Me.DGVPetManager.Name = "DGVPetManager"
        Me.DGVPetManager.ReadOnly = True
        Me.DGVPetManager.RowHeadersVisible = False
        Me.DGVPetManager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVPetManager.ShowCellErrors = False
        Me.DGVPetManager.ShowCellToolTips = False
        Me.DGVPetManager.ShowEditingIcon = False
        Me.DGVPetManager.ShowRowErrors = False
        Me.DGVPetManager.Size = New System.Drawing.Size(567, 232)
        Me.DGVPetManager.TabIndex = 31
        '
        'LblClientHome
        '
        Me.LblClientHome.AutoSize = True
        Me.LblClientHome.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblClientHome.Location = New System.Drawing.Point(12, 34)
        Me.LblClientHome.Name = "LblClientHome"
        Me.LblClientHome.Size = New System.Drawing.Size(117, 26)
        Me.LblClientHome.TabIndex = 16
        Me.LblClientHome.Text = "Client Home"
        '
        'BtnLogOut
        '
        Me.BtnLogOut.Location = New System.Drawing.Point(483, 39)
        Me.BtnLogOut.Name = "BtnLogOut"
        Me.BtnLogOut.Size = New System.Drawing.Size(87, 35)
        Me.BtnLogOut.TabIndex = 28
        Me.BtnLogOut.Text = "Log Out"
        Me.BtnLogOut.UseVisualStyleBackColor = True
        '
        'LblSearch
        '
        Me.LblSearch.AutoSize = True
        Me.LblSearch.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSearch.Location = New System.Drawing.Point(19, 89)
        Me.LblSearch.Name = "LblSearch"
        Me.LblSearch.Size = New System.Drawing.Size(71, 19)
        Me.LblSearch.TabIndex = 22
        Me.LblSearch.Text = "Search by"
        '
        'CBSearchFor
        '
        Me.CBSearchFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBSearchFor.FormattingEnabled = True
        Me.CBSearchFor.Items.AddRange(New Object() {"Pet ID", "Pet Name", "Animal", "Breed", "Microchip Number"})
        Me.CBSearchFor.Location = New System.Drawing.Point(92, 89)
        Me.CBSearchFor.MaxDropDownItems = 5
        Me.CBSearchFor.Name = "CBSearchFor"
        Me.CBSearchFor.Size = New System.Drawing.Size(253, 21)
        Me.CBSearchFor.TabIndex = 23
        '
        'BtnSearch
        '
        Me.BtnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.BtnSearch.Location = New System.Drawing.Point(270, 121)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(75, 26)
        Me.BtnSearch.TabIndex = 25
        Me.BtnSearch.Text = "Search"
        Me.BtnSearch.UseVisualStyleBackColor = True
        '
        'TxtBxSearch
        '
        Me.TxtBxSearch.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxSearch.Location = New System.Drawing.Point(20, 121)
        Me.TxtBxSearch.Name = "TxtBxSearch"
        Me.TxtBxSearch.Size = New System.Drawing.Size(244, 26)
        Me.TxtBxSearch.TabIndex = 24
        '
        'BtnAccount
        '
        Me.BtnAccount.Location = New System.Drawing.Point(483, 87)
        Me.BtnAccount.Name = "BtnAccount"
        Me.BtnAccount.Size = New System.Drawing.Size(87, 59)
        Me.BtnAccount.TabIndex = 30
        Me.BtnAccount.Text = "My Account"
        Me.BtnAccount.UseVisualStyleBackColor = True
        '
        'ClientHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 417)
        Me.Controls.Add(Me.RdBtnAllAnimals)
        Me.Controls.Add(Me.LblRowCount)
        Me.Controls.Add(Me.BtnRefresh)
        Me.Controls.Add(Me.BtnAccount)
        Me.Controls.Add(Me.RdBtnDeceased)
        Me.Controls.Add(Me.RdBtnLiving)
        Me.Controls.Add(Me.BtnSearch)
        Me.Controls.Add(Me.BtnViewPet)
        Me.Controls.Add(Me.CBSearchFor)
        Me.Controls.Add(Me.TxtBxSearch)
        Me.Controls.Add(Me.LblSearch)
        Me.Controls.Add(Me.DGVPetManager)
        Me.Controls.Add(Me.LblClientHome)
        Me.Controls.Add(Me.BtnLogOut)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "ClientHome"
        Me.Text = "Client Home"
        CType(Me.DGVPetManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RdBtnAllAnimals As RadioButton
    Friend WithEvents LblRowCount As Label
    Friend WithEvents BtnRefresh As Button
    Friend WithEvents RdBtnDeceased As RadioButton
    Friend WithEvents RdBtnLiving As RadioButton
    Friend WithEvents BtnViewPet As Button
    Friend WithEvents DGVPetManager As DataGridView
    Friend WithEvents LblClientHome As Label
    Friend WithEvents BtnLogOut As Button
    Friend WithEvents LblSearch As Label
    Friend WithEvents CBSearchFor As ComboBox
    Friend WithEvents BtnSearch As Button
    Friend WithEvents TxtBxSearch As TextBox
    Friend WithEvents BtnAccount As Button
End Class
