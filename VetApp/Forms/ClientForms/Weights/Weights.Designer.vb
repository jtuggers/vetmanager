﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Weights
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ListBxHistory = New System.Windows.Forms.ListBox()
        Me.LblWeightHistory = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnAddWeight = New System.Windows.Forms.Button()
        Me.LblWeightID = New System.Windows.Forms.Label()
        Me.TxtBxReadingID = New System.Windows.Forms.TextBox()
        Me.TxtBxWeight = New System.Windows.Forms.TextBox()
        Me.LblRecordedWeight = New System.Windows.Forms.Label()
        Me.TxtBxKG = New System.Windows.Forms.TextBox()
        Me.LblKG = New System.Windows.Forms.Label()
        Me.TxtBxUnits = New System.Windows.Forms.TextBox()
        Me.LblAddedUser = New System.Windows.Forms.Label()
        Me.TxtBxAddedBy = New System.Windows.Forms.TextBox()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBxHistory
        '
        Me.ListBxHistory.FormattingEnabled = True
        Me.ListBxHistory.Location = New System.Drawing.Point(21, 94)
        Me.ListBxHistory.Name = "ListBxHistory"
        Me.ListBxHistory.ScrollAlwaysVisible = True
        Me.ListBxHistory.Size = New System.Drawing.Size(173, 147)
        Me.ListBxHistory.TabIndex = 0
        '
        'LblWeightHistory
        '
        Me.LblWeightHistory.AutoSize = True
        Me.LblWeightHistory.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWeightHistory.Location = New System.Drawing.Point(38, 43)
        Me.LblWeightHistory.Name = "LblWeightHistory"
        Me.LblWeightHistory.Size = New System.Drawing.Size(139, 26)
        Me.LblWeightHistory.TabIndex = 112
        Me.LblWeightHistory.Text = "Weight History"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(354, 43)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 35)
        Me.BtnClose.TabIndex = 111
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(476, 24)
        Me.MenuStrip1.TabIndex = 110
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'BtnAddWeight
        '
        Me.BtnAddWeight.Location = New System.Drawing.Point(239, 43)
        Me.BtnAddWeight.Name = "BtnAddWeight"
        Me.BtnAddWeight.Size = New System.Drawing.Size(92, 35)
        Me.BtnAddWeight.TabIndex = 113
        Me.BtnAddWeight.Text = "Add Weight Update"
        Me.BtnAddWeight.UseVisualStyleBackColor = True
        '
        'LblWeightID
        '
        Me.LblWeightID.AutoSize = True
        Me.LblWeightID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWeightID.Location = New System.Drawing.Point(205, 95)
        Me.LblWeightID.Name = "LblWeightID"
        Me.LblWeightID.Size = New System.Drawing.Size(47, 26)
        Me.LblWeightID.TabIndex = 114
        Me.LblWeightID.Text = "Reading" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ID"
        '
        'TxtBxReadingID
        '
        Me.TxtBxReadingID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxReadingID.Location = New System.Drawing.Point(259, 99)
        Me.TxtBxReadingID.Name = "TxtBxReadingID"
        Me.TxtBxReadingID.ReadOnly = True
        Me.TxtBxReadingID.Size = New System.Drawing.Size(187, 20)
        Me.TxtBxReadingID.TabIndex = 115
        '
        'TxtBxWeight
        '
        Me.TxtBxWeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxWeight.Location = New System.Drawing.Point(259, 137)
        Me.TxtBxWeight.Name = "TxtBxWeight"
        Me.TxtBxWeight.ReadOnly = True
        Me.TxtBxWeight.Size = New System.Drawing.Size(133, 20)
        Me.TxtBxWeight.TabIndex = 117
        '
        'LblRecordedWeight
        '
        Me.LblRecordedWeight.AutoSize = True
        Me.LblRecordedWeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedWeight.Location = New System.Drawing.Point(205, 133)
        Me.LblRecordedWeight.Name = "LblRecordedWeight"
        Me.LblRecordedWeight.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedWeight.TabIndex = 116
        Me.LblRecordedWeight.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Weight"
        '
        'TxtBxKG
        '
        Me.TxtBxKG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxKG.Location = New System.Drawing.Point(259, 175)
        Me.TxtBxKG.Name = "TxtBxKG"
        Me.TxtBxKG.ReadOnly = True
        Me.TxtBxKG.Size = New System.Drawing.Size(187, 20)
        Me.TxtBxKG.TabIndex = 119
        '
        'LblKG
        '
        Me.LblKG.AutoSize = True
        Me.LblKG.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKG.Location = New System.Drawing.Point(205, 178)
        Me.LblKG.Name = "LblKG"
        Me.LblKG.Size = New System.Drawing.Size(34, 13)
        Me.LblKG.TabIndex = 118
        Me.LblKG.Text = "In KG"
        '
        'TxtBxUnits
        '
        Me.TxtBxUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxUnits.Location = New System.Drawing.Point(391, 137)
        Me.TxtBxUnits.Name = "TxtBxUnits"
        Me.TxtBxUnits.ReadOnly = True
        Me.TxtBxUnits.Size = New System.Drawing.Size(55, 20)
        Me.TxtBxUnits.TabIndex = 120
        '
        'LblAddedUser
        '
        Me.LblAddedUser.AutoSize = True
        Me.LblAddedUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddedUser.Location = New System.Drawing.Point(205, 213)
        Me.LblAddedUser.Name = "LblAddedUser"
        Me.LblAddedUser.Size = New System.Drawing.Size(38, 26)
        Me.LblAddedUser.TabIndex = 121
        Me.LblAddedUser.Text = "Added" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "By"
        '
        'TxtBxAddedBy
        '
        Me.TxtBxAddedBy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddedBy.Location = New System.Drawing.Point(259, 213)
        Me.TxtBxAddedBy.Name = "TxtBxAddedBy"
        Me.TxtBxAddedBy.ReadOnly = True
        Me.TxtBxAddedBy.Size = New System.Drawing.Size(187, 20)
        Me.TxtBxAddedBy.TabIndex = 123
        '
        'Weights
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(476, 256)
        Me.Controls.Add(Me.TxtBxAddedBy)
        Me.Controls.Add(Me.LblAddedUser)
        Me.Controls.Add(Me.TxtBxUnits)
        Me.Controls.Add(Me.TxtBxKG)
        Me.Controls.Add(Me.LblKG)
        Me.Controls.Add(Me.TxtBxWeight)
        Me.Controls.Add(Me.LblRecordedWeight)
        Me.Controls.Add(Me.TxtBxReadingID)
        Me.Controls.Add(Me.LblWeightID)
        Me.Controls.Add(Me.BtnAddWeight)
        Me.Controls.Add(Me.LblWeightHistory)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.ListBxHistory)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Weights"
        Me.Text = "Weights"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ListBxHistory As ListBox
    Friend WithEvents LblWeightHistory As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BtnAddWeight As Button
    Friend WithEvents LblWeightID As Label
    Friend WithEvents TxtBxReadingID As TextBox
    Friend WithEvents TxtBxWeight As TextBox
    Friend WithEvents LblRecordedWeight As Label
    Friend WithEvents TxtBxKG As TextBox
    Friend WithEvents LblKG As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents TxtBxUnits As TextBox
    Friend WithEvents LblAddedUser As Label
    Friend WithEvents TxtBxAddedBy As TextBox
End Class
