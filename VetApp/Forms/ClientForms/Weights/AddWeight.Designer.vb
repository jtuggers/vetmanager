﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class AddWeight
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TxtBxWeight = New System.Windows.Forms.TextBox()
        Me.LblRecordedWeight = New System.Windows.Forms.Label()
        Me.BtnAddWeight = New System.Windows.Forms.Button()
        Me.LblAddWeight = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DatePicker = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CboxUnits = New System.Windows.Forms.ComboBox()
        Me.RdBtnNow = New System.Windows.Forms.RadioButton()
        Me.RdBtnBackdate = New System.Windows.Forms.RadioButton()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.MenuStrip1.SuspendLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TxtBxWeight
        '
        Me.TxtBxWeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.TxtBxWeight.Location = New System.Drawing.Point(85, 165)
        Me.TxtBxWeight.Name = "TxtBxWeight"
        Me.TxtBxWeight.Size = New System.Drawing.Size(112, 21)
        Me.TxtBxWeight.TabIndex = 128
        '
        'LblRecordedWeight
        '
        Me.LblRecordedWeight.AutoSize = True
        Me.LblRecordedWeight.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblRecordedWeight.Location = New System.Drawing.Point(31, 160)
        Me.LblRecordedWeight.Name = "LblRecordedWeight"
        Me.LblRecordedWeight.Size = New System.Drawing.Size(54, 26)
        Me.LblRecordedWeight.TabIndex = 127
        Me.LblRecordedWeight.Text = "Recorded" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Weight"
        '
        'BtnAddWeight
        '
        Me.BtnAddWeight.Location = New System.Drawing.Point(85, 206)
        Me.BtnAddWeight.Name = "BtnAddWeight"
        Me.BtnAddWeight.Size = New System.Drawing.Size(181, 35)
        Me.BtnAddWeight.TabIndex = 124
        Me.BtnAddWeight.Text = "Add Weight Update"
        Me.BtnAddWeight.UseVisualStyleBackColor = True
        '
        'LblAddWeight
        '
        Me.LblAddWeight.AutoSize = True
        Me.LblAddWeight.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddWeight.Location = New System.Drawing.Point(29, 43)
        Me.LblAddWeight.Name = "LblAddWeight"
        Me.LblAddWeight.Size = New System.Drawing.Size(112, 26)
        Me.LblAddWeight.TabIndex = 123
        Me.LblAddWeight.Text = "Add Weight"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(174, 43)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 35)
        Me.BtnClose.TabIndex = 122
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(303, 24)
        Me.MenuStrip1.TabIndex = 121
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'DatePicker
        '
        Me.DatePicker.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DatePicker.Location = New System.Drawing.Point(85, 124)
        Me.DatePicker.MinDate = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.DatePicker.Name = "DatePicker"
        Me.DatePicker.Size = New System.Drawing.Size(181, 21)
        Me.DatePicker.TabIndex = 129
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(31, 109)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 26)
        Me.Label1.TabIndex = 130
        Me.Label1.Text = "Reading" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Date"
        '
        'CboxUnits
        '
        Me.CboxUnits.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.5!)
        Me.CboxUnits.FormattingEnabled = True
        Me.CboxUnits.Items.AddRange(New Object() {"Tonne", "Kilogram", "Gram", "Milligram", "Stone", "Pound", "Ounce"})
        Me.CboxUnits.Location = New System.Drawing.Point(196, 165)
        Me.CboxUnits.Name = "CboxUnits"
        Me.CboxUnits.Size = New System.Drawing.Size(70, 21)
        Me.CboxUnits.TabIndex = 131
        '
        'RdBtnNow
        '
        Me.RdBtnNow.AutoSize = True
        Me.RdBtnNow.Location = New System.Drawing.Point(85, 99)
        Me.RdBtnNow.Name = "RdBtnNow"
        Me.RdBtnNow.Size = New System.Drawing.Size(47, 17)
        Me.RdBtnNow.TabIndex = 132
        Me.RdBtnNow.TabStop = True
        Me.RdBtnNow.Text = "Now"
        Me.RdBtnNow.UseVisualStyleBackColor = True
        '
        'RdBtnBackdate
        '
        Me.RdBtnBackdate.AutoSize = True
        Me.RdBtnBackdate.Location = New System.Drawing.Point(138, 99)
        Me.RdBtnBackdate.Name = "RdBtnBackdate"
        Me.RdBtnBackdate.Size = New System.Drawing.Size(79, 17)
        Me.RdBtnBackdate.TabIndex = 133
        Me.RdBtnBackdate.TabStop = True
        Me.RdBtnBackdate.Text = "Backdating"
        Me.RdBtnBackdate.UseVisualStyleBackColor = True
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'AddWeight
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(303, 273)
        Me.Controls.Add(Me.RdBtnBackdate)
        Me.Controls.Add(Me.RdBtnNow)
        Me.Controls.Add(Me.CboxUnits)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DatePicker)
        Me.Controls.Add(Me.TxtBxWeight)
        Me.Controls.Add(Me.LblRecordedWeight)
        Me.Controls.Add(Me.BtnAddWeight)
        Me.Controls.Add(Me.LblAddWeight)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "AddWeight"
        Me.Text = "Add Pet Weight Reading"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtBxWeight As TextBox
    Friend WithEvents LblRecordedWeight As Label
    Friend WithEvents BtnAddWeight As Button
    Friend WithEvents LblAddWeight As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DatePicker As DateTimePicker
    Friend WithEvents Label1 As Label
    Friend WithEvents CboxUnits As ComboBox
    Friend WithEvents RdBtnNow As RadioButton
    Friend WithEvents RdBtnBackdate As RadioButton
    Friend WithEvents ErrorProvider As ErrorProvider
End Class
