﻿Public Class AddWeight
    Dim User As New User
    Dim AddedDate As String
    Dim Validation As New Validation
    Private Sub AddWeight_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RdBtnNow.Checked = True
        DatePicker.Enabled = False
    End Sub

    Private Sub RdBtnNow_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnNow.CheckedChanged
        DatePicker.Enabled = False
    End Sub

    Private Sub RdBtnBackdate_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnBackdate.CheckedChanged
        DatePicker.Enabled = True
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub


    Private Sub BtnAddWeight_Click(sender As Object, e As EventArgs) Handles BtnAddWeight.Click
        If Len(TxtBxWeight.Text) = 0 Then
            ErrorProvider.SetError(CboxUnits, "Please enter Weight value.")
            Return
        Else
            ErrorProvider.SetError(CboxUnits, "")
        End If

        If CboxUnits.SelectedIndex = -1 Then
            ErrorProvider.SetError(CboxUnits, "Please select units.")
            Return
        Else
            ErrorProvider.SetError(CboxUnits, "")
        End If

        If RdBtnNow.Checked = True Then
            AddedDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
        ElseIf RdBtnBackdate.Checked = True Then
            AddedDate = DatePicker.Value.ToString()
        End If

        If Validation.PetVals(TxtBxWeight.Text) = False Then
            ErrorProvider.SetError(CboxUnits, "Invalid weight. Please ensure only numbers and periods are used.")
            Return
        ElseIf Validation.PetVals(TxtBxWeight.Text) = True Then
            ErrorProvider.SetError(CboxUnits, "")
        End If

        Try
            User.AddWeight(AddedDate, TxtBxWeight.Text, CboxUnits.Text)
            MessageBox.Show("Weight recording added successfully!")
            Me.Close()

        Catch
            MessageBox.Show("An error occured and the record could not be added. Please try again later.")
        End Try
    End Sub
End Class