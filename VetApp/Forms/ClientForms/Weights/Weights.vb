﻿Public Class Weights
    Dim User As New User
    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub Weights_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Details As List(Of String) = User.GetWeights()
        For Each Item In Details
            ListBxHistory.Items.Add(Item)
        Next
    End Sub

    Private Sub ListBxHistory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBxHistory.SelectedIndexChanged
        Dim Selected As String = ListBxHistory.GetItemText(ListBxHistory.SelectedItem)

        Dim Details() As String = User.GetWeightDetails(Selected)

        TxtBxReadingID.Text = Details(0)
        TxtBxWeight.Text = Details(1)
        TxtBxAddedBy.Text = User.GetFullname(Details(2))
        TxtBxUnits.Text = Details(3)
        TxtBxKG.Text = Details(4)

    End Sub

    Private Sub BtnAddWeight_Click(sender As Object, e As EventArgs) Handles BtnAddWeight.Click
        AddWeight.ShowDialog()
    End Sub
End Class