﻿Imports System.ComponentModel

Public Class ChangeEmail
    Dim user As New User
    Dim Validation As New Validation

    Private Sub BtnSubmit_Click(sender As Object, e As EventArgs) Handles BtnSubmit.Click
        Dim UserID As Integer = Globals.ModifyID
        Dim OldEmail As String = user.GetEmail()
        Dim NewEmail As String = TxtBxEmail.Text
        Dim ConfEmail As String = TxtBxConfirmEmail.Text
        Dim Password As String = TxtBxPass.Text


        TxtBxEmail.Focus()
        TxtBxConfirmEmail.Focus()
        TxtBxPass.Focus()

        For Each control As Control In Me.Controls
            If TypeName(control) = "TextBox" Then
                Dim txtbx As TextBox = CType(control, TextBox)
                If Not ErrorProvider.GetError(txtbx) = "" Then
                    MessageBox.Show("Validation Errors found, please correct before continuing.", "Validation Errors", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If
            End If
        Next



        If user.LoginTest(OldEmail, Password) = True Then
            Try
                user.ChangeUserPass(NewEmail, Password)
            Catch
                MessageBox.Show("Email could not be changed.")
            End Try


        Else
            MessageBox.Show("Authentication failed, invalid password provided.")
        End If


        ModifyUser.Close()
        UserManager.Show()
        Me.Close()










    End Sub



    Private Sub TxtBxEmail_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxEmail.Validating
        If Len(TxtBxEmail.Text) = 0 Then
            ErrorProvider.SetError(TxtBxEmail, "This field is required")

        ElseIf Validation.Email(TxtBxEmail.Text) = True Then
            ErrorProvider.SetError(TxtBxEmail, "")
        Else
            ErrorProvider.SetError(TxtBxEmail, "Email format invalid")

        End If
    End Sub


    Private Sub TxtBxConfirmEmail_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxConfirmEmail.Validating

        If Not TxtBxConfirmEmail.Text = TxtBxEmail.Text Then
            ErrorProvider.SetError(TxtBxConfirmEmail, "Emails must match")

        Else
            ErrorProvider.SetError(TxtBxConfirmEmail, "")
        End If
    End Sub

    Private Sub TxtBxPass_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxPass.Validating
        If Len(TxtBxPass.Text) = 0 Then
            ErrorProvider.SetError(TxtBxPass, "Password is required.")
        Else
            ErrorProvider.SetError(TxtBxPass, "")
        End If
    End Sub


End Class