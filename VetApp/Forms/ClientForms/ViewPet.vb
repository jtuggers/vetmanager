﻿Imports System.ComponentModel
Public Class ViewPet
    Dim User As New User

    Private Sub ViewPet_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Globals.PetID = 0 Then
            Return
        End If

        Dim Details() As String

        Details = User.GetPetDetails()

        Globals.ModifyID = Details(0)
        TxtBxEmail.Text = User.GetEmail()

        TxtBxPetName.Text = Details(1)

        TxtBxPetDob.Text = Details(2).Substring(0, 10)


        TxtBxDescription.Text = Details(3)
        TxtBxAnimal.Text = Details(4)
        TxtBxBreed.Text = Details(5)

        TxtBxMChipNo.Text = Details(6)

        If Details(7) = "0" Then
            RdBtnLiving.Checked = True
        Else
            RdBtnDeceased.Checked = True
        End If

    End Sub

    Private Sub BtnBack_Click(sender As Object, e As EventArgs) Handles BtnBack.Click
        Me.Close()
    End Sub

    Private Sub BtnWeights_Click(sender As Object, e As EventArgs) Handles BtnWeights.Click
        Weights.ShowDialog()
    End Sub

    Private Sub BtnSize_Click(sender As Object, e As EventArgs) Handles BtnSize.Click
        Sizes.ShowDialog()
    End Sub

    Private Sub BtnComments_Click(sender As Object, e As EventArgs) Handles BtnComments.Click
        Comments.ShowDialog()
    End Sub
End Class