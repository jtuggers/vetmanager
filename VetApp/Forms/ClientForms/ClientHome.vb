﻿Imports MySql.Data.MySqlClient
Imports System.Security.Cryptography 'Imports the Cryptography libraries
Imports System.Text 'Imports the system text libraries
Imports System.ComponentModel

Public Class ClientHome
    Dim User As New User
    Dim LoadStatus As Integer

    Private Sub ClientHome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CBSearchFor.SelectedIndex = 0
        BtnRefresh.PerformClick()
        LoadStatus = 1
        BtnViewPet.Enabled = False
    End Sub


    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogoutToolStripMenuItem.Click
        Login.Show()
        Me.Close()
    End Sub


    Private Sub BtnLogOut_Click(sender As Object, e As EventArgs) Handles BtnLogOut.Click
        Login.Show()
        Me.Close()
    End Sub

    Private Sub BtnRefresh_Click(sender As Object, e As EventArgs) Handles BtnRefresh.Click
        DGVPetManager.Columns.Clear()
        Dim ds As New DataTable
        ds = User.GetPetList()
        DGVPetManager.DataSource = ds
        DGVPetManager.DataMember = ds.TableName

        For Each column As DataGridViewColumn In DGVPetManager.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        Dim ResizeColumn As DataGridViewColumn = DGVPetManager.Columns(0)
        ResizeColumn.Width = 50 'petid

        ResizeColumn = DGVPetManager.Columns(1)
        ResizeColumn.Width = 103 'petname

        ResizeColumn = DGVPetManager.Columns(2)
        ResizeColumn.Width = 104 'animal

        ResizeColumn = DGVPetManager.Columns(3)
        ResizeColumn.Width = 120 'Breed

        ResizeColumn = DGVPetManager.Columns(4)
        ResizeColumn.Width = 110 'M/chip

        ResizeColumn = DGVPetManager.Columns(5)
        ResizeColumn.Width = 60 'Deceased

        DGVPetManager.Columns(2).HeaderText = "Animal"
        DGVPetManager.Columns(3).HeaderText = "Breed"
        LblRowCount.Text = "Rows Found: " + DGVPetManager.RowCount.ToString()
    End Sub

    Dim SearchFor As String

    Private Sub DGVUserManager_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DGVPetManager.CellMouseDoubleClick
        If (e.ColumnIndex >= 0) And (e.RowIndex >= 0) Then
            BtnViewPet.Enabled = True
            Dim selectedRow = DGVPetManager.Rows(e.RowIndex)
            Dim value As String = DGVPetManager.Rows(e.RowIndex).Cells("PetID").Value
            Globals.PetID = value
            ViewPet.ShowDialog()

        End If

    End Sub


    Private Sub DGVUserManager_KeyDown(sender As Object, e As KeyEventArgs) Handles DGVPetManager.KeyDown
        If e.KeyCode = Keys.Enter Then
            If (DGVPetManager.CurrentCell.RowIndex >= 0) And (DGVPetManager.CurrentCell.ColumnIndex >= 0) Then
                BtnViewPet.Enabled = True
                Dim selectedRow = DGVPetManager.SelectedColumns
                Dim value As String = DGVPetManager.Rows(DGVPetManager.CurrentCell.RowIndex).Cells("PetID").Value
                Globals.PetID = value
                ViewPet.ShowDialog()
                'Me.Close()
            End If
        End If
    End Sub

    Private Sub DGVUserManager_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVPetManager.CellClick
        If (DGVPetManager.CurrentCell.RowIndex >= 0) And (DGVPetManager.CurrentCell.ColumnIndex >= 0) Then
            BtnViewPet.Enabled = True
            Dim selectedRow = DGVPetManager.SelectedColumns
            Dim value As String = DGVPetManager.Rows(DGVPetManager.CurrentCell.RowIndex).Cells("PetID").Value

            Globals.PetID = value
        End If
    End Sub

    Private Sub CBSearchFor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBSearchFor.SelectedIndexChanged
        SearchFor = CBSearchFor.Text
        BtnSearch.PerformClick()
    End Sub


    Dim value As String
    Dim result As String

    Dim Validation As New Validation

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        If Not LoadStatus = 1 Then
            Return
        End If
        value = TxtBxSearch.Text
        value = value.Trim()
        If value = "" Then
            result = ""

        ElseIf SearchFor = "Pet ID" Then
            If (Len(value) <= 10) Then
                If Validation.CheckInt(value) = True Then
                    result = "CONVERT(PetID, 'System.String') LIKE '%" + TxtBxSearch.Text + "%'"
                Else
                    result = String.Format("PetID = '0'")
                End If
            Else
                result = String.Format("PetID = '0'")
            End If

        ElseIf SearchFor = "Pet Name" Then
            value = TxtBxSearch.Text
            If Validation.Name(value) = True Then
                result = String.Format("PetName LIKE '%{0}%'", value)
            Else
                result = String.Format("PetID = '0'")
            End If

        ElseIf SearchFor = "Animal" Then
            If Validation.CheckString(value) = True Then
                result = String.Format("Animals_AnimalID LIKE '%{0}%'", value)
            Else
                result = String.Format("PetID = '0'")
            End If

        ElseIf SearchFor = "Breed" Then
            If Validation.CheckString(value) = True Then
                result = String.Format("Breeds_BreedID LIKE '%{0}%'", value)
            Else
                result = String.Format("PetID = '0'")
            End If

        ElseIf SearchFor = "Microchip Number" Then
            If Validation.CheckString(value) = True Then
                result = String.Format("MicrochipNumber LIKE '%{0}%'", value)
            Else
                result = String.Format("PetID = '0'")
            End If
        End If

        If result = "" Then
            If RdBtnAllAnimals.Checked = True Then
                result = ""
            ElseIf RdBtnLiving.Checked Then
                result = "Deceased = '0'"
            ElseIf RdBtnDeceased.Checked Then
                result = "Deceased = '1'"
            End If
        Else
            If RdBtnAllAnimals.Checked = True Then
                result = result + ""
            ElseIf RdBtnLiving.Checked Then
                result = result + " AND Deceased = '0'"
            ElseIf RdBtnDeceased.Checked Then
                result = result + " AND Deceased = '1'"
            End If
        End If

        DGVPetManager.DataSource.DefaultView.RowFilter = result
        LblRowCount.Text = "Rows Found: " + DGVPetManager.RowCount.ToString()

    End Sub

    Private Sub TxtBxSearch_TextChanged(sender As Object, e As EventArgs) Handles TxtBxSearch.TextChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnAllAnimals_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnAllAnimals.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnLiving_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnLiving.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnDeceased_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnDeceased.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub BtnAccount_Click(sender As Object, e As EventArgs) Handles BtnAccount.Click
        MyAccount.ShowDialog()
    End Sub

    Private Sub BtnViewPet_Click(sender As Object, e As EventArgs) Handles BtnViewPet.Click
        ViewPet.ShowDialog()
    End Sub
End Class