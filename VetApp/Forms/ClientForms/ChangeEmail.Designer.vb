﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChangeEmail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LblModifyUser = New System.Windows.Forms.Label()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblConfEmail = New System.Windows.Forms.Label()
        Me.LblPassword = New System.Windows.Forms.Label()
        Me.TxtBxEmail = New System.Windows.Forms.TextBox()
        Me.TxtBxConfirmEmail = New System.Windows.Forms.TextBox()
        Me.TxtBxPass = New System.Windows.Forms.TextBox()
        Me.BtnSubmit = New System.Windows.Forms.Button()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LblModifyUser
        '
        Me.LblModifyUser.AutoSize = True
        Me.LblModifyUser.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblModifyUser.Location = New System.Drawing.Point(12, 9)
        Me.LblModifyUser.Name = "LblModifyUser"
        Me.LblModifyUser.Size = New System.Drawing.Size(125, 26)
        Me.LblModifyUser.TabIndex = 74
        Me.LblModifyUser.Text = "Modify Email"
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Location = New System.Drawing.Point(93, 61)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(57, 13)
        Me.LblEmail.TabIndex = 75
        Me.LblEmail.Text = "New Email"
        '
        'LblConfEmail
        '
        Me.LblConfEmail.AutoSize = True
        Me.LblConfEmail.Location = New System.Drawing.Point(55, 103)
        Me.LblConfEmail.Name = "LblConfEmail"
        Me.LblConfEmail.Size = New System.Drawing.Size(95, 13)
        Me.LblConfEmail.TabIndex = 76
        Me.LblConfEmail.Text = "Confirm New Email"
        '
        'LblPassword
        '
        Me.LblPassword.AutoSize = True
        Me.LblPassword.Location = New System.Drawing.Point(60, 145)
        Me.LblPassword.Name = "LblPassword"
        Me.LblPassword.Size = New System.Drawing.Size(90, 13)
        Me.LblPassword.TabIndex = 77
        Me.LblPassword.Text = "Current Password"
        '
        'TxtBxEmail
        '
        Me.TxtBxEmail.Location = New System.Drawing.Point(156, 58)
        Me.TxtBxEmail.Name = "TxtBxEmail"
        Me.TxtBxEmail.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxEmail.TabIndex = 78
        '
        'TxtBxConfirmEmail
        '
        Me.TxtBxConfirmEmail.Location = New System.Drawing.Point(156, 100)
        Me.TxtBxConfirmEmail.Name = "TxtBxConfirmEmail"
        Me.TxtBxConfirmEmail.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxConfirmEmail.TabIndex = 79
        '
        'TxtBxPass
        '
        Me.TxtBxPass.Location = New System.Drawing.Point(156, 142)
        Me.TxtBxPass.Name = "TxtBxPass"
        Me.TxtBxPass.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxPass.TabIndex = 81
        Me.TxtBxPass.UseSystemPasswordChar = True
        '
        'BtnSubmit
        '
        Me.BtnSubmit.Location = New System.Drawing.Point(156, 184)
        Me.BtnSubmit.Name = "BtnSubmit"
        Me.BtnSubmit.Size = New System.Drawing.Size(189, 23)
        Me.BtnSubmit.TabIndex = 82
        Me.BtnSubmit.Text = "Submit"
        Me.BtnSubmit.UseVisualStyleBackColor = True
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'ChangeEmail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(427, 261)
        Me.Controls.Add(Me.BtnSubmit)
        Me.Controls.Add(Me.TxtBxPass)
        Me.Controls.Add(Me.TxtBxConfirmEmail)
        Me.Controls.Add(Me.TxtBxEmail)
        Me.Controls.Add(Me.LblPassword)
        Me.Controls.Add(Me.LblConfEmail)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblModifyUser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "ChangeEmail"
        Me.Text = "Change Email"
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblModifyUser As Label
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblConfEmail As Label
    Friend WithEvents LblPassword As Label
    Friend WithEvents TxtBxEmail As TextBox
    Friend WithEvents TxtBxConfirmEmail As TextBox
    Friend WithEvents TxtBxPass As TextBox
    Friend WithEvents BtnSubmit As Button
    Friend WithEvents ErrorProvider As ErrorProvider
End Class
