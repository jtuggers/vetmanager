﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class DBConnSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.LblDBHostname = New System.Windows.Forms.Label()
        Me.LblDBUsername = New System.Windows.Forms.Label()
        Me.LblDBPassword = New System.Windows.Forms.Label()
        Me.LblDBDatabase = New System.Windows.Forms.Label()
        Me.TxtDBHostname = New System.Windows.Forms.TextBox()
        Me.TxtDBUsername = New System.Windows.Forms.TextBox()
        Me.TxtDBDatabase = New System.Windows.Forms.TextBox()
        Me.TxtDBPassword = New System.Windows.Forms.TextBox()
        Me.BtnDBTestConection = New System.Windows.Forms.Button()
        Me.BtnDBSubmit = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LblDBHostname
        '
        Me.LblDBHostname.AutoSize = True
        Me.LblDBHostname.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LblDBHostname.Location = New System.Drawing.Point(77, 53)
        Me.LblDBHostname.Name = "LblDBHostname"
        Me.LblDBHostname.Size = New System.Drawing.Size(72, 17)
        Me.LblDBHostname.TabIndex = 0
        Me.LblDBHostname.Text = "Hostname"
        '
        'LblDBUsername
        '
        Me.LblDBUsername.AutoSize = True
        Me.LblDBUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LblDBUsername.Location = New System.Drawing.Point(76, 96)
        Me.LblDBUsername.Name = "LblDBUsername"
        Me.LblDBUsername.Size = New System.Drawing.Size(73, 17)
        Me.LblDBUsername.TabIndex = 1
        Me.LblDBUsername.Text = "Username"
        '
        'LblDBPassword
        '
        Me.LblDBPassword.AutoSize = True
        Me.LblDBPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LblDBPassword.Location = New System.Drawing.Point(80, 140)
        Me.LblDBPassword.Name = "LblDBPassword"
        Me.LblDBPassword.Size = New System.Drawing.Size(69, 17)
        Me.LblDBPassword.TabIndex = 2
        Me.LblDBPassword.Text = "Password"
        '
        'LblDBDatabase
        '
        Me.LblDBDatabase.AutoSize = True
        Me.LblDBDatabase.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LblDBDatabase.Location = New System.Drawing.Point(80, 181)
        Me.LblDBDatabase.Name = "LblDBDatabase"
        Me.LblDBDatabase.Size = New System.Drawing.Size(69, 17)
        Me.LblDBDatabase.TabIndex = 3
        Me.LblDBDatabase.Text = "Database"
        '
        'TxtDBHostname
        '
        Me.TxtDBHostname.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.TxtDBHostname.Location = New System.Drawing.Point(155, 53)
        Me.TxtDBHostname.Name = "TxtDBHostname"
        Me.TxtDBHostname.Size = New System.Drawing.Size(163, 23)
        Me.TxtDBHostname.TabIndex = 4
        '
        'TxtDBUsername
        '
        Me.TxtDBUsername.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.TxtDBUsername.Location = New System.Drawing.Point(155, 96)
        Me.TxtDBUsername.Name = "TxtDBUsername"
        Me.TxtDBUsername.Size = New System.Drawing.Size(163, 23)
        Me.TxtDBUsername.TabIndex = 5
        '
        'TxtDBDatabase
        '
        Me.TxtDBDatabase.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.TxtDBDatabase.Location = New System.Drawing.Point(155, 181)
        Me.TxtDBDatabase.Name = "TxtDBDatabase"
        Me.TxtDBDatabase.Size = New System.Drawing.Size(163, 23)
        Me.TxtDBDatabase.TabIndex = 7
        '
        'TxtDBPassword
        '
        Me.TxtDBPassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.TxtDBPassword.Location = New System.Drawing.Point(155, 137)
        Me.TxtDBPassword.Name = "TxtDBPassword"
        Me.TxtDBPassword.Size = New System.Drawing.Size(163, 23)
        Me.TxtDBPassword.TabIndex = 8
        Me.TxtDBPassword.UseSystemPasswordChar = True
        '
        'BtnDBTestConection
        '
        Me.BtnDBTestConection.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnDBTestConection.Location = New System.Drawing.Point(44, 236)
        Me.BtnDBTestConection.Name = "BtnDBTestConection"
        Me.BtnDBTestConection.Size = New System.Drawing.Size(155, 34)
        Me.BtnDBTestConection.TabIndex = 8
        Me.BtnDBTestConection.Text = "Test Connection"
        Me.BtnDBTestConection.UseVisualStyleBackColor = True
        '
        'BtnDBSubmit
        '
        Me.BtnDBSubmit.Enabled = False
        Me.BtnDBSubmit.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.BtnDBSubmit.Location = New System.Drawing.Point(205, 236)
        Me.BtnDBSubmit.Name = "BtnDBSubmit"
        Me.BtnDBSubmit.Size = New System.Drawing.Size(155, 34)
        Me.BtnDBSubmit.TabIndex = 9
        Me.BtnDBSubmit.Text = "Save Configuration"
        Me.BtnDBSubmit.UseVisualStyleBackColor = True
        '
        'DBConnSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 317)
        Me.Controls.Add(Me.BtnDBSubmit)
        Me.Controls.Add(Me.BtnDBTestConection)
        Me.Controls.Add(Me.TxtDBPassword)
        Me.Controls.Add(Me.TxtDBDatabase)
        Me.Controls.Add(Me.TxtDBUsername)
        Me.Controls.Add(Me.TxtDBHostname)
        Me.Controls.Add(Me.LblDBDatabase)
        Me.Controls.Add(Me.LblDBPassword)
        Me.Controls.Add(Me.LblDBUsername)
        Me.Controls.Add(Me.LblDBHostname)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "DBConnSettings"
        Me.Text = "Database Connection Settings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LblDBHostname As Label
    Friend WithEvents LblDBUsername As Label
    Friend WithEvents LblDBPassword As Label
    Friend WithEvents LblDBDatabase As Label
    Friend WithEvents TxtDBHostname As TextBox
    Friend WithEvents TxtDBUsername As TextBox
    Friend WithEvents TxtDBDatabase As TextBox
    Friend WithEvents TxtDBPassword As TextBox
    Friend WithEvents BtnDBTestConection As Button
    Friend WithEvents BtnDBSubmit As Button
End Class
