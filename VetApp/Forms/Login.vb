﻿Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports MySql.Data.MySqlClient ' imports mysql library

Public Class Login

    Dim User As New User
    Dim Email As String
    Dim Password As String


    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load ' when main form loads, run this
        User.SQLConn()

        'ADD TO DOCUMENT PROBLEMS - USING LOCALHOST WAS RESULTING IN SLOW CONNECTIONS, SO USING 127.0.0.1 INSTEAD

    End Sub

    Private Sub DatabaseLocationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DatabaseLocationToolStripMenuItem.Click
        ConfigLogin.ShowDialog()
    End Sub

    Private Sub BtnLogin_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click

        If String.IsNullOrEmpty(TxtBxUsername.Text) Then

            MessageBox.Show("Please enter a valid Email.")
            ErrorProvider1.SetError(TxtBxUsername, "Email required")
            Return

        End If
        If String.IsNullOrEmpty(TxtBxPassword.Text) Then

            MessageBox.Show("Please enter a valid password.")
            ErrorProvider1.SetError(TxtBxPassword, "Password required")
            Return

        End If

        Email = TxtBxUsername.Text
        Password = TxtBxPassword.Text
        Dim loginSuccess As String = User.Login(Email, Password)
        Dim UserID As Integer
        If loginSuccess = False Then

            MessageBox.Show("Username or password incorrect." + vbCrLf + "Please try again.", "Authentication Failed", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TxtBxPassword.Text = Nothing
        ElseIf Integer.TryParse(loginSuccess, UserID) Then
            Globals.UserID = UserID
            Dim AccessLevel As Integer = User.GetAL(UserID)
            If AccessLevel = 3 Then
                MessageBox.Show("Your account has been locked." + vbCrLf + "Please contact the administrator.", "Account Locked", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ElseIf AccessLevel = 2 Then

                AdminHome.Show()
                Me.Close()
            ElseIf AccessLevel = 1 Then
                StaffHome.Show()
                Me.Close()
            ElseIf AccessLevel = 0 Then
                ClientHome.Show()
                Me.Close()
            Else
                MessageBox.Show("An error has occured. Please try again." + vbCrLf + "If the problem persists, please restart the program.", "Authentication Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If


        End If


    End Sub

    Private Sub TxtBxUsername_TextChanged(sender As Object, e As EventArgs) Handles TxtBxUsername.TextChanged
        ErrorProvider1.SetError(TxtBxUsername, "")
    End Sub

    Private Sub TxtBxPassword_TextChanged(sender As Object, e As EventArgs) Handles TxtBxPassword.TextChanged
        ErrorProvider1.SetError(TxtBxPassword, "")
    End Sub


End Class


'ADD TO DOCUMENT 10/08/2016
'ACCESS LEVEL 3 = DISABLED
'CHANGED "Else, clienthome.show" to:
'ElseIf AccessLevel = 0 Then
'ClientHome.Show()
'Me.Close()
'Else
'MessageBox.Show("An error has occured. Please try again." + vbCrLf + "If the problem persists, please restart the program.", "Authentication Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
'End If

