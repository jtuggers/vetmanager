﻿Public Class ViewUser
    Dim User As New User
    Private Sub BtnAdminBack_Click(sender As Object, e As EventArgs) Handles BtnAdminBack.Click
        Me.Close()
    End Sub

    Private Sub ViewUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If Globals.PreButton = "PetManager" Then
            BtnViewPets.Enabled = False
        Else
            BtnViewPets.Enabled = True
        End If


        Dim Details() As String = User.GetUserDetails()

        TxtBxEmail.Text = Details(0)
        CBTitle.Text = Details(1)
        TxtBxFirstName.Text = Details(2)
        TxtBxSurname.Text = Details(3)
        TxtBxDOB.Text = Details(4).Substring(0, 10)
        TxtBxHomePhone.Text = Details(5)
        TxtBxMobPhone.Text = Details(6)
        TxtBxWorkPhone.Text = Details(7)
        TxtBxAddNameNo.Text = Details(8)
        TxtBxAddRoad.Text = Details(9)
        TxtBxAddCity.Text = Details(10)
        TxtBxAddCounty.Text = Details(11)
        TxtBxAddCountry.Text = Details(12)
        TxtBxAddPostCode.Text = Details(13)
        CBPrivilageLevel.SelectedIndex = Details(14)

    End Sub

    Private Sub BtnEditDetails_Click(sender As Object, e As EventArgs) Handles BtnEditDetails.Click

        StaffModifyUser.ShowDialog()
    End Sub

    Private Sub BtnViewPets_Click(sender As Object, e As EventArgs) Handles BtnViewPets.Click
        UserPets.ShowDialog()
    End Sub
End Class