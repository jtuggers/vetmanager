﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class StaffHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BtnAdminAniBreedMan = New System.Windows.Forms.Button()
        Me.BtnAdminPetMan = New System.Windows.Forms.Button()
        Me.BtnAdminUsrMan = New System.Windows.Forms.Button()
        Me.LblAdminTools = New System.Windows.Forms.Label()
        Me.LblLoggedInName = New System.Windows.Forms.Label()
        Me.LblSignedInAs = New System.Windows.Forms.Label()
        Me.BtnAdminLogout = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BtnAdminAniBreedMan
        '
        Me.BtnAdminAniBreedMan.Location = New System.Drawing.Point(253, 105)
        Me.BtnAdminAniBreedMan.Name = "BtnAdminAniBreedMan"
        Me.BtnAdminAniBreedMan.Size = New System.Drawing.Size(112, 67)
        Me.BtnAdminAniBreedMan.TabIndex = 26
        Me.BtnAdminAniBreedMan.Text = "My Account"
        Me.BtnAdminAniBreedMan.UseVisualStyleBackColor = True
        '
        'BtnAdminPetMan
        '
        Me.BtnAdminPetMan.Location = New System.Drawing.Point(135, 105)
        Me.BtnAdminPetMan.Name = "BtnAdminPetMan"
        Me.BtnAdminPetMan.Size = New System.Drawing.Size(112, 67)
        Me.BtnAdminPetMan.TabIndex = 21
        Me.BtnAdminPetMan.Text = "Pet Manager"
        Me.BtnAdminPetMan.UseVisualStyleBackColor = True
        '
        'BtnAdminUsrMan
        '
        Me.BtnAdminUsrMan.Location = New System.Drawing.Point(17, 105)
        Me.BtnAdminUsrMan.Name = "BtnAdminUsrMan"
        Me.BtnAdminUsrMan.Size = New System.Drawing.Size(112, 67)
        Me.BtnAdminUsrMan.TabIndex = 20
        Me.BtnAdminUsrMan.Text = "User Manager"
        Me.BtnAdminUsrMan.UseVisualStyleBackColor = True
        '
        'LblAdminTools
        '
        Me.LblAdminTools.AutoSize = True
        Me.LblAdminTools.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAdminTools.Location = New System.Drawing.Point(12, 41)
        Me.LblAdminTools.Name = "LblAdminTools"
        Me.LblAdminTools.Size = New System.Drawing.Size(51, 26)
        Me.LblAdminTools.TabIndex = 19
        Me.LblAdminTools.Text = "Staff"
        '
        'LblLoggedInName
        '
        Me.LblLoggedInName.AutoSize = True
        Me.LblLoggedInName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLoggedInName.Location = New System.Drawing.Point(144, 57)
        Me.LblLoggedInName.Name = "LblLoggedInName"
        Me.LblLoggedInName.Size = New System.Drawing.Size(73, 16)
        Me.LblLoggedInName.TabIndex = 18
        Me.LblLoggedInName.Text = "First Name"
        '
        'LblSignedInAs
        '
        Me.LblSignedInAs.AutoSize = True
        Me.LblSignedInAs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSignedInAs.Location = New System.Drawing.Point(144, 41)
        Me.LblSignedInAs.Name = "LblSignedInAs"
        Me.LblSignedInAs.Size = New System.Drawing.Size(103, 16)
        Me.LblSignedInAs.TabIndex = 17
        Me.LblSignedInAs.Text = "Welcome Back,"
        '
        'BtnAdminLogout
        '
        Me.BtnAdminLogout.Location = New System.Drawing.Point(253, 41)
        Me.BtnAdminLogout.Name = "BtnAdminLogout"
        Me.BtnAdminLogout.Size = New System.Drawing.Size(112, 35)
        Me.BtnAdminLogout.TabIndex = 16
        Me.BtnAdminLogout.Text = "Sign Out"
        Me.BtnAdminLogout.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(104, 139)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 14
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(380, 24)
        Me.MenuStrip1.TabIndex = 15
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'StaffHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 196)
        Me.Controls.Add(Me.BtnAdminAniBreedMan)
        Me.Controls.Add(Me.BtnAdminPetMan)
        Me.Controls.Add(Me.BtnAdminUsrMan)
        Me.Controls.Add(Me.LblAdminTools)
        Me.Controls.Add(Me.LblLoggedInName)
        Me.Controls.Add(Me.LblSignedInAs)
        Me.Controls.Add(Me.BtnAdminLogout)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "StaffHome"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Staff Home"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnAdminAniBreedMan As Button
    Friend WithEvents BtnAdminPetMan As Button
    Friend WithEvents BtnAdminUsrMan As Button
    Friend WithEvents LblAdminTools As Label
    Friend WithEvents LblLoggedInName As Label
    Friend WithEvents LblSignedInAs As Label
    Friend WithEvents BtnAdminLogout As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
End Class
