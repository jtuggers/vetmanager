﻿Public Class StaffHome
    Dim User As New User
    Private Sub BtnAdminUsrMan_Click(sender As Object, e As EventArgs) Handles BtnAdminUsrMan.Click
        StaffUsrMgr.Show()
        Me.Close()
    End Sub

    Private Sub BtnAdminLogout_Click(sender As Object, e As EventArgs) Handles BtnAdminLogout.Click
        Login.Show()
        Me.Close()
    End Sub

    Private Sub StaffHome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Name As String = User.GetName(Globals.UserID)
        LblLoggedInName.Text = Name
    End Sub

    Private Sub BtnAdminAniBreedMan_Click(sender As Object, e As EventArgs) Handles BtnAdminAniBreedMan.Click
        MyAccount.ShowDialog()
    End Sub

    Private Sub BtnAdminPetMan_Click(sender As Object, e As EventArgs) Handles BtnAdminPetMan.Click
        PetManager.ShowDialog()
    End Sub
End Class