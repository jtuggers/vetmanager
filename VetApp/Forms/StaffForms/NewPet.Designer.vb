﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NewPet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CBAnimalList = New System.Windows.Forms.ComboBox()
        Me.BtnCreatePet = New System.Windows.Forms.Button()
        Me.BtnCancel = New System.Windows.Forms.Button()
        Me.TxtBxPetName = New System.Windows.Forms.TextBox()
        Me.LblPassword = New System.Windows.Forms.Label()
        Me.TxtBxMChipNo = New System.Windows.Forms.TextBox()
        Me.LblMobPhone = New System.Windows.Forms.Label()
        Me.LblHomePhone = New System.Windows.Forms.Label()
        Me.LblDOB = New System.Windows.Forms.Label()
        Me.TxtBxDescription = New System.Windows.Forms.TextBox()
        Me.LblSurname = New System.Windows.Forms.Label()
        Me.TxtBxPetDob = New System.Windows.Forms.TextBox()
        Me.LblFirstName = New System.Windows.Forms.Label()
        Me.TxtBxEmail = New System.Windows.Forms.TextBox()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblUserManager = New System.Windows.Forms.Label()
        Me.BtnBack = New System.Windows.Forms.Button()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CBBreedList = New System.Windows.Forms.ComboBox()
        Me.RdBtnLiving = New System.Windows.Forms.RadioButton()
        Me.RdBtnDeceased = New System.Windows.Forms.RadioButton()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LblCharsRemaining = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.Label1.Location = New System.Drawing.Point(14, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(108, 15)
        Me.Label1.TabIndex = 90
        Me.Label1.Text = "(*) marks required"
        '
        'CBAnimalList
        '
        Me.CBAnimalList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBAnimalList.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CBAnimalList.FormattingEnabled = True
        Me.CBAnimalList.ItemHeight = 15
        Me.CBAnimalList.Location = New System.Drawing.Point(349, 107)
        Me.CBAnimalList.Name = "CBAnimalList"
        Me.CBAnimalList.Size = New System.Drawing.Size(168, 23)
        Me.CBAnimalList.TabIndex = 54
        '
        'BtnCreatePet
        '
        Me.BtnCreatePet.Location = New System.Drawing.Point(297, 308)
        Me.BtnCreatePet.Name = "BtnCreatePet"
        Me.BtnCreatePet.Size = New System.Drawing.Size(99, 54)
        Me.BtnCreatePet.TabIndex = 69
        Me.BtnCreatePet.Text = "Save Changes"
        Me.BtnCreatePet.UseVisualStyleBackColor = True
        '
        'BtnCancel
        '
        Me.BtnCancel.Location = New System.Drawing.Point(419, 308)
        Me.BtnCancel.Name = "BtnCancel"
        Me.BtnCancel.Size = New System.Drawing.Size(99, 54)
        Me.BtnCancel.TabIndex = 70
        Me.BtnCancel.Text = "Cancel"
        Me.BtnCancel.UseVisualStyleBackColor = True
        '
        'TxtBxPetName
        '
        Me.TxtBxPetName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPetName.Location = New System.Drawing.Point(94, 140)
        Me.TxtBxPetName.Name = "TxtBxPetName"
        Me.TxtBxPetName.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxPetName.TabIndex = 53
        '
        'LblPassword
        '
        Me.LblPassword.AutoSize = True
        Me.LblPassword.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPassword.Location = New System.Drawing.Point(10, 144)
        Me.LblPassword.Name = "LblPassword"
        Me.LblPassword.Size = New System.Drawing.Size(64, 15)
        Me.LblPassword.TabIndex = 81
        Me.LblPassword.Text = "Pet Name*"
        '
        'TxtBxMChipNo
        '
        Me.TxtBxMChipNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxMChipNo.Location = New System.Drawing.Point(349, 173)
        Me.TxtBxMChipNo.Name = "TxtBxMChipNo"
        Me.TxtBxMChipNo.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxMChipNo.TabIndex = 59
        '
        'LblMobPhone
        '
        Me.LblMobPhone.AutoSize = True
        Me.LblMobPhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMobPhone.Location = New System.Drawing.Point(282, 170)
        Me.LblMobPhone.Name = "LblMobPhone"
        Me.LblMobPhone.Size = New System.Drawing.Size(64, 30)
        Me.LblMobPhone.TabIndex = 80
        Me.LblMobPhone.Text = "Microchip" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Number*"
        '
        'LblHomePhone
        '
        Me.LblHomePhone.AutoSize = True
        Me.LblHomePhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblHomePhone.Location = New System.Drawing.Point(282, 144)
        Me.LblHomePhone.Name = "LblHomePhone"
        Me.LblHomePhone.Size = New System.Drawing.Size(44, 15)
        Me.LblHomePhone.TabIndex = 79
        Me.LblHomePhone.Text = "Breed*"
        '
        'LblDOB
        '
        Me.LblDOB.AutoSize = True
        Me.LblDOB.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDOB.Location = New System.Drawing.Point(282, 111)
        Me.LblDOB.Name = "LblDOB"
        Me.LblDOB.Size = New System.Drawing.Size(46, 15)
        Me.LblDOB.TabIndex = 78
        Me.LblDOB.Text = "Animal"
        '
        'TxtBxDescription
        '
        Me.TxtBxDescription.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxDescription.Location = New System.Drawing.Point(94, 206)
        Me.TxtBxDescription.MaxLength = 4000
        Me.TxtBxDescription.Multiline = True
        Me.TxtBxDescription.Name = "TxtBxDescription"
        Me.TxtBxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxtBxDescription.Size = New System.Drawing.Size(423, 96)
        Me.TxtBxDescription.TabIndex = 56
        '
        'LblSurname
        '
        Me.LblSurname.AutoSize = True
        Me.LblSurname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSurname.Location = New System.Drawing.Point(10, 238)
        Me.LblSurname.Name = "LblSurname"
        Me.LblSurname.Size = New System.Drawing.Size(76, 30)
        Me.LblSurname.TabIndex = 77
        Me.LblSurname.Text = "Animal" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Description*"
        '
        'TxtBxPetDob
        '
        Me.TxtBxPetDob.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPetDob.Location = New System.Drawing.Point(94, 173)
        Me.TxtBxPetDob.Name = "TxtBxPetDob"
        Me.TxtBxPetDob.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxPetDob.TabIndex = 55
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFirstName.Location = New System.Drawing.Point(10, 163)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(87, 45)
        Me.LblFirstName.TabIndex = 76
        Me.LblFirstName.Text = "Pet DOB*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Approx)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(DD/MM/YYYY)"
        '
        'TxtBxEmail
        '
        Me.TxtBxEmail.Enabled = False
        Me.TxtBxEmail.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxEmail.Location = New System.Drawing.Point(94, 107)
        Me.TxtBxEmail.Name = "TxtBxEmail"
        Me.TxtBxEmail.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxEmail.TabIndex = 52
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmail.Location = New System.Drawing.Point(10, 104)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(80, 30)
        Me.LblEmail.TabIndex = 74
        Me.LblEmail.Text = "Owner Email " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address*"
        '
        'LblUserManager
        '
        Me.LblUserManager.AutoSize = True
        Me.LblUserManager.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUserManager.Location = New System.Drawing.Point(12, 37)
        Me.LblUserManager.Name = "LblUserManager"
        Me.LblUserManager.Size = New System.Drawing.Size(144, 26)
        Me.LblUserManager.TabIndex = 73
        Me.LblUserManager.Text = "Create New Pet"
        '
        'BtnBack
        '
        Me.BtnBack.Location = New System.Drawing.Point(426, 35)
        Me.BtnBack.Name = "BtnBack"
        Me.BtnBack.Size = New System.Drawing.Size(92, 35)
        Me.BtnBack.TabIndex = 72
        Me.BtnBack.Text = "Back"
        Me.BtnBack.UseVisualStyleBackColor = True
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'CBBreedList
        '
        Me.CBBreedList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBBreedList.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CBBreedList.FormattingEnabled = True
        Me.CBBreedList.ItemHeight = 15
        Me.CBBreedList.Location = New System.Drawing.Point(349, 140)
        Me.CBBreedList.Name = "CBBreedList"
        Me.CBBreedList.Size = New System.Drawing.Size(168, 23)
        Me.CBBreedList.TabIndex = 93
        '
        'RdBtnLiving
        '
        Me.RdBtnLiving.AutoSize = True
        Me.RdBtnLiving.Location = New System.Drawing.Point(94, 328)
        Me.RdBtnLiving.Name = "RdBtnLiving"
        Me.RdBtnLiving.Size = New System.Drawing.Size(53, 17)
        Me.RdBtnLiving.TabIndex = 94
        Me.RdBtnLiving.TabStop = True
        Me.RdBtnLiving.Text = "Living"
        Me.RdBtnLiving.UseVisualStyleBackColor = True
        '
        'RdBtnDeceased
        '
        Me.RdBtnDeceased.AutoSize = True
        Me.RdBtnDeceased.Location = New System.Drawing.Point(153, 328)
        Me.RdBtnDeceased.Name = "RdBtnDeceased"
        Me.RdBtnDeceased.Size = New System.Drawing.Size(74, 17)
        Me.RdBtnDeceased.TabIndex = 95
        Me.RdBtnDeceased.TabStop = True
        Me.RdBtnDeceased.Text = "Deceased"
        Me.RdBtnDeceased.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(10, 328)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 15)
        Me.Label2.TabIndex = 96
        Me.Label2.Text = "Status"
        '
        'ErrorProvider
        '
        Me.ErrorProvider.ContainerControl = Me
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(94, 305)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(114, 13)
        Me.Label3.TabIndex = 97
        Me.Label3.Text = "Characters Remaining:"
        '
        'LblCharsRemaining
        '
        Me.LblCharsRemaining.AutoSize = True
        Me.LblCharsRemaining.Location = New System.Drawing.Point(204, 306)
        Me.LblCharsRemaining.Name = "LblCharsRemaining"
        Me.LblCharsRemaining.Size = New System.Drawing.Size(31, 13)
        Me.LblCharsRemaining.TabIndex = 98
        Me.LblCharsRemaining.Text = "4000"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(551, 24)
        Me.MenuStrip1.TabIndex = 99
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(44, 20)
        Me.ToolStripMenuItem1.Text = "Help"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(80, 22)
        Me.ToolStripMenuItem2.Text = "E"
        '
        'NewPet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 373)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.TxtBxMChipNo)
        Me.Controls.Add(Me.LblCharsRemaining)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RdBtnDeceased)
        Me.Controls.Add(Me.RdBtnLiving)
        Me.Controls.Add(Me.CBBreedList)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.CBAnimalList)
        Me.Controls.Add(Me.BtnCreatePet)
        Me.Controls.Add(Me.BtnCancel)
        Me.Controls.Add(Me.TxtBxPetName)
        Me.Controls.Add(Me.LblPassword)
        Me.Controls.Add(Me.LblMobPhone)
        Me.Controls.Add(Me.LblHomePhone)
        Me.Controls.Add(Me.LblDOB)
        Me.Controls.Add(Me.TxtBxDescription)
        Me.Controls.Add(Me.LblSurname)
        Me.Controls.Add(Me.TxtBxPetDob)
        Me.Controls.Add(Me.LblFirstName)
        Me.Controls.Add(Me.TxtBxEmail)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblUserManager)
        Me.Controls.Add(Me.BtnBack)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "NewPet"
        Me.Text = "Create Pet"
        CType(Me.ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As Label
    Friend WithEvents CBAnimalList As ComboBox
    Friend WithEvents BtnCreatePet As Button
    Friend WithEvents BtnCancel As Button
    Friend WithEvents TxtBxPetName As TextBox
    Friend WithEvents LblPassword As Label
    Friend WithEvents TxtBxMChipNo As TextBox
    Friend WithEvents LblMobPhone As Label
    Friend WithEvents LblHomePhone As Label
    Friend WithEvents LblDOB As Label
    Friend WithEvents TxtBxDescription As TextBox
    Friend WithEvents LblSurname As Label
    Friend WithEvents TxtBxPetDob As TextBox
    Friend WithEvents LblFirstName As Label
    Friend WithEvents TxtBxEmail As TextBox
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblUserManager As Label
    Friend WithEvents BtnBack As Button
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CBBreedList As ComboBox
    Friend WithEvents RdBtnLiving As RadioButton
    Friend WithEvents RdBtnDeceased As RadioButton
    Friend WithEvents Label2 As Label
    Friend WithEvents ErrorProvider As ErrorProvider
    Friend WithEvents LblCharsRemaining As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
End Class
