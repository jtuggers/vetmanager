﻿Public Class UserPets
    Dim Staff As New Staff
    Dim loadStatus As Integer = 0
    Private Sub UserPets_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub





    Private Sub BtnRefresh_Click(sender As Object, e As EventArgs) Handles BtnRefresh.Click
        DGVPetManager.Columns.Clear()
        Dim ds As New DataTable
        ds = Staff.GetClientPetList(Globals.ModifyID)
        DGVPetManager.DataSource = ds
        DGVPetManager.DataMember = ds.TableName

        For Each column As DataGridViewColumn In DGVPetManager.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        Dim ResizeColumn As DataGridViewColumn = DGVPetManager.Columns(0)
        ResizeColumn.Width = 50 'petid

        ResizeColumn = DGVPetManager.Columns(1)
        ResizeColumn.Width = 103 'petname

        ResizeColumn = DGVPetManager.Columns(2)
        ResizeColumn.Width = 104 'animal

        ResizeColumn = DGVPetManager.Columns(3)
        ResizeColumn.Width = 120 'Breed

        ResizeColumn = DGVPetManager.Columns(4)
        ResizeColumn.Width = 110 'M/chip

        ResizeColumn = DGVPetManager.Columns(5)
        ResizeColumn.Width = 60 'Deceased

        DGVPetManager.Columns(2).HeaderText = "Animal"
        DGVPetManager.Columns(3).HeaderText = "Breed"
        LblRowCount.Text = "Rows Found: " + DGVPetManager.RowCount.ToString()
    End Sub








    Private Sub DGVUserManager_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DGVPetManager.CellMouseDoubleClick
        If (e.ColumnIndex >= 0) And (e.RowIndex >= 0) Then
            BtnViewPet.Enabled = True
            Dim selectedRow = DGVPetManager.Rows(e.RowIndex)
            Dim value As String = DGVPetManager.Rows(e.RowIndex).Cells("PetID").Value
            Globals.PetID = value
            Globals.PreButton = "PetsByUser"
            StaffViewPet.ShowDialog()

        End If

    End Sub


    Private Sub DGVUserManager_KeyDown(sender As Object, e As KeyEventArgs) Handles DGVPetManager.KeyDown
        If e.KeyCode = Keys.Enter Then
            If (DGVPetManager.CurrentCell.RowIndex >= 0) And (DGVPetManager.CurrentCell.ColumnIndex >= 0) Then
                BtnViewPet.Enabled = True
                Dim selectedRow = DGVPetManager.SelectedColumns
                Dim value As String = DGVPetManager.Rows(DGVPetManager.CurrentCell.RowIndex).Cells("PetID").Value
                Globals.PetID = value
                Globals.PreButton = "PetsByUser"
                StaffViewPet.ShowDialog()
                'Me.Close()
            End If
        End If
    End Sub

    Private Sub DGVUserManager_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVPetManager.CellClick
        If (DGVPetManager.CurrentCell.RowIndex >= 0) And (DGVPetManager.CurrentCell.ColumnIndex >= 0) Then
            BtnViewPet.Enabled = True
            Dim selectedRow = DGVPetManager.SelectedColumns
            Dim value As String = DGVPetManager.Rows(DGVPetManager.CurrentCell.RowIndex).Cells("PetID").Value

            Globals.PetID = value
        End If
    End Sub

    Private Sub BtnNewPet_Click(sender As Object, e As EventArgs) Handles BtnNewPet.Click
        Globals.NewPet = True
        NewPet.ShowDialog()

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub RdBtnAllAnimals_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnAllAnimals.CheckedChanged
        If loadStatus = 1 Then
            DGVPetManager.DataSource.DefaultView.RowFilter = ""
        End If
    End Sub

    Private Sub RdBtnLiving_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnLiving.CheckedChanged
        If loadStatus = 1 Then
            DGVPetManager.DataSource.DefaultView.RowFilter = "Deceased='0'"
        End If
    End Sub

    Private Sub RdBtnDeceased_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnDeceased.CheckedChanged
        If loadStatus = 1 Then
            DGVPetManager.DataSource.DefaultView.RowFilter = "Deceased='1'"
        End If
    End Sub

    Private Sub UserPets_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        BtnRefresh.PerformClick()
    End Sub
End Class