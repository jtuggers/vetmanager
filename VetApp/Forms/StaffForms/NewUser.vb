﻿Imports System.ComponentModel

Public Class NewUser

    Dim User As New User
    Dim Validation As New Validation
    Dim Staff As New Staff

    Private Sub AdminNewUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CBPrivilageLevel.SelectedIndex = 0
    End Sub


    Private Sub BtnAdminBack_Click(sender As Object, e As EventArgs) Handles BtnAdminBack.Click
        Me.Close()
    End Sub
    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        BtnAdminBack.PerformClick()
    End Sub








    'ADD MESSAGEBOX CONNFIRMATION DISPLAYING EMAIL AND PASSWORD FOR USER TO COPY DOWN



    Private Sub BtnCreateAccount_Click(sender As Object, e As EventArgs) Handles BtnCreateAccount.Click


        '##Focuses on all textboxes triggering the "validating" action
        For Each control As Control In Me.Controls
            If TypeName(control) = "TextBox" Then
                Dim txtbx As TextBox = CType(control, TextBox)
                txtbx.Focus()
            End If
        Next
        CBTitle.Focus()
        CBPrivilageLevel.Focus()

        '##Checks for any errors on form
        For Each control As Control In Me.Controls
            If TypeName(control) = "TextBox" Then
                Dim txtbx As TextBox = CType(control, TextBox)
                If Not ErrorProvider.GetError(txtbx) = "" Then
                    MessageBox.Show("Validation errors found. Please correct these errors and try again.", "Validation Errors", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If
            End If
        Next




        Dim revdate As String
        Try
            revdate = User.ReverseDate(TxtBxDOB.Text)
        Catch
            revdate = "1111/11/11"
        End Try

        If User.EmailExists(TxtBxEmail.Text) = 0 Then
            Dim Details(15) As String
            Details(0) = TxtBxEmail.Text
            Details(1) = CBTitle.Text
            Details(2) = TxtBxFirstName.Text
            Details(3) = TxtBxSurname.Text
            Details(4) = revdate
            Details(5) = TxtBxHomePhone.Text
            Details(6) = TxtBxMobPhone.Text
            Details(7) = TxtBxWorkPhone.Text
            Details(8) = TxtBxPassword.Text
            Details(9) = TxtBxAddNameNo.Text
            Details(10) = TxtBxAddRoad.Text
            Details(11) = TxtBxAddCity.Text
            Details(12) = TxtBxAddCounty.Text
            Details(13) = TxtBxAddCountry.Text
            Details(14) = TxtBxAddPostCode.Text
            Details(15) = CBPrivilageLevel.SelectedIndex

            Try
                Staff.CreateUser(Details)
                MessageBox.Show("User Account created Successfully.")


                For Each control As Control In Me.Controls
                    If TypeName(control) = "TextBox" Then
                        Dim txtbx As TextBox = CType(control, TextBox)
                        txtbx.Text = ""
                    End If
                Next
                Me.Close()

            Catch ex As Exception
                MessageBox.Show("An error occured during user creation. Please try again later." + vbNewLine + vbNewLine + "Error: " + ex.ToString)
            End Try
        Else
            MessageBox.Show("There is already an account linked to this email.")
        End If
    End Sub







    '################
    'VALIDATING BLOCK
    '################






    Private Sub TxtBxEmail_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxEmail.Validating
        If Len(TxtBxEmail.Text) = 0 Then
            ErrorProvider.SetError(TxtBxEmail, "This field is required")
        ElseIf Validation.Email(TxtBxEmail.Text) = True Then
            ErrorProvider.SetError(TxtBxEmail, "")
        Else
            ErrorProvider.SetError(TxtBxEmail, "Email format invalid")
        End If
    End Sub


    Private Sub TxtBxFirstName_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxFirstName.Validating
        If Len(TxtBxFirstName.Text) = 0 Then
            ErrorProvider.SetError(TxtBxFirstName, "This field is required")
        ElseIf Len(TxtBxFirstName.Text) < 2 Then
            ErrorProvider.SetError(TxtBxFirstName, "Name must be at least 2 characters long.")

        ElseIf Validation.Name(TxtBxFirstName.Text) = True Then
            ErrorProvider.SetError(TxtBxFirstName, "")
        Else
            ErrorProvider.SetError(TxtBxFirstName, "Input invalid")
        End If
    End Sub


    Private Sub TxtBxSurname_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxSurname.Validating
        If Len(TxtBxSurname.Text) = 0 Then
            ErrorProvider.SetError(TxtBxSurname, "This field is required")
        ElseIf Validation.Name(TxtBxSurname.Text) = True Then
            ErrorProvider.SetError(TxtBxSurname, "")
        Else
            ErrorProvider.SetError(TxtBxSurname, "Input invalid")
        End If
    End Sub

    Private Sub TxtBxDOB_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxDOB.Validating
        If Len(TxtBxDOB.Text) = 0 Then
            ErrorProvider.SetError(TxtBxDOB, "This field is required")
        ElseIf Validation.DOB(TxtBxDOB.Text) = True Then
            ErrorProvider.SetError(TxtBxDOB, "")
        Else
            ErrorProvider.SetError(TxtBxDOB, "Date Format/Range Invalid." + vbNewLine + "Please ensure the date format is being used (DD-MM-YYYY)")
        End If
    End Sub

    Private Sub TxtBxHomePhone_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxHomePhone.Validating
        If Len(TxtBxHomePhone.Text) = 0 Then
            ErrorProvider.SetError(TxtBxHomePhone, "")
        ElseIf Validation.Phone(TxtBxHomePhone.Text) = True Then
            ErrorProvider.SetError(TxtBxHomePhone, "")
        Else
            ErrorProvider.SetError(TxtBxHomePhone, "Number invalid. " + vbNewLine + "Please ensure the number begins with 0," + vbNewLine + "and is 11 numbers long.")
        End If
    End Sub

    Private Sub TxtBxMobPhone_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxMobPhone.Validating
        If Len(TxtBxMobPhone.Text) = 0 Then
            ErrorProvider.SetError(TxtBxMobPhone, "This field is required.")
        ElseIf Validation.Phone(TxtBxMobPhone.Text) = True Then
            ErrorProvider.SetError(TxtBxMobPhone, "")
        Else
            ErrorProvider.SetError(TxtBxMobPhone, "Number invalid. " + vbNewLine + "Please ensure the number begins with 0," + vbNewLine + "and is 11 numbers long.")
        End If
    End Sub

    Private Sub TxtBxWorkPhone_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxWorkPhone.Validating
        If Len(TxtBxWorkPhone.Text) = 0 Then
            ErrorProvider.SetError(TxtBxWorkPhone, "")
        ElseIf Validation.Phone(TxtBxWorkPhone.Text) = True Then
            ErrorProvider.SetError(TxtBxWorkPhone, "")
        Else
            ErrorProvider.SetError(TxtBxWorkPhone, "Number invalid. " + vbNewLine + "Please ensure the number begins with 0," + vbNewLine + "and is 11 numbers long.")
        End If
    End Sub


    Private Sub TxtBxPassword_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxPassword.Validating

        If Len(TxtBxPassword.Text) < 6 Then
            ErrorProvider.SetError(TxtBxPassword, "Passwords must be at least 6 characters long.")
        Else
            ErrorProvider.SetError(TxtBxPassword, "")
        End If
    End Sub

    Private Sub TxtBxAddNameNo_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddNameNo.Validating
        If Len(TxtBxAddNameNo.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddNameNo, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddNameNo, "")
        End If
    End Sub

    Private Sub TxtBxAddRoad_Validating(sender As Object, e As EventArgs) Handles TxtBxAddRoad.Validated
        If Len(TxtBxAddRoad.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddRoad, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddRoad, "")
        End If
    End Sub

    Private Sub TxtBxAddCity_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddCity.Validating
        If Len(TxtBxAddCity.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddCity, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddCity, "")
        End If
    End Sub

    Private Sub TxtBxAddCounty_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddCounty.Validating
        If Len(TxtBxAddCounty.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddCounty, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddCounty, "")
        End If
    End Sub

    Private Sub TxtBxAddCountry_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddCountry.Validating
        If Len(TxtBxAddCountry.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddCountry, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddCountry, "")
        End If
    End Sub

    Private Sub TxtBxAddPostCode_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddPostCode.Validating
        If Len(TxtBxAddPostCode.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddPostCode, "Field is required.")
        ElseIf Validation.Postcode(TxtBxAddPostCode.Text) = True Then
            ErrorProvider.SetError(TxtBxAddPostCode, "")
        Else
            ErrorProvider.SetError(TxtBxAddPostCode, "Postcode Invalid")
        End If
    End Sub

    Private Sub CBTitle_Validating(sender As Object, e As CancelEventArgs) Handles CBTitle.Validating
        If CBTitle.SelectedIndex = -1 Then
            ErrorProvider.SetError(TxtBxAddPostCode, "Title must be selected.")
        Else
            ErrorProvider.SetError(TxtBxAddPostCode, "")
        End If
    End Sub


End Class