﻿Imports System.ComponentModel

Public Class NewPet
    Dim User As New User
    Dim Validation As New Validation

    Private Sub BtnAdminBack_Click(sender As Object, e As EventArgs) Handles BtnBack.Click
        Me.Close()
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Me.Close()
    End Sub

    Private Sub NewPet_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TxtBxEmail.Text = User.GetEmail()
        CBAnimalList.Items.Clear()
        Dim Animals As List(Of String) = User.GetAnimalList()
        For Each item As String In Animals
            CBAnimalList.Items.Add(item)
        Next

        If Globals.NewPet = True Then
            CBBreedList.Enabled = False
            RdBtnLiving.Checked = True
        ElseIf Globals.NewPet = False Then

            CBBreedList.Items.Clear()
            Dim Breeds As List(Of String) = User.GetBreedList(CBAnimalList.Text)
            For Each item As String In Breeds
                CBBreedList.Items.Add(item)
            Next
            CBBreedList.Enabled = True

            Dim Details() As String
            Details = User.GetPetDetails()

            Globals.ModifyID = Details(0)
            TxtBxEmail.Text = User.GetEmail()

            TxtBxPetName.Text = Details(1)

            TxtBxPetDob.Text = Details(2).Substring(0, 10)


            TxtBxDescription.Text = Details(3)
            CBAnimalList.Text = Details(4)
            CBBreedList.Text = Details(5)

            TxtBxMChipNo.Text = Details(6)
            If Details(7) = 1 Then
                RdBtnDeceased.Checked = True
                RdBtnLiving.Checked = False
            Else
                RdBtnLiving.Checked = True
                RdBtnDeceased.Checked = False

            End If
        End If
    End Sub

    Private Sub CBAnimalList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBAnimalList.SelectedIndexChanged
        CBBreedList.Items.Clear()
        Dim SelectedAnimal As String = CBAnimalList.Text
        Dim Breeds As List(Of String) = User.GetBreedList(SelectedAnimal)
        For Each item As String In Breeds
            CBBreedList.Items.Add(item)
        Next
        CBBreedList.Enabled = True
    End Sub

    Private Sub TxtBxPetName_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxPetName.Validating
        If Len(TxtBxPetName.Text) = 0 Then
            ErrorProvider.SetError(TxtBxPetName, "Field Required.")
        ElseIf Validation.PetName(TxtBxPetName.Text) = True Then
            ErrorProvider.SetError(TxtBxPetName, "")
        Else
            ErrorProvider.SetError(TxtBxPetName, "Invalid Input")
        End If
    End Sub

    Private Sub TxtBxPetDob_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxPetDob.Validating
        If Len(TxtBxPetDob.Text) = 0 Then
            ErrorProvider.SetError(TxtBxPetDob, "This field is required")
        ElseIf Validation.DOB(TxtBxPetDOB.Text) = True Then
            ErrorProvider.SetError(TxtBxPetDob, "")
        Else
            ErrorProvider.SetError(TxtBxPetDob, "Date Format/Range Invalid." + vbNewLine + "Please ensure the date format is being used (DD/MM/YYYY) and the year is within the last 100 years.")
        End If
    End Sub

    Private Sub TxtBxDescription_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxDescription.Validating
        'FINISH VALIDATION
        'FORM QUERY TO ADD PET
        'REMEMBER TO REVERSE DATE OF BIRTH USING USER.REVERSEDATE BEFORE SAVING TO DB
        'CREATE MODIFYPET

        If Len(TxtBxDescription.Text) = 0 Then
            ErrorProvider.SetError(TxtBxDescription, "Field Required.")
        Else
            ErrorProvider.SetError(TxtBxDescription, "")
        End If

    End Sub

    Private Sub CBAnimalList_Validating(sender As Object, e As CancelEventArgs) Handles CBAnimalList.Validating
        If CBAnimalList.SelectedIndex = -1 Then
            ErrorProvider.SetError(CBAnimalList, "Please select an Animal.")
        Else
            ErrorProvider.SetError(CBAnimalList, "")
        End If

    End Sub

    Private Sub CBBreedList_Validating(sender As Object, e As CancelEventArgs) Handles CBBreedList.Validating
        If CBBreedList.SelectedIndex = -1 Then
            ErrorProvider.SetError(CBBreedList, "Please select a Breed.")
        Else
            ErrorProvider.SetError(CBBreedList, "")
        End If
    End Sub

    Private Sub TxtBxMChipNo_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxMChipNo.Validating
        If Len(TxtBxMChipNo.Text) = 0 Then
            ErrorProvider.SetError(TxtBxMChipNo, "Field Required.")
        ElseIf (Len(TxtBxMChipNo.Text) < 9) Or (Len(TxtBxMChipNo.Text) > 15) Then
            ErrorProvider.SetError(TxtBxMChipNo, "Invalid Format. Please ensure number is typed correctly, and is between 9 and 15 characters.")
        Else
            ErrorProvider.SetError(TxtBxMChipNo, "")
        End If

    End Sub

    Private Sub TxtBxDescription_TextChanged(sender As Object, e As EventArgs) Handles TxtBxDescription.TextChanged
        LblCharsRemaining.Text = 4000 - Len(TxtBxDescription.Text)
    End Sub

    Private Sub TxtBxDescription_KeyDown(sender As Object, e As KeyEventArgs) Handles TxtBxDescription.KeyDown
        If (e.Control And e.KeyCode = Keys.A) Then TxtBxDescription.SelectAll()
    End Sub

    Private Sub BtnCreatePet_Click(sender As Object, e As EventArgs) Handles BtnCreatePet.Click




        '##Focuses on all textboxes triggering the "validating" action
        For Each control As Control In Me.Controls
            If TypeName(control) = "TextBox" Then
                Dim txtbx As TextBox = CType(control, TextBox)
                txtbx.Focus()
            End If
        Next

        CBAnimalList.Focus()
        CBBreedList.Focus()

        '##Checks for any errors on form
        For Each control As Control In Me.Controls
            If TypeName(control) = "TextBox" Then
                Dim txtbx As TextBox = CType(control, TextBox)
                If Not ErrorProvider.GetError(txtbx) = "" Then
                    MessageBox.Show("Validation errors found. Please correct these errors and try again.", "Validation Errors", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    Return
                End If
            End If
        Next



        Dim RevDate As String
        Try
            RevDate = User.ReverseDate(TxtBxPetDob.Text)
        Catch
            RevDate = "1111/11/11"
        End Try

        Dim Details(7) As String
        Details(0) = Globals.ModifyID
        Details(1) = TxtBxPetName.Text
        Details(2) = RevDate
        Details(3) = TxtBxDescription.Text
        Details(4) = CBAnimalList.Text
        Details(5) = CBBreedList.Text
        Details(6) = TxtBxMChipNo.Text
        Details(7) = Status

        If Globals.NewPet = True Then
            Try
                User.UserCreatePet(Details)
                MessageBox.Show("Changes were saved successfully.")
                Me.Close()
            Catch ex As Exception
                MessageBox.Show("An error occured when attempting to save changes. Please try again later." + vbNewLine + vbNewLine + "Error: " + ex.ToString)
            End Try
        ElseIf Globals.newPet = False Then
            Try
                User.ModifyPet(Details)
                MessageBox.Show("Changes were saved successfully.")
                Me.Close()
            Catch ex As Exception
                MessageBox.Show("An error occured when attempting to save changes. Please try again later." + vbNewLine + vbNewLine + "Error: " + ex.ToString)
            End Try
        End If
    End Sub


    Dim Status As Integer
    Private Sub RdBtnLiving_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnLiving.CheckedChanged
        Status = 0
    End Sub

    Private Sub RdBtnDeceased_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnDeceased.CheckedChanged
        Status = 1
    End Sub
End Class