﻿Imports System.ComponentModel
Public Class StaffModifyUser

    Dim Staff As New Staff
    Dim User As New User
    Private Sub ModifyUser_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Details() As String = User.GetUserDetails()

        TxtBxEmail.Text = Details(0)
        CBTitle.Text = Details(1)
        TxtBxFirstName.Text = Details(2)
        TxtBxSurname.Text = Details(3)
        TxtBxDOB.Text = Details(4).Substring(0, 10)
        TxtBxHomePhone.Text = Details(5)
        TxtBxMobPhone.Text = Details(6)
        TxtBxWorkPhone.Text = Details(7)
        TxtBxAddNameNo.Text = Details(8)
        TxtBxAddRoad.Text = Details(9)
        TxtBxAddCity.Text = Details(10)
        TxtBxAddCounty.Text = Details(11)
        TxtBxAddCountry.Text = Details(12)
        TxtBxAddPostCode.Text = Details(13)
        CBPrivilageLevel.SelectedIndex = Details(14)


        CBPrivilageLevel.DropDownStyle = ComboBoxStyle.Simple
        CBPrivilageLevel.Enabled = False
    End Sub

    Private Sub BtnCancel_Click(sender As Object, e As EventArgs) Handles BtnCancel.Click
        Me.Close()
    End Sub

    Private Sub BtnAdminBack_Click(sender As Object, e As EventArgs) Handles BtnAdminBack.Click
        Me.Close()
    End Sub






    Private Sub BtnSaveChanges_Click(sender As Object, e As EventArgs) Handles BtnSaveChanges.Click




        '##Focuses on all textboxes triggering the "validating" action
        For Each control As Control In Me.Controls
                If TypeName(control) = "TextBox" Then
                    Dim txtbx As TextBox = CType(control, TextBox)
                    txtbx.Focus()
                End If
            Next
        CBTitle.Focus()

        '##Checks for any errors on form
        For Each control As Control In Me.Controls
                If TypeName(control) = "TextBox" Then
                    Dim txtbx As TextBox = CType(control, TextBox)
                    If Not ErrorProvider.GetError(txtbx) = "" Then
                        MessageBox.Show("Validation errors found. Please correct these errors and try again.", "Validation Errors", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Return
                    End If
                End If
            Next



        Dim RevDate As String
        Try
            RevDate = User.ReverseDate(TxtBxDOB.Text)
        Catch
            RevDate = "1111/11/11"
        End Try

        Dim Details(13) As String
        Details(0) = CBTitle.Text
        Details(1) = TxtBxFirstName.Text
        Details(2) = TxtBxSurname.Text
        Details(3) = RevDate
        Details(4) = TxtBxHomePhone.Text
        Details(5) = TxtBxMobPhone.Text
        Details(6) = TxtBxWorkPhone.Text
        Details(7) = TxtBxAddNameNo.Text
        Details(8) = TxtBxAddRoad.Text
        Details(9) = TxtBxAddCity.Text
        Details(10) = TxtBxAddCounty.Text
        Details(11) = TxtBxAddCountry.Text
        Details(12) = TxtBxAddPostCode.Text
        Details(13) = CBPrivilageLevel.SelectedIndex



        Try
            Staff.UpdateDetails(Details)
            MessageBox.Show("Changes were saved successfully.")
            Me.Close()
        Catch ex As Exception
            MessageBox.Show("An error occured when attempting to modify details. Please try again later." + vbNewLine + vbNewLine + "Error: " + ex.ToString)
        End Try


    End Sub




    '################
    'VALIDATING BLOCK
    '################


    Dim validation As New Validation


    Private Sub TxtBxFirstName_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxFirstName.Validating
        If Len(TxtBxFirstName.Text) = 0 Then
            ErrorProvider.SetError(TxtBxFirstName, "This field is required")
        ElseIf Len(TxtBxFirstName.Text) < 2 Then
            ErrorProvider.SetError(TxtBxFirstName, "Name must be at least 2 characters long.")

        ElseIf validation.Name(TxtBxFirstName.Text) = True Then
            ErrorProvider.SetError(TxtBxFirstName, "")
        Else
            ErrorProvider.SetError(TxtBxFirstName, "Input invalid")
        End If
    End Sub


    Private Sub TxtBxSurname_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxSurname.Validating
        If Len(TxtBxSurname.Text) = 0 Then
            ErrorProvider.SetError(TxtBxSurname, "This field is required")
        ElseIf validation.Name(TxtBxSurname.Text) = True Then
            ErrorProvider.SetError(TxtBxSurname, "")
        Else
            ErrorProvider.SetError(TxtBxSurname, "Input invalid")
        End If
    End Sub

    Private Sub TxtBxDOB_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxDOB.Validating
        If Len(TxtBxDOB.Text) = 0 Then
            ErrorProvider.SetError(TxtBxDOB, "This field is required")
        ElseIf validation.DOB(TxtBxDOB.Text) = True Then
            ErrorProvider.SetError(TxtBxDOB, "")
        Else
            ErrorProvider.SetError(TxtBxDOB, "Date Format/Range Invalid." + vbNewLine + "Please ensure the date format is being used (DD-MM-YYYY)")
        End If
    End Sub

    Private Sub TxtBxHomePhone_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxHomePhone.Validating
        If Len(TxtBxHomePhone.Text) = 0 Then
            ErrorProvider.SetError(TxtBxHomePhone, "")
        ElseIf validation.Phone(TxtBxHomePhone.Text) = True Then
            ErrorProvider.SetError(TxtBxHomePhone, "")
        Else
            ErrorProvider.SetError(TxtBxHomePhone, "Number invalid. " + vbNewLine + "Please ensure the number begins with 0," + vbNewLine + "and is 11 numbers long.")
        End If
    End Sub

    Private Sub TxtBxMobPhone_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxMobPhone.Validating
        If Len(TxtBxMobPhone.Text) = 0 Then
            ErrorProvider.SetError(TxtBxMobPhone, "This field is required.")
        ElseIf validation.Phone(TxtBxMobPhone.Text) = True Then
            ErrorProvider.SetError(TxtBxMobPhone, "")
        Else
            ErrorProvider.SetError(TxtBxMobPhone, "Number invalid. " + vbNewLine + "Please ensure the number begins with 0," + vbNewLine + "and is 11 numbers long.")
        End If
    End Sub

    Private Sub TxtBxWorkPhone_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxWorkPhone.Validating
        If Len(TxtBxWorkPhone.Text) = 0 Then
            ErrorProvider.SetError(TxtBxWorkPhone, "")
        ElseIf validation.Phone(TxtBxWorkPhone.Text) = True Then
            ErrorProvider.SetError(TxtBxWorkPhone, "")
        Else
            ErrorProvider.SetError(TxtBxWorkPhone, "Number invalid. " + vbNewLine + "Please ensure the number begins with 0," + vbNewLine + "and is 11 numbers long.")
        End If
    End Sub


    Private Sub TxtBxAddNameNo_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddNameNo.Validating
        If Len(TxtBxAddNameNo.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddNameNo, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddNameNo, "")
        End If
    End Sub

    Private Sub TxtBxAddRoad_Validating(sender As Object, e As EventArgs) Handles TxtBxAddRoad.Validated
        If Len(TxtBxAddRoad.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddRoad, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddRoad, "")
        End If
    End Sub

    Private Sub TxtBxAddCity_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddCity.Validating
        If Len(TxtBxAddCity.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddCity, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddCity, "")
        End If
    End Sub

    Private Sub TxtBxAddCounty_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddCounty.Validating
        If Len(TxtBxAddCounty.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddCounty, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddCounty, "")
        End If
    End Sub

    Private Sub TxtBxAddCountry_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddCountry.Validating
        If Len(TxtBxAddCountry.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddCountry, "Field is required.")
        Else
            ErrorProvider.SetError(TxtBxAddCountry, "")
        End If
    End Sub

    Private Sub TxtBxAddPostCode_Validating(sender As Object, e As CancelEventArgs) Handles TxtBxAddPostCode.Validating
        If Len(TxtBxAddPostCode.Text) = 0 Then
            ErrorProvider.SetError(TxtBxAddPostCode, "Field is required.")
        ElseIf validation.Postcode(TxtBxAddPostCode.Text) = True Then
            ErrorProvider.SetError(TxtBxAddPostCode, "")
        Else
            ErrorProvider.SetError(TxtBxAddPostCode, "Postcode Invalid")
        End If
    End Sub

    Private Sub CBTitle_Validating(sender As Object, e As CancelEventArgs) Handles CBTitle.Validating
        If CBTitle.SelectedIndex = -1 Then
            ErrorProvider.SetError(TxtBxAddPostCode, "Title must be selected.")
        Else
            ErrorProvider.SetError(TxtBxAddPostCode, "")
        End If
    End Sub

    Private Sub BtnChangeEmail_Click(sender As Object, e As EventArgs) Handles BtnChangeEmail.Click
        ChangeEmail.Show()
    End Sub

    Private Sub BtnChangePass_Click(sender As Object, e As EventArgs) Handles BtnChangePass.Click
        ChangePassword.Show()
    End Sub


End Class