﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CBPrivilageLevel = New System.Windows.Forms.ComboBox()
        Me.CBTitle = New System.Windows.Forms.ComboBox()
        Me.LblPrivLevel = New System.Windows.Forms.Label()
        Me.TxtBxAddPostCode = New System.Windows.Forms.TextBox()
        Me.LblAddPostCode = New System.Windows.Forms.Label()
        Me.TxtBxAddCountry = New System.Windows.Forms.TextBox()
        Me.LblAddCountry = New System.Windows.Forms.Label()
        Me.TxtBxAddCounty = New System.Windows.Forms.TextBox()
        Me.LblAddCounty = New System.Windows.Forms.Label()
        Me.TxtBxAddCity = New System.Windows.Forms.TextBox()
        Me.LblAddCity = New System.Windows.Forms.Label()
        Me.TxtBxAddRoad = New System.Windows.Forms.TextBox()
        Me.LblAddRoad = New System.Windows.Forms.Label()
        Me.TxtBxAddNameNo = New System.Windows.Forms.TextBox()
        Me.LblAddNameNo = New System.Windows.Forms.Label()
        Me.TxtBxWorkPhone = New System.Windows.Forms.TextBox()
        Me.LblWorkPhone = New System.Windows.Forms.Label()
        Me.TxtBxMobPhone = New System.Windows.Forms.TextBox()
        Me.LblMobPhone = New System.Windows.Forms.Label()
        Me.TxtBxHomePhone = New System.Windows.Forms.TextBox()
        Me.LblHomePhone = New System.Windows.Forms.Label()
        Me.TxtBxDOB = New System.Windows.Forms.TextBox()
        Me.LblDOB = New System.Windows.Forms.Label()
        Me.TxtBxSurname = New System.Windows.Forms.TextBox()
        Me.LblSurname = New System.Windows.Forms.Label()
        Me.TxtBxFirstName = New System.Windows.Forms.TextBox()
        Me.LblFirstName = New System.Windows.Forms.Label()
        Me.LblTitle = New System.Windows.Forms.Label()
        Me.TxtBxEmail = New System.Windows.Forms.TextBox()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblViewUser = New System.Windows.Forms.Label()
        Me.BtnAdminBack = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnViewPets = New System.Windows.Forms.Button()
        Me.BtnEditDetails = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'CBPrivilageLevel
        '
        Me.CBPrivilageLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBPrivilageLevel.Enabled = False
        Me.CBPrivilageLevel.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CBPrivilageLevel.FormattingEnabled = True
        Me.CBPrivilageLevel.ItemHeight = 15
        Me.CBPrivilageLevel.Items.AddRange(New Object() {"Client", "Staff", "Admin", "Locked"})
        Me.CBPrivilageLevel.Location = New System.Drawing.Point(377, 302)
        Me.CBPrivilageLevel.Name = "CBPrivilageLevel"
        Me.CBPrivilageLevel.Size = New System.Drawing.Size(135, 23)
        Me.CBPrivilageLevel.TabIndex = 168
        '
        'CBTitle
        '
        Me.CBTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBTitle.Enabled = False
        Me.CBTitle.Font = New System.Drawing.Font("Calibri", 9.75!)
        Me.CBTitle.FormattingEnabled = True
        Me.CBTitle.ItemHeight = 15
        Me.CBTitle.Items.AddRange(New Object() {"Ms", "Miss", "Mrs", "Mr", "Rev", "Father", "Sister", "Dr", "Prof"})
        Me.CBTitle.Location = New System.Drawing.Point(119, 140)
        Me.CBTitle.Name = "CBTitle"
        Me.CBTitle.Size = New System.Drawing.Size(135, 23)
        Me.CBTitle.TabIndex = 149
        '
        'LblPrivLevel
        '
        Me.LblPrivLevel.AutoSize = True
        Me.LblPrivLevel.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPrivLevel.Location = New System.Drawing.Point(286, 305)
        Me.LblPrivLevel.Name = "LblPrivLevel"
        Me.LblPrivLevel.Size = New System.Drawing.Size(92, 15)
        Me.LblPrivLevel.TabIndex = 166
        Me.LblPrivLevel.Text = "Privilage Level*"
        '
        'TxtBxAddPostCode
        '
        Me.TxtBxAddPostCode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddPostCode.Location = New System.Drawing.Point(377, 269)
        Me.TxtBxAddPostCode.Name = "TxtBxAddPostCode"
        Me.TxtBxAddPostCode.ReadOnly = True
        Me.TxtBxAddPostCode.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxAddPostCode.TabIndex = 145
        '
        'LblAddPostCode
        '
        Me.LblAddPostCode.AutoSize = True
        Me.LblAddPostCode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddPostCode.Location = New System.Drawing.Point(286, 272)
        Me.LblAddPostCode.Name = "LblAddPostCode"
        Me.LblAddPostCode.Size = New System.Drawing.Size(67, 15)
        Me.LblAddPostCode.TabIndex = 165
        Me.LblAddPostCode.Text = "Post Code*"
        '
        'TxtBxAddCountry
        '
        Me.TxtBxAddCountry.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddCountry.Location = New System.Drawing.Point(377, 236)
        Me.TxtBxAddCountry.Name = "TxtBxAddCountry"
        Me.TxtBxAddCountry.ReadOnly = True
        Me.TxtBxAddCountry.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxAddCountry.TabIndex = 144
        '
        'LblAddCountry
        '
        Me.LblAddCountry.AutoSize = True
        Me.LblAddCountry.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddCountry.Location = New System.Drawing.Point(288, 239)
        Me.LblAddCountry.Name = "LblAddCountry"
        Me.LblAddCountry.Size = New System.Drawing.Size(56, 15)
        Me.LblAddCountry.TabIndex = 164
        Me.LblAddCountry.Text = "Country*"
        '
        'TxtBxAddCounty
        '
        Me.TxtBxAddCounty.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddCounty.Location = New System.Drawing.Point(377, 204)
        Me.TxtBxAddCounty.Name = "TxtBxAddCounty"
        Me.TxtBxAddCounty.ReadOnly = True
        Me.TxtBxAddCounty.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxAddCounty.TabIndex = 143
        '
        'LblAddCounty
        '
        Me.LblAddCounty.AutoSize = True
        Me.LblAddCounty.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddCounty.Location = New System.Drawing.Point(288, 207)
        Me.LblAddCounty.Name = "LblAddCounty"
        Me.LblAddCounty.Size = New System.Drawing.Size(51, 15)
        Me.LblAddCounty.TabIndex = 163
        Me.LblAddCounty.Text = "County*"
        '
        'TxtBxAddCity
        '
        Me.TxtBxAddCity.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddCity.Location = New System.Drawing.Point(377, 171)
        Me.TxtBxAddCity.Name = "TxtBxAddCity"
        Me.TxtBxAddCity.ReadOnly = True
        Me.TxtBxAddCity.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxAddCity.TabIndex = 142
        '
        'LblAddCity
        '
        Me.LblAddCity.AutoSize = True
        Me.LblAddCity.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddCity.Location = New System.Drawing.Point(288, 174)
        Me.LblAddCity.Name = "LblAddCity"
        Me.LblAddCity.Size = New System.Drawing.Size(34, 15)
        Me.LblAddCity.TabIndex = 162
        Me.LblAddCity.Text = "City*"
        '
        'TxtBxAddRoad
        '
        Me.TxtBxAddRoad.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddRoad.Location = New System.Drawing.Point(377, 138)
        Me.TxtBxAddRoad.Name = "TxtBxAddRoad"
        Me.TxtBxAddRoad.ReadOnly = True
        Me.TxtBxAddRoad.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxAddRoad.TabIndex = 141
        '
        'LblAddRoad
        '
        Me.LblAddRoad.AutoSize = True
        Me.LblAddRoad.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddRoad.Location = New System.Drawing.Point(286, 141)
        Me.LblAddRoad.Name = "LblAddRoad"
        Me.LblAddRoad.Size = New System.Drawing.Size(75, 15)
        Me.LblAddRoad.TabIndex = 161
        Me.LblAddRoad.Text = "Road Name*"
        '
        'TxtBxAddNameNo
        '
        Me.TxtBxAddNameNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddNameNo.Location = New System.Drawing.Point(377, 106)
        Me.TxtBxAddNameNo.Name = "TxtBxAddNameNo"
        Me.TxtBxAddNameNo.ReadOnly = True
        Me.TxtBxAddNameNo.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxAddNameNo.TabIndex = 140
        '
        'LblAddNameNo
        '
        Me.LblAddNameNo.AutoSize = True
        Me.LblAddNameNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddNameNo.Location = New System.Drawing.Point(287, 102)
        Me.LblAddNameNo.Name = "LblAddNameNo"
        Me.LblAddNameNo.Size = New System.Drawing.Size(91, 30)
        Me.LblAddNameNo.TabIndex = 160
        Me.LblAddNameNo.Text = "House number/" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Name*"
        '
        'TxtBxWorkPhone
        '
        Me.TxtBxWorkPhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxWorkPhone.Location = New System.Drawing.Point(119, 337)
        Me.TxtBxWorkPhone.Name = "TxtBxWorkPhone"
        Me.TxtBxWorkPhone.ReadOnly = True
        Me.TxtBxWorkPhone.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxWorkPhone.TabIndex = 139
        '
        'LblWorkPhone
        '
        Me.LblWorkPhone.AutoSize = True
        Me.LblWorkPhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWorkPhone.Location = New System.Drawing.Point(29, 340)
        Me.LblWorkPhone.Name = "LblWorkPhone"
        Me.LblWorkPhone.Size = New System.Drawing.Size(73, 15)
        Me.LblWorkPhone.TabIndex = 159
        Me.LblWorkPhone.Text = "Work Phone"
        '
        'TxtBxMobPhone
        '
        Me.TxtBxMobPhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxMobPhone.Location = New System.Drawing.Point(119, 304)
        Me.TxtBxMobPhone.Name = "TxtBxMobPhone"
        Me.TxtBxMobPhone.ReadOnly = True
        Me.TxtBxMobPhone.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxMobPhone.TabIndex = 138
        '
        'LblMobPhone
        '
        Me.LblMobPhone.AutoSize = True
        Me.LblMobPhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMobPhone.Location = New System.Drawing.Point(29, 307)
        Me.LblMobPhone.Name = "LblMobPhone"
        Me.LblMobPhone.Size = New System.Drawing.Size(89, 15)
        Me.LblMobPhone.TabIndex = 158
        Me.LblMobPhone.Text = "Mobile Phone*"
        '
        'TxtBxHomePhone
        '
        Me.TxtBxHomePhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxHomePhone.Location = New System.Drawing.Point(119, 271)
        Me.TxtBxHomePhone.Name = "TxtBxHomePhone"
        Me.TxtBxHomePhone.ReadOnly = True
        Me.TxtBxHomePhone.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxHomePhone.TabIndex = 137
        '
        'LblHomePhone
        '
        Me.LblHomePhone.AutoSize = True
        Me.LblHomePhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblHomePhone.Location = New System.Drawing.Point(29, 274)
        Me.LblHomePhone.Name = "LblHomePhone"
        Me.LblHomePhone.Size = New System.Drawing.Size(75, 15)
        Me.LblHomePhone.TabIndex = 157
        Me.LblHomePhone.Text = "Home Phone"
        '
        'TxtBxDOB
        '
        Me.TxtBxDOB.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxDOB.Location = New System.Drawing.Point(119, 238)
        Me.TxtBxDOB.Name = "TxtBxDOB"
        Me.TxtBxDOB.ReadOnly = True
        Me.TxtBxDOB.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxDOB.TabIndex = 136
        '
        'LblDOB
        '
        Me.LblDOB.AutoSize = True
        Me.LblDOB.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDOB.Location = New System.Drawing.Point(29, 234)
        Me.LblDOB.Name = "LblDOB"
        Me.LblDOB.Size = New System.Drawing.Size(87, 30)
        Me.LblDOB.TabIndex = 156
        Me.LblDOB.Text = "Date Of Birth*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(DD/MM/YYYY)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'TxtBxSurname
        '
        Me.TxtBxSurname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxSurname.Location = New System.Drawing.Point(119, 206)
        Me.TxtBxSurname.Name = "TxtBxSurname"
        Me.TxtBxSurname.ReadOnly = True
        Me.TxtBxSurname.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxSurname.TabIndex = 135
        '
        'LblSurname
        '
        Me.LblSurname.AutoSize = True
        Me.LblSurname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSurname.Location = New System.Drawing.Point(29, 209)
        Me.LblSurname.Name = "LblSurname"
        Me.LblSurname.Size = New System.Drawing.Size(61, 15)
        Me.LblSurname.TabIndex = 155
        Me.LblSurname.Text = "Surname*"
        '
        'TxtBxFirstName
        '
        Me.TxtBxFirstName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxFirstName.Location = New System.Drawing.Point(119, 173)
        Me.TxtBxFirstName.Name = "TxtBxFirstName"
        Me.TxtBxFirstName.ReadOnly = True
        Me.TxtBxFirstName.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxFirstName.TabIndex = 134
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFirstName.Location = New System.Drawing.Point(29, 176)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(72, 15)
        Me.LblFirstName.TabIndex = 154
        Me.LblFirstName.Text = "First Name*"
        '
        'LblTitle
        '
        Me.LblTitle.AutoSize = True
        Me.LblTitle.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTitle.Location = New System.Drawing.Point(29, 141)
        Me.LblTitle.Name = "LblTitle"
        Me.LblTitle.Size = New System.Drawing.Size(37, 15)
        Me.LblTitle.TabIndex = 153
        Me.LblTitle.Text = "Title*"
        '
        'TxtBxEmail
        '
        Me.TxtBxEmail.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxEmail.Location = New System.Drawing.Point(119, 106)
        Me.TxtBxEmail.Name = "TxtBxEmail"
        Me.TxtBxEmail.ReadOnly = True
        Me.TxtBxEmail.Size = New System.Drawing.Size(135, 23)
        Me.TxtBxEmail.TabIndex = 131
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmail.Location = New System.Drawing.Point(29, 109)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(91, 15)
        Me.LblEmail.TabIndex = 152
        Me.LblEmail.Text = "Email Address*"
        '
        'LblViewUser
        '
        Me.LblViewUser.AutoSize = True
        Me.LblViewUser.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblViewUser.Location = New System.Drawing.Point(22, 37)
        Me.LblViewUser.Name = "LblViewUser"
        Me.LblViewUser.Size = New System.Drawing.Size(97, 26)
        Me.LblViewUser.TabIndex = 151
        Me.LblViewUser.Text = "View User"
        '
        'BtnAdminBack
        '
        Me.BtnAdminBack.Location = New System.Drawing.Point(420, 49)
        Me.BtnAdminBack.Name = "BtnAdminBack"
        Me.BtnAdminBack.Size = New System.Drawing.Size(92, 35)
        Me.BtnAdminBack.TabIndex = 148
        Me.BtnAdminBack.Text = "Back"
        Me.BtnAdminBack.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(538, 24)
        Me.MenuStrip1.TabIndex = 150
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'BtnViewPets
        '
        Me.BtnViewPets.Location = New System.Drawing.Point(322, 49)
        Me.BtnViewPets.Name = "BtnViewPets"
        Me.BtnViewPets.Size = New System.Drawing.Size(92, 35)
        Me.BtnViewPets.TabIndex = 169
        Me.BtnViewPets.Text = "View Pets"
        Me.BtnViewPets.UseVisualStyleBackColor = True
        '
        'BtnEditDetails
        '
        Me.BtnEditDetails.Location = New System.Drawing.Point(224, 49)
        Me.BtnEditDetails.Name = "BtnEditDetails"
        Me.BtnEditDetails.Size = New System.Drawing.Size(92, 35)
        Me.BtnEditDetails.TabIndex = 170
        Me.BtnEditDetails.Text = "Edit Details"
        Me.BtnEditDetails.UseVisualStyleBackColor = True
        '
        'ViewUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(538, 417)
        Me.Controls.Add(Me.BtnEditDetails)
        Me.Controls.Add(Me.BtnViewPets)
        Me.Controls.Add(Me.CBPrivilageLevel)
        Me.Controls.Add(Me.CBTitle)
        Me.Controls.Add(Me.LblPrivLevel)
        Me.Controls.Add(Me.TxtBxAddPostCode)
        Me.Controls.Add(Me.LblAddPostCode)
        Me.Controls.Add(Me.TxtBxAddCountry)
        Me.Controls.Add(Me.LblAddCountry)
        Me.Controls.Add(Me.TxtBxAddCounty)
        Me.Controls.Add(Me.LblAddCounty)
        Me.Controls.Add(Me.TxtBxAddCity)
        Me.Controls.Add(Me.LblAddCity)
        Me.Controls.Add(Me.TxtBxAddRoad)
        Me.Controls.Add(Me.LblAddRoad)
        Me.Controls.Add(Me.TxtBxAddNameNo)
        Me.Controls.Add(Me.LblAddNameNo)
        Me.Controls.Add(Me.TxtBxWorkPhone)
        Me.Controls.Add(Me.LblWorkPhone)
        Me.Controls.Add(Me.TxtBxMobPhone)
        Me.Controls.Add(Me.LblMobPhone)
        Me.Controls.Add(Me.TxtBxHomePhone)
        Me.Controls.Add(Me.LblHomePhone)
        Me.Controls.Add(Me.TxtBxDOB)
        Me.Controls.Add(Me.LblDOB)
        Me.Controls.Add(Me.TxtBxSurname)
        Me.Controls.Add(Me.LblSurname)
        Me.Controls.Add(Me.TxtBxFirstName)
        Me.Controls.Add(Me.LblFirstName)
        Me.Controls.Add(Me.LblTitle)
        Me.Controls.Add(Me.TxtBxEmail)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblViewUser)
        Me.Controls.Add(Me.BtnAdminBack)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "ViewUser"
        Me.Text = "User Details"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents CBPrivilageLevel As ComboBox
    Friend WithEvents CBTitle As ComboBox
    Friend WithEvents LblPrivLevel As Label
    Friend WithEvents TxtBxAddPostCode As TextBox
    Friend WithEvents LblAddPostCode As Label
    Friend WithEvents TxtBxAddCountry As TextBox
    Friend WithEvents LblAddCountry As Label
    Friend WithEvents TxtBxAddCounty As TextBox
    Friend WithEvents LblAddCounty As Label
    Friend WithEvents TxtBxAddCity As TextBox
    Friend WithEvents LblAddCity As Label
    Friend WithEvents TxtBxAddRoad As TextBox
    Friend WithEvents LblAddRoad As Label
    Friend WithEvents TxtBxAddNameNo As TextBox
    Friend WithEvents LblAddNameNo As Label
    Friend WithEvents TxtBxWorkPhone As TextBox
    Friend WithEvents LblWorkPhone As Label
    Friend WithEvents TxtBxMobPhone As TextBox
    Friend WithEvents LblMobPhone As Label
    Friend WithEvents TxtBxHomePhone As TextBox
    Friend WithEvents LblHomePhone As Label
    Friend WithEvents TxtBxDOB As TextBox
    Friend WithEvents LblDOB As Label
    Friend WithEvents TxtBxSurname As TextBox
    Friend WithEvents LblSurname As Label
    Friend WithEvents TxtBxFirstName As TextBox
    Friend WithEvents LblFirstName As Label
    Friend WithEvents LblTitle As Label
    Friend WithEvents TxtBxEmail As TextBox
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblViewUser As Label
    Friend WithEvents BtnAdminBack As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BtnViewPets As Button
    Friend WithEvents BtnEditDetails As Button
End Class
