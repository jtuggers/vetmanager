﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StaffViewPet
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnWeights = New System.Windows.Forms.Button()
        Me.BtnSize = New System.Windows.Forms.Button()
        Me.BtnComments = New System.Windows.Forms.Button()
        Me.TxtBxAnimal = New System.Windows.Forms.TextBox()
        Me.TxtBxBreed = New System.Windows.Forms.TextBox()
        Me.TxtBxMChipNo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.RdBtnDeceased = New System.Windows.Forms.RadioButton()
        Me.RdBtnLiving = New System.Windows.Forms.RadioButton()
        Me.TxtBxPetName = New System.Windows.Forms.TextBox()
        Me.LblPassword = New System.Windows.Forms.Label()
        Me.LblMobPhone = New System.Windows.Forms.Label()
        Me.LblHomePhone = New System.Windows.Forms.Label()
        Me.LblDOB = New System.Windows.Forms.Label()
        Me.TxtBxDescription = New System.Windows.Forms.TextBox()
        Me.LblSurname = New System.Windows.Forms.Label()
        Me.TxtBxPetDob = New System.Windows.Forms.TextBox()
        Me.LblFirstName = New System.Windows.Forms.Label()
        Me.TxtBxEmail = New System.Windows.Forms.TextBox()
        Me.LblEmail = New System.Windows.Forms.Label()
        Me.LblUserManager = New System.Windows.Forms.Label()
        Me.BtnBack = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnModifyPet = New System.Windows.Forms.Button()
        Me.BtnViewOwner = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BtnWeights
        '
        Me.BtnWeights.Location = New System.Drawing.Point(238, 311)
        Me.BtnWeights.Name = "BtnWeights"
        Me.BtnWeights.Size = New System.Drawing.Size(92, 35)
        Me.BtnWeights.TabIndex = 152
        Me.BtnWeights.Text = "Weight"
        Me.BtnWeights.UseVisualStyleBackColor = True
        '
        'BtnSize
        '
        Me.BtnSize.Location = New System.Drawing.Point(336, 311)
        Me.BtnSize.Name = "BtnSize"
        Me.BtnSize.Size = New System.Drawing.Size(92, 35)
        Me.BtnSize.TabIndex = 151
        Me.BtnSize.Text = "Size"
        Me.BtnSize.UseVisualStyleBackColor = True
        '
        'BtnComments
        '
        Me.BtnComments.Location = New System.Drawing.Point(434, 311)
        Me.BtnComments.Name = "BtnComments"
        Me.BtnComments.Size = New System.Drawing.Size(92, 35)
        Me.BtnComments.TabIndex = 150
        Me.BtnComments.Text = "Comments"
        Me.BtnComments.UseVisualStyleBackColor = True
        '
        'TxtBxAnimal
        '
        Me.TxtBxAnimal.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAnimal.Location = New System.Drawing.Point(358, 100)
        Me.TxtBxAnimal.Name = "TxtBxAnimal"
        Me.TxtBxAnimal.ReadOnly = True
        Me.TxtBxAnimal.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxAnimal.TabIndex = 149
        '
        'TxtBxBreed
        '
        Me.TxtBxBreed.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxBreed.Location = New System.Drawing.Point(358, 133)
        Me.TxtBxBreed.Name = "TxtBxBreed"
        Me.TxtBxBreed.ReadOnly = True
        Me.TxtBxBreed.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxBreed.TabIndex = 148
        '
        'TxtBxMChipNo
        '
        Me.TxtBxMChipNo.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxMChipNo.Location = New System.Drawing.Point(358, 166)
        Me.TxtBxMChipNo.Name = "TxtBxMChipNo"
        Me.TxtBxMChipNo.ReadOnly = True
        Me.TxtBxMChipNo.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxMChipNo.TabIndex = 134
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(18, 321)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 15)
        Me.Label2.TabIndex = 147
        Me.Label2.Text = "Status"
        '
        'RdBtnDeceased
        '
        Me.RdBtnDeceased.AutoCheck = False
        Me.RdBtnDeceased.AutoSize = True
        Me.RdBtnDeceased.Location = New System.Drawing.Point(161, 321)
        Me.RdBtnDeceased.Name = "RdBtnDeceased"
        Me.RdBtnDeceased.Size = New System.Drawing.Size(74, 17)
        Me.RdBtnDeceased.TabIndex = 146
        Me.RdBtnDeceased.TabStop = True
        Me.RdBtnDeceased.Text = "Deceased"
        Me.RdBtnDeceased.UseVisualStyleBackColor = True
        '
        'RdBtnLiving
        '
        Me.RdBtnLiving.AutoCheck = False
        Me.RdBtnLiving.AutoSize = True
        Me.RdBtnLiving.Location = New System.Drawing.Point(102, 321)
        Me.RdBtnLiving.Name = "RdBtnLiving"
        Me.RdBtnLiving.Size = New System.Drawing.Size(53, 17)
        Me.RdBtnLiving.TabIndex = 145
        Me.RdBtnLiving.TabStop = True
        Me.RdBtnLiving.Text = "Living"
        Me.RdBtnLiving.UseVisualStyleBackColor = True
        '
        'TxtBxPetName
        '
        Me.TxtBxPetName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPetName.Location = New System.Drawing.Point(102, 133)
        Me.TxtBxPetName.Name = "TxtBxPetName"
        Me.TxtBxPetName.ReadOnly = True
        Me.TxtBxPetName.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxPetName.TabIndex = 131
        '
        'LblPassword
        '
        Me.LblPassword.AutoSize = True
        Me.LblPassword.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblPassword.Location = New System.Drawing.Point(18, 137)
        Me.LblPassword.Name = "LblPassword"
        Me.LblPassword.Size = New System.Drawing.Size(64, 15)
        Me.LblPassword.TabIndex = 144
        Me.LblPassword.Text = "Pet Name*"
        '
        'LblMobPhone
        '
        Me.LblMobPhone.AutoSize = True
        Me.LblMobPhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblMobPhone.Location = New System.Drawing.Point(290, 163)
        Me.LblMobPhone.Name = "LblMobPhone"
        Me.LblMobPhone.Size = New System.Drawing.Size(64, 30)
        Me.LblMobPhone.TabIndex = 143
        Me.LblMobPhone.Text = "Microchip" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Number*"
        '
        'LblHomePhone
        '
        Me.LblHomePhone.AutoSize = True
        Me.LblHomePhone.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblHomePhone.Location = New System.Drawing.Point(290, 137)
        Me.LblHomePhone.Name = "LblHomePhone"
        Me.LblHomePhone.Size = New System.Drawing.Size(44, 15)
        Me.LblHomePhone.TabIndex = 142
        Me.LblHomePhone.Text = "Breed*"
        '
        'LblDOB
        '
        Me.LblDOB.AutoSize = True
        Me.LblDOB.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDOB.Location = New System.Drawing.Point(290, 104)
        Me.LblDOB.Name = "LblDOB"
        Me.LblDOB.Size = New System.Drawing.Size(46, 15)
        Me.LblDOB.TabIndex = 141
        Me.LblDOB.Text = "Animal"
        '
        'TxtBxDescription
        '
        Me.TxtBxDescription.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxDescription.Location = New System.Drawing.Point(102, 199)
        Me.TxtBxDescription.Multiline = True
        Me.TxtBxDescription.Name = "TxtBxDescription"
        Me.TxtBxDescription.ReadOnly = True
        Me.TxtBxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TxtBxDescription.Size = New System.Drawing.Size(423, 96)
        Me.TxtBxDescription.TabIndex = 133
        '
        'LblSurname
        '
        Me.LblSurname.AutoSize = True
        Me.LblSurname.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSurname.Location = New System.Drawing.Point(18, 231)
        Me.LblSurname.Name = "LblSurname"
        Me.LblSurname.Size = New System.Drawing.Size(76, 30)
        Me.LblSurname.TabIndex = 140
        Me.LblSurname.Text = "Animal" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Description*"
        '
        'TxtBxPetDob
        '
        Me.TxtBxPetDob.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPetDob.Location = New System.Drawing.Point(102, 166)
        Me.TxtBxPetDob.Name = "TxtBxPetDob"
        Me.TxtBxPetDob.ReadOnly = True
        Me.TxtBxPetDob.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxPetDob.TabIndex = 132
        '
        'LblFirstName
        '
        Me.LblFirstName.AutoSize = True
        Me.LblFirstName.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblFirstName.Location = New System.Drawing.Point(18, 156)
        Me.LblFirstName.Name = "LblFirstName"
        Me.LblFirstName.Size = New System.Drawing.Size(87, 45)
        Me.LblFirstName.TabIndex = 139
        Me.LblFirstName.Text = "Pet DOB*" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(Approx)" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "(DD/MM/YYYY)"
        '
        'TxtBxEmail
        '
        Me.TxtBxEmail.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxEmail.Location = New System.Drawing.Point(102, 100)
        Me.TxtBxEmail.Name = "TxtBxEmail"
        Me.TxtBxEmail.ReadOnly = True
        Me.TxtBxEmail.Size = New System.Drawing.Size(168, 23)
        Me.TxtBxEmail.TabIndex = 130
        '
        'LblEmail
        '
        Me.LblEmail.AutoSize = True
        Me.LblEmail.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblEmail.Location = New System.Drawing.Point(18, 97)
        Me.LblEmail.Name = "LblEmail"
        Me.LblEmail.Size = New System.Drawing.Size(80, 30)
        Me.LblEmail.TabIndex = 138
        Me.LblEmail.Text = "Owner Email " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Address*"
        '
        'LblUserManager
        '
        Me.LblUserManager.AutoSize = True
        Me.LblUserManager.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUserManager.Location = New System.Drawing.Point(20, 48)
        Me.LblUserManager.Name = "LblUserManager"
        Me.LblUserManager.Size = New System.Drawing.Size(87, 26)
        Me.LblUserManager.TabIndex = 137
        Me.LblUserManager.Text = "View Pet"
        '
        'BtnBack
        '
        Me.BtnBack.Location = New System.Drawing.Point(434, 48)
        Me.BtnBack.Name = "BtnBack"
        Me.BtnBack.Size = New System.Drawing.Size(92, 35)
        Me.BtnBack.TabIndex = 136
        Me.BtnBack.Text = "Back"
        Me.BtnBack.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(551, 24)
        Me.MenuStrip1.TabIndex = 135
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'BtnModifyPet
        '
        Me.BtnModifyPet.Location = New System.Drawing.Point(336, 48)
        Me.BtnModifyPet.Name = "BtnModifyPet"
        Me.BtnModifyPet.Size = New System.Drawing.Size(92, 35)
        Me.BtnModifyPet.TabIndex = 153
        Me.BtnModifyPet.Text = "Modify Pet"
        Me.BtnModifyPet.UseVisualStyleBackColor = True
        '
        'BtnViewOwner
        '
        Me.BtnViewOwner.Location = New System.Drawing.Point(238, 48)
        Me.BtnViewOwner.Name = "BtnViewOwner"
        Me.BtnViewOwner.Size = New System.Drawing.Size(92, 35)
        Me.BtnViewOwner.TabIndex = 154
        Me.BtnViewOwner.Text = "View Owner"
        Me.BtnViewOwner.UseVisualStyleBackColor = True
        '
        'StaffViewPet
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 383)
        Me.Controls.Add(Me.BtnViewOwner)
        Me.Controls.Add(Me.BtnModifyPet)
        Me.Controls.Add(Me.BtnWeights)
        Me.Controls.Add(Me.BtnSize)
        Me.Controls.Add(Me.BtnComments)
        Me.Controls.Add(Me.TxtBxAnimal)
        Me.Controls.Add(Me.TxtBxBreed)
        Me.Controls.Add(Me.TxtBxMChipNo)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.RdBtnDeceased)
        Me.Controls.Add(Me.RdBtnLiving)
        Me.Controls.Add(Me.TxtBxPetName)
        Me.Controls.Add(Me.LblPassword)
        Me.Controls.Add(Me.LblMobPhone)
        Me.Controls.Add(Me.LblHomePhone)
        Me.Controls.Add(Me.LblDOB)
        Me.Controls.Add(Me.TxtBxDescription)
        Me.Controls.Add(Me.LblSurname)
        Me.Controls.Add(Me.TxtBxPetDob)
        Me.Controls.Add(Me.LblFirstName)
        Me.Controls.Add(Me.TxtBxEmail)
        Me.Controls.Add(Me.LblEmail)
        Me.Controls.Add(Me.LblUserManager)
        Me.Controls.Add(Me.BtnBack)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "StaffViewPet"
        Me.Text = "Pet Details"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnWeights As Button
    Friend WithEvents BtnSize As Button
    Friend WithEvents BtnComments As Button
    Friend WithEvents TxtBxAnimal As TextBox
    Friend WithEvents TxtBxBreed As TextBox
    Friend WithEvents TxtBxMChipNo As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents RdBtnDeceased As RadioButton
    Friend WithEvents RdBtnLiving As RadioButton
    Friend WithEvents TxtBxPetName As TextBox
    Friend WithEvents LblPassword As Label
    Friend WithEvents LblMobPhone As Label
    Friend WithEvents LblHomePhone As Label
    Friend WithEvents LblDOB As Label
    Friend WithEvents TxtBxDescription As TextBox
    Friend WithEvents LblSurname As Label
    Friend WithEvents TxtBxPetDob As TextBox
    Friend WithEvents LblFirstName As Label
    Friend WithEvents TxtBxEmail As TextBox
    Friend WithEvents LblEmail As Label
    Friend WithEvents LblUserManager As Label
    Friend WithEvents BtnBack As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BtnModifyPet As Button
    Friend WithEvents BtnViewOwner As Button
End Class
