﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UserPets
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.RdBtnAllAnimals = New System.Windows.Forms.RadioButton()
        Me.LblRowCount = New System.Windows.Forms.Label()
        Me.BtnRefresh = New System.Windows.Forms.Button()
        Me.BtnNewPet = New System.Windows.Forms.Button()
        Me.RdBtnDeceased = New System.Windows.Forms.RadioButton()
        Me.RdBtnLiving = New System.Windows.Forms.RadioButton()
        Me.BtnViewPet = New System.Windows.Forms.Button()
        Me.DGVPetManager = New System.Windows.Forms.DataGridView()
        Me.LblClientHome = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        CType(Me.DGVPetManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(479, 36)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(87, 35)
        Me.Button1.TabIndex = 62
        Me.Button1.Text = "Back"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'RdBtnAllAnimals
        '
        Me.RdBtnAllAnimals.AutoSize = True
        Me.RdBtnAllAnimals.Checked = True
        Me.RdBtnAllAnimals.Location = New System.Drawing.Point(24, 68)
        Me.RdBtnAllAnimals.Name = "RdBtnAllAnimals"
        Me.RdBtnAllAnimals.Size = New System.Drawing.Size(60, 17)
        Me.RdBtnAllAnimals.TabIndex = 50
        Me.RdBtnAllAnimals.TabStop = True
        Me.RdBtnAllAnimals.Text = "All Pets"
        Me.RdBtnAllAnimals.UseVisualStyleBackColor = True
        '
        'LblRowCount
        '
        Me.LblRowCount.AutoSize = True
        Me.LblRowCount.Location = New System.Drawing.Point(18, 113)
        Me.LblRowCount.Name = "LblRowCount"
        Me.LblRowCount.Size = New System.Drawing.Size(64, 13)
        Me.LblRowCount.TabIndex = 57
        Me.LblRowCount.Text = "RowsFound"
        '
        'BtnRefresh
        '
        Me.BtnRefresh.Location = New System.Drawing.Point(367, 36)
        Me.BtnRefresh.Name = "BtnRefresh"
        Me.BtnRefresh.Size = New System.Drawing.Size(87, 35)
        Me.BtnRefresh.TabIndex = 58
        Me.BtnRefresh.Text = "Refresh"
        Me.BtnRefresh.UseVisualStyleBackColor = True
        '
        'BtnNewPet
        '
        Me.BtnNewPet.Location = New System.Drawing.Point(479, 77)
        Me.BtnNewPet.Name = "BtnNewPet"
        Me.BtnNewPet.Size = New System.Drawing.Size(87, 35)
        Me.BtnNewPet.TabIndex = 60
        Me.BtnNewPet.Text = "Add Pet"
        Me.BtnNewPet.UseVisualStyleBackColor = True
        '
        'RdBtnDeceased
        '
        Me.RdBtnDeceased.AutoSize = True
        Me.RdBtnDeceased.Location = New System.Drawing.Point(158, 68)
        Me.RdBtnDeceased.Name = "RdBtnDeceased"
        Me.RdBtnDeceased.Size = New System.Drawing.Size(74, 17)
        Me.RdBtnDeceased.TabIndex = 52
        Me.RdBtnDeceased.Text = "Deceased"
        Me.RdBtnDeceased.UseVisualStyleBackColor = True
        '
        'RdBtnLiving
        '
        Me.RdBtnLiving.AutoSize = True
        Me.RdBtnLiving.Location = New System.Drawing.Point(96, 68)
        Me.RdBtnLiving.Name = "RdBtnLiving"
        Me.RdBtnLiving.Size = New System.Drawing.Size(48, 17)
        Me.RdBtnLiving.TabIndex = 51
        Me.RdBtnLiving.Text = "Alive"
        Me.RdBtnLiving.UseVisualStyleBackColor = True
        '
        'BtnViewPet
        '
        Me.BtnViewPet.Location = New System.Drawing.Point(367, 77)
        Me.BtnViewPet.Name = "BtnViewPet"
        Me.BtnViewPet.Size = New System.Drawing.Size(87, 35)
        Me.BtnViewPet.TabIndex = 59
        Me.BtnViewPet.Text = "View Details"
        Me.BtnViewPet.UseVisualStyleBackColor = True
        '
        'DGVPetManager
        '
        Me.DGVPetManager.AllowUserToAddRows = False
        Me.DGVPetManager.AllowUserToDeleteRows = False
        Me.DGVPetManager.AllowUserToResizeColumns = False
        Me.DGVPetManager.AllowUserToResizeRows = False
        Me.DGVPetManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVPetManager.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGVPetManager.Location = New System.Drawing.Point(18, 129)
        Me.DGVPetManager.MultiSelect = False
        Me.DGVPetManager.Name = "DGVPetManager"
        Me.DGVPetManager.ReadOnly = True
        Me.DGVPetManager.RowHeadersVisible = False
        Me.DGVPetManager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVPetManager.ShowCellErrors = False
        Me.DGVPetManager.ShowCellToolTips = False
        Me.DGVPetManager.ShowEditingIcon = False
        Me.DGVPetManager.ShowRowErrors = False
        Me.DGVPetManager.Size = New System.Drawing.Size(567, 232)
        Me.DGVPetManager.TabIndex = 61
        '
        'LblClientHome
        '
        Me.LblClientHome.AutoSize = True
        Me.LblClientHome.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblClientHome.Location = New System.Drawing.Point(13, 36)
        Me.LblClientHome.Name = "LblClientHome"
        Me.LblClientHome.Size = New System.Drawing.Size(117, 26)
        Me.LblClientHome.TabIndex = 49
        Me.LblClientHome.Text = "Pets By User"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(614, 24)
        Me.MenuStrip1.TabIndex = 48
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'UserPets
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 412)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.RdBtnAllAnimals)
        Me.Controls.Add(Me.LblRowCount)
        Me.Controls.Add(Me.BtnRefresh)
        Me.Controls.Add(Me.BtnNewPet)
        Me.Controls.Add(Me.RdBtnDeceased)
        Me.Controls.Add(Me.RdBtnLiving)
        Me.Controls.Add(Me.BtnViewPet)
        Me.Controls.Add(Me.DGVPetManager)
        Me.Controls.Add(Me.LblClientHome)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "UserPets"
        Me.Text = "User Associated Pets"
        CType(Me.DGVPetManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents RdBtnAllAnimals As RadioButton
    Friend WithEvents LblRowCount As Label
    Friend WithEvents BtnRefresh As Button
    Friend WithEvents BtnNewPet As Button
    Friend WithEvents RdBtnDeceased As RadioButton
    Friend WithEvents RdBtnLiving As RadioButton
    Friend WithEvents BtnViewPet As Button
    Friend WithEvents DGVPetManager As DataGridView
    Friend WithEvents LblClientHome As Label
    Friend WithEvents MenuStrip1 As MenuStrip
End Class
