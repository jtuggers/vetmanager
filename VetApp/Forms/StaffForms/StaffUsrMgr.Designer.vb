﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class StaffUsrMgr
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LblRowCount = New System.Windows.Forms.Label()
        Me.BtnRefresh = New System.Windows.Forms.Button()
        Me.BtnSearch = New System.Windows.Forms.Button()
        Me.BtnNewUser = New System.Windows.Forms.Button()
        Me.CBSearchFor = New System.Windows.Forms.ComboBox()
        Me.TxtBxSearch = New System.Windows.Forms.TextBox()
        Me.LblSearch = New System.Windows.Forms.Label()
        Me.DGVUserManager = New System.Windows.Forms.DataGridView()
        Me.LblUserManager = New System.Windows.Forms.Label()
        Me.BtnAdminBack = New System.Windows.Forms.Button()
        Me.BtnDisableUser = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.DGVUserManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(614, 24)
        Me.MenuStrip1.TabIndex = 33
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(44, 20)
        Me.ToolStripMenuItem1.Text = "Help"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(80, 22)
        Me.ToolStripMenuItem2.Text = "E"
        '
        'LblRowCount
        '
        Me.LblRowCount.AutoSize = True
        Me.LblRowCount.Location = New System.Drawing.Point(24, 139)
        Me.LblRowCount.Name = "LblRowCount"
        Me.LblRowCount.Size = New System.Drawing.Size(64, 13)
        Me.LblRowCount.TabIndex = 27
        Me.LblRowCount.Text = "RowsFound"
        '
        'BtnRefresh
        '
        Me.BtnRefresh.Location = New System.Drawing.Point(481, 100)
        Me.BtnRefresh.Name = "BtnRefresh"
        Me.BtnRefresh.Size = New System.Drawing.Size(87, 35)
        Me.BtnRefresh.TabIndex = 28
        Me.BtnRefresh.Text = "Refresh"
        Me.BtnRefresh.UseVisualStyleBackColor = True
        '
        'BtnSearch
        '
        Me.BtnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.BtnSearch.Location = New System.Drawing.Point(274, 110)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(75, 26)
        Me.BtnSearch.TabIndex = 26
        Me.BtnSearch.Text = "Search"
        Me.BtnSearch.UseVisualStyleBackColor = True
        '
        'BtnNewUser
        '
        Me.BtnNewUser.Location = New System.Drawing.Point(388, 101)
        Me.BtnNewUser.Name = "BtnNewUser"
        Me.BtnNewUser.Size = New System.Drawing.Size(87, 34)
        Me.BtnNewUser.TabIndex = 30
        Me.BtnNewUser.Text = "Create New User"
        Me.BtnNewUser.UseVisualStyleBackColor = True
        '
        'CBSearchFor
        '
        Me.CBSearchFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBSearchFor.FormattingEnabled = True
        Me.CBSearchFor.Items.AddRange(New Object() {"User ID", "Email", "Title", "First Name", "Last Name"})
        Me.CBSearchFor.Location = New System.Drawing.Point(96, 78)
        Me.CBSearchFor.MaxDropDownItems = 5
        Me.CBSearchFor.Name = "CBSearchFor"
        Me.CBSearchFor.Size = New System.Drawing.Size(253, 21)
        Me.CBSearchFor.TabIndex = 24
        '
        'TxtBxSearch
        '
        Me.TxtBxSearch.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxSearch.Location = New System.Drawing.Point(24, 110)
        Me.TxtBxSearch.Name = "TxtBxSearch"
        Me.TxtBxSearch.Size = New System.Drawing.Size(244, 26)
        Me.TxtBxSearch.TabIndex = 25
        '
        'LblSearch
        '
        Me.LblSearch.AutoSize = True
        Me.LblSearch.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSearch.Location = New System.Drawing.Point(23, 78)
        Me.LblSearch.Name = "LblSearch"
        Me.LblSearch.Size = New System.Drawing.Size(71, 19)
        Me.LblSearch.TabIndex = 23
        Me.LblSearch.Text = "Search by"
        '
        'DGVUserManager
        '
        Me.DGVUserManager.AllowUserToAddRows = False
        Me.DGVUserManager.AllowUserToDeleteRows = False
        Me.DGVUserManager.AllowUserToResizeColumns = False
        Me.DGVUserManager.AllowUserToResizeRows = False
        Me.DGVUserManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVUserManager.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGVUserManager.Location = New System.Drawing.Point(24, 156)
        Me.DGVUserManager.MultiSelect = False
        Me.DGVUserManager.Name = "DGVUserManager"
        Me.DGVUserManager.ReadOnly = True
        Me.DGVUserManager.RowHeadersVisible = False
        Me.DGVUserManager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVUserManager.ShowCellErrors = False
        Me.DGVUserManager.ShowCellToolTips = False
        Me.DGVUserManager.ShowEditingIcon = False
        Me.DGVUserManager.ShowRowErrors = False
        Me.DGVUserManager.Size = New System.Drawing.Size(567, 232)
        Me.DGVUserManager.TabIndex = 32
        '
        'LblUserManager
        '
        Me.LblUserManager.AutoSize = True
        Me.LblUserManager.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUserManager.Location = New System.Drawing.Point(16, 42)
        Me.LblUserManager.Name = "LblUserManager"
        Me.LblUserManager.Size = New System.Drawing.Size(131, 26)
        Me.LblUserManager.TabIndex = 17
        Me.LblUserManager.Text = "User Manager"
        '
        'BtnAdminBack
        '
        Me.BtnAdminBack.Location = New System.Drawing.Point(481, 59)
        Me.BtnAdminBack.Name = "BtnAdminBack"
        Me.BtnAdminBack.Size = New System.Drawing.Size(87, 35)
        Me.BtnAdminBack.TabIndex = 29
        Me.BtnAdminBack.Text = "Back"
        Me.BtnAdminBack.UseVisualStyleBackColor = True
        '
        'BtnDisableUser
        '
        Me.BtnDisableUser.Enabled = False
        Me.BtnDisableUser.Location = New System.Drawing.Point(388, 59)
        Me.BtnDisableUser.Name = "BtnDisableUser"
        Me.BtnDisableUser.Size = New System.Drawing.Size(87, 35)
        Me.BtnDisableUser.TabIndex = 34
        Me.BtnDisableUser.Text = "Disable Account"
        Me.BtnDisableUser.UseVisualStyleBackColor = True
        '
        'StaffUsrMgr
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 429)
        Me.Controls.Add(Me.BtnDisableUser)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.LblRowCount)
        Me.Controls.Add(Me.BtnRefresh)
        Me.Controls.Add(Me.BtnSearch)
        Me.Controls.Add(Me.BtnNewUser)
        Me.Controls.Add(Me.CBSearchFor)
        Me.Controls.Add(Me.TxtBxSearch)
        Me.Controls.Add(Me.LblSearch)
        Me.Controls.Add(Me.DGVUserManager)
        Me.Controls.Add(Me.LblUserManager)
        Me.Controls.Add(Me.BtnAdminBack)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "StaffUsrMgr"
        Me.Text = "User Manager"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.DGVUserManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents LblRowCount As Label
    Friend WithEvents BtnRefresh As Button
    Friend WithEvents BtnSearch As Button
    Friend WithEvents BtnNewUser As Button
    Friend WithEvents CBSearchFor As ComboBox
    Friend WithEvents TxtBxSearch As TextBox
    Friend WithEvents LblSearch As Label
    Friend WithEvents DGVUserManager As DataGridView
    Friend WithEvents LblUserManager As Label
    Friend WithEvents BtnAdminBack As Button
    Friend WithEvents BtnDisableUser As Button
End Class
