﻿Public Class ConfigLogin
    Dim Admin As New Admin
    Private Sub BtnLogin_Click(sender As Object, e As EventArgs) Handles BtnLogin.Click

        If TxtBxConfigPass.Text = My.Settings.ConfigPassword Then
            DBConnSettings.Show()
            Me.Close()
        Else
            MsgBox("Incorrect Password")
        End If
    End Sub

    Private Sub ConfigLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Admin.GetConfigPass()
        LblPassVers.Text = "Password Version = " + My.Settings.PasswordVersion
    End Sub

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub
End Class