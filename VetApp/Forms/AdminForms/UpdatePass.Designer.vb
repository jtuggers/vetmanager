﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class UpdatePass
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtBxPass = New System.Windows.Forms.TextBox()
        Me.LblAddedUser = New System.Windows.Forms.Label()
        Me.BtnAddWeight = New System.Windows.Forms.Button()
        Me.LblConfig = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'TxtBxPass
        '
        Me.TxtBxPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPass.Location = New System.Drawing.Point(18, 106)
        Me.TxtBxPass.Name = "TxtBxPass"
        Me.TxtBxPass.Size = New System.Drawing.Size(187, 20)
        Me.TxtBxPass.TabIndex = 145
        '
        'LblAddedUser
        '
        Me.LblAddedUser.AutoSize = True
        Me.LblAddedUser.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddedUser.Location = New System.Drawing.Point(29, 67)
        Me.LblAddedUser.Name = "LblAddedUser"
        Me.LblAddedUser.Size = New System.Drawing.Size(176, 36)
        Me.LblAddedUser.TabIndex = 144
        Me.LblAddedUser.Text = "Please enter new Database" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Configuration Password"
        Me.LblAddedUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'BtnAddWeight
        '
        Me.BtnAddWeight.Location = New System.Drawing.Point(18, 137)
        Me.BtnAddWeight.Name = "BtnAddWeight"
        Me.BtnAddWeight.Size = New System.Drawing.Size(92, 34)
        Me.BtnAddWeight.TabIndex = 141
        Me.BtnAddWeight.Text = "Change Password"
        Me.BtnAddWeight.UseVisualStyleBackColor = True
        '
        'LblConfig
        '
        Me.LblConfig.AutoSize = True
        Me.LblConfig.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConfig.Location = New System.Drawing.Point(31, 31)
        Me.LblConfig.Name = "LblConfig"
        Me.LblConfig.Size = New System.Drawing.Size(161, 26)
        Me.LblConfig.TabIndex = 140
        Me.LblConfig.Text = "Change Password"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(113, 137)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 34)
        Me.BtnClose.TabIndex = 139
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'UpdatePass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(224, 206)
        Me.Controls.Add(Me.TxtBxPass)
        Me.Controls.Add(Me.LblAddedUser)
        Me.Controls.Add(Me.BtnAddWeight)
        Me.Controls.Add(Me.LblConfig)
        Me.Controls.Add(Me.BtnClose)
        Me.Name = "UpdatePass"
        Me.Text = "UpdatePass"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtBxPass As TextBox
    Friend WithEvents LblAddedUser As Label
    Friend WithEvents BtnAddWeight As Button
    Friend WithEvents LblConfig As Label
    Friend WithEvents BtnClose As Button
End Class
