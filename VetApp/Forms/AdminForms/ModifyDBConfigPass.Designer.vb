﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ModifyDBConfigPass
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtBxPass = New System.Windows.Forms.TextBox()
        Me.LblAddedUser = New System.Windows.Forms.Label()
        Me.TxtBxPassID = New System.Windows.Forms.TextBox()
        Me.LblWeightID = New System.Windows.Forms.Label()
        Me.BtnAddWeight = New System.Windows.Forms.Button()
        Me.LblConfig = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.ListBxHistory = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'TxtBxPass
        '
        Me.TxtBxPass.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPass.Location = New System.Drawing.Point(182, 154)
        Me.TxtBxPass.Name = "TxtBxPass"
        Me.TxtBxPass.ReadOnly = True
        Me.TxtBxPass.Size = New System.Drawing.Size(187, 20)
        Me.TxtBxPass.TabIndex = 136
        '
        'LblAddedUser
        '
        Me.LblAddedUser.AutoSize = True
        Me.LblAddedUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddedUser.Location = New System.Drawing.Point(128, 157)
        Me.LblAddedUser.Name = "LblAddedUser"
        Me.LblAddedUser.Size = New System.Drawing.Size(53, 13)
        Me.LblAddedUser.TabIndex = 135
        Me.LblAddedUser.Text = "Password"
        '
        'TxtBxPassID
        '
        Me.TxtBxPassID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxPassID.Location = New System.Drawing.Point(182, 116)
        Me.TxtBxPassID.Name = "TxtBxPassID"
        Me.TxtBxPassID.ReadOnly = True
        Me.TxtBxPassID.Size = New System.Drawing.Size(187, 20)
        Me.TxtBxPassID.TabIndex = 129
        '
        'LblWeightID
        '
        Me.LblWeightID.AutoSize = True
        Me.LblWeightID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWeightID.Location = New System.Drawing.Point(138, 116)
        Me.LblWeightID.Name = "LblWeightID"
        Me.LblWeightID.Size = New System.Drawing.Size(38, 26)
        Me.LblWeightID.TabIndex = 128
        Me.LblWeightID.Text = "Date" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Issued" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'BtnAddWeight
        '
        Me.BtnAddWeight.Location = New System.Drawing.Point(182, 66)
        Me.BtnAddWeight.Name = "BtnAddWeight"
        Me.BtnAddWeight.Size = New System.Drawing.Size(92, 34)
        Me.BtnAddWeight.TabIndex = 127
        Me.BtnAddWeight.Text = "Change Password"
        Me.BtnAddWeight.UseVisualStyleBackColor = True
        '
        'LblConfig
        '
        Me.LblConfig.AutoSize = True
        Me.LblConfig.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblConfig.Location = New System.Drawing.Point(21, 27)
        Me.LblConfig.Name = "LblConfig"
        Me.LblConfig.Size = New System.Drawing.Size(165, 26)
        Me.LblConfig.TabIndex = 126
        Me.LblConfig.Text = "Database Security" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(280, 66)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(92, 34)
        Me.BtnClose.TabIndex = 125
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'ListBxHistory
        '
        Me.ListBxHistory.FormattingEnabled = True
        Me.ListBxHistory.Location = New System.Drawing.Point(26, 80)
        Me.ListBxHistory.Name = "ListBxHistory"
        Me.ListBxHistory.ScrollAlwaysVisible = True
        Me.ListBxHistory.Size = New System.Drawing.Size(101, 108)
        Me.ListBxHistory.TabIndex = 124
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 13)
        Me.Label1.TabIndex = 137
        Me.Label1.Text = "Password Version"
        '
        'ModifyDBConfigPass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 217)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtBxPass)
        Me.Controls.Add(Me.LblAddedUser)
        Me.Controls.Add(Me.TxtBxPassID)
        Me.Controls.Add(Me.LblWeightID)
        Me.Controls.Add(Me.BtnAddWeight)
        Me.Controls.Add(Me.LblConfig)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.ListBxHistory)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "ModifyDBConfigPass"
        Me.Text = "ModifyDBConfigPass"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TxtBxPass As TextBox
    Friend WithEvents LblAddedUser As Label
    Friend WithEvents TxtBxPassID As TextBox
    Friend WithEvents LblWeightID As Label
    Friend WithEvents BtnAddWeight As Button
    Friend WithEvents LblConfig As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents ListBxHistory As ListBox
    Friend WithEvents Label1 As Label
End Class
