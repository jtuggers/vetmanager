﻿Public Class AdminChangePass
    Dim User As New User
    Private Sub AdminChangePass_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim UserID As Integer = Globals.ModifyID
        Dim Email As String = User.GetEmail()

        Dim NewPass As String = TxtBxNewPass.Text
        Dim ConfNewPass As String = TxtBxConfNewPass.Text

        TxtBxNewPass.Focus()
        TxtBxConfNewPass.Focus()



        If NewPass = ConfNewPass Then
            Try
                User.ChangeUserPass(Email, NewPass)
                MessageBox.Show("Password changed.")
            Catch
                MessageBox.Show("Password could not be changed.")
            End Try

            Me.Close()

        Else
            MessageBox.Show("Passwords must match. ")
        End If
    End Sub
End Class