﻿Public Class AdminHome
    Dim User As New User

    Private Sub AdminHome_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim Name As String = User.GetName(Globals.UserID)
        LblLoggedInName.Text = Name
    End Sub

    Private Sub BtnAdminLogout_Click(sender As Object, e As EventArgs) Handles BtnAdminLogout.Click
        Login.Show()
        Me.Close()
    End Sub

    Private Sub BtnAdminUsrMan_Click(sender As Object, e As EventArgs) Handles BtnAdminUsrMan.Click
        UserManager.Show()
        Me.Close()
    End Sub

    Private Sub BtnAdminPetMan_Click(sender As Object, e As EventArgs) Handles BtnAdminPetMan.Click
        PetManager.ShowDialog()
    End Sub

    Private Sub BtnMyAccount_Click(sender As Object, e As EventArgs) Handles BtnMyAccount.Click
        MyAccount.ShowDialog()
    End Sub

    Private Sub BtnSecurity_Click(sender As Object, e As EventArgs) Handles BtnSecurity.Click
        ModifyDBConfigPass.ShowDialog()
    End Sub
End Class