﻿Public Class ModifyDBConfigPass
    Dim Admin As New Admin
    Private Sub ModifyDBConfigPass_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ListBxHistory.Items.Clear()
        Dim Details As List(Of String) = Admin.GetPasses()
        For Each Item In Details
            ListBxHistory.Items.Add(Item)
        Next
    End Sub

    Private Sub ListBxHistory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBxHistory.SelectedIndexChanged
        Dim Selected As String = ListBxHistory.GetItemText(ListBxHistory.SelectedItem)
        Dim Details() As String = Admin.GetPassDetails(Selected)

        TxtBxPassID.Text = Details(0)
        TxtBxPass.Text = Details(1)


    End Sub

    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnAddWeight_Click(sender As Object, e As EventArgs) Handles BtnAddWeight.Click
        UpdatePass.Show()
    End Sub
End Class