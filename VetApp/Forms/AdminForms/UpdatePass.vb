﻿Public Class UpdatePass
    Dim Admin As New Admin
    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub

    Private Sub BtnAddWeight_Click(sender As Object, e As EventArgs) Handles BtnAddWeight.Click
        If Len(TxtBxPass.Text) < 5 Then
            MessageBox.Show("Password must be at least 5 characters long.")
            Return
        End If
        Try
            Admin.UpdatePass(TxtBxPass.Text)
            MessageBox.Show("Password changed")
            Me.Close()
        Catch
            MessageBox.Show("An error occured when attempting to change the password. Please try again later.")
        End Try
    End Sub
End Class