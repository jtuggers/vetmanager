﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminHome
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BtnAdminLogout = New System.Windows.Forms.Button()
        Me.LblSignedInAs = New System.Windows.Forms.Label()
        Me.LblLoggedInName = New System.Windows.Forms.Label()
        Me.LblAdminTools = New System.Windows.Forms.Label()
        Me.BtnAdminUsrMan = New System.Windows.Forms.Button()
        Me.BtnSecurity = New System.Windows.Forms.Button()
        Me.BtnAdminPetMan = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.BtnMyAccount = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(119, 137)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(0, 13)
        Me.Label1.TabIndex = 0
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(417, 24)
        Me.MenuStrip1.TabIndex = 1
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'BtnAdminLogout
        '
        Me.BtnAdminLogout.Location = New System.Drawing.Point(284, 69)
        Me.BtnAdminLogout.Name = "BtnAdminLogout"
        Me.BtnAdminLogout.Size = New System.Drawing.Size(92, 35)
        Me.BtnAdminLogout.TabIndex = 2
        Me.BtnAdminLogout.Text = "Sign Out"
        Me.BtnAdminLogout.UseVisualStyleBackColor = True
        '
        'LblSignedInAs
        '
        Me.LblSignedInAs.AutoSize = True
        Me.LblSignedInAs.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSignedInAs.Location = New System.Drawing.Point(180, 69)
        Me.LblSignedInAs.Name = "LblSignedInAs"
        Me.LblSignedInAs.Size = New System.Drawing.Size(103, 16)
        Me.LblSignedInAs.TabIndex = 3
        Me.LblSignedInAs.Text = "Welcome Back,"
        '
        'LblLoggedInName
        '
        Me.LblLoggedInName.AutoSize = True
        Me.LblLoggedInName.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLoggedInName.Location = New System.Drawing.Point(180, 85)
        Me.LblLoggedInName.Name = "LblLoggedInName"
        Me.LblLoggedInName.Size = New System.Drawing.Size(73, 16)
        Me.LblLoggedInName.TabIndex = 4
        Me.LblLoggedInName.Text = "First Name"
        '
        'LblAdminTools
        '
        Me.LblAdminTools.AutoSize = True
        Me.LblAdminTools.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAdminTools.Location = New System.Drawing.Point(12, 34)
        Me.LblAdminTools.Name = "LblAdminTools"
        Me.LblAdminTools.Size = New System.Drawing.Size(184, 26)
        Me.LblAdminTools.TabIndex = 5
        Me.LblAdminTools.Text = "Administrative Tools"
        '
        'BtnAdminUsrMan
        '
        Me.BtnAdminUsrMan.Location = New System.Drawing.Point(32, 110)
        Me.BtnAdminUsrMan.Name = "BtnAdminUsrMan"
        Me.BtnAdminUsrMan.Size = New System.Drawing.Size(160, 67)
        Me.BtnAdminUsrMan.TabIndex = 6
        Me.BtnAdminUsrMan.Text = "User Manager"
        Me.BtnAdminUsrMan.UseVisualStyleBackColor = True
        '
        'BtnSecurity
        '
        Me.BtnSecurity.Location = New System.Drawing.Point(32, 191)
        Me.BtnSecurity.Name = "BtnSecurity"
        Me.BtnSecurity.Size = New System.Drawing.Size(160, 67)
        Me.BtnSecurity.TabIndex = 9
        Me.BtnSecurity.Text = "Security"
        Me.BtnSecurity.UseVisualStyleBackColor = True
        '
        'BtnAdminPetMan
        '
        Me.BtnAdminPetMan.Location = New System.Drawing.Point(216, 110)
        Me.BtnAdminPetMan.Name = "BtnAdminPetMan"
        Me.BtnAdminPetMan.Size = New System.Drawing.Size(160, 67)
        Me.BtnAdminPetMan.TabIndex = 7
        Me.BtnAdminPetMan.Text = "Pet Manager"
        Me.BtnAdminPetMan.UseVisualStyleBackColor = True
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(216, 191)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(160, 67)
        Me.Button5.TabIndex = 10
        Me.Button5.Text = "Manage Animals and Breeds"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(32, 272)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(160, 67)
        Me.Button6.TabIndex = 11
        Me.Button6.Text = "View Error Logs"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'BtnMyAccount
        '
        Me.BtnMyAccount.Location = New System.Drawing.Point(216, 272)
        Me.BtnMyAccount.Name = "BtnMyAccount"
        Me.BtnMyAccount.Size = New System.Drawing.Size(160, 67)
        Me.BtnMyAccount.TabIndex = 27
        Me.BtnMyAccount.Text = "My Account"
        Me.BtnMyAccount.UseVisualStyleBackColor = True
        '
        'AdminHome
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(417, 379)
        Me.Controls.Add(Me.BtnMyAccount)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.BtnSecurity)
        Me.Controls.Add(Me.BtnAdminPetMan)
        Me.Controls.Add(Me.BtnAdminUsrMan)
        Me.Controls.Add(Me.LblAdminTools)
        Me.Controls.Add(Me.LblLoggedInName)
        Me.Controls.Add(Me.LblSignedInAs)
        Me.Controls.Add(Me.BtnAdminLogout)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "AdminHome"
        Me.Text = "AdminHome"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BtnAdminLogout As Button
    Friend WithEvents LblSignedInAs As Label
    Friend WithEvents LblLoggedInName As Label
    Friend WithEvents LblAdminTools As Label
    Friend WithEvents BtnAdminUsrMan As Button
    Friend WithEvents BtnSecurity As Button
    Friend WithEvents BtnAdminPetMan As Button
    Friend WithEvents Button5 As Button
    Friend WithEvents Button6 As Button
    Friend WithEvents BtnMyAccount As Button
End Class
