﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class UserManager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LblUserManager = New System.Windows.Forms.Label()
        Me.BtnAdminBack = New System.Windows.Forms.Button()
        Me.DGVUserManager = New System.Windows.Forms.DataGridView()
        Me.TxtBxSearch = New System.Windows.Forms.TextBox()
        Me.LblSearch = New System.Windows.Forms.Label()
        Me.CBSearchFor = New System.Windows.Forms.ComboBox()
        Me.BtnNewUser = New System.Windows.Forms.Button()
        Me.BtnSearch = New System.Windows.Forms.Button()
        Me.RdBtnAllUsers = New System.Windows.Forms.RadioButton()
        Me.RdBtnClients = New System.Windows.Forms.RadioButton()
        Me.RdBtnStaff = New System.Windows.Forms.RadioButton()
        Me.RdBtnAccLocked = New System.Windows.Forms.RadioButton()
        Me.RdBtnAdmins = New System.Windows.Forms.RadioButton()
        Me.BtnDisableUser = New System.Windows.Forms.Button()
        Me.BtnRefresh = New System.Windows.Forms.Button()
        Me.LblRowCount = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.DGVUserManager, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'LblUserManager
        '
        Me.LblUserManager.AutoSize = True
        Me.LblUserManager.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblUserManager.Location = New System.Drawing.Point(16, 32)
        Me.LblUserManager.Name = "LblUserManager"
        Me.LblUserManager.Size = New System.Drawing.Size(131, 26)
        Me.LblUserManager.TabIndex = 0
        Me.LblUserManager.Text = "User Manager"
        '
        'BtnAdminBack
        '
        Me.BtnAdminBack.Location = New System.Drawing.Point(487, 37)
        Me.BtnAdminBack.Name = "BtnAdminBack"
        Me.BtnAdminBack.Size = New System.Drawing.Size(87, 35)
        Me.BtnAdminBack.TabIndex = 12
        Me.BtnAdminBack.Text = "Back"
        Me.BtnAdminBack.UseVisualStyleBackColor = True
        '
        'DGVUserManager
        '
        Me.DGVUserManager.AllowUserToAddRows = False
        Me.DGVUserManager.AllowUserToDeleteRows = False
        Me.DGVUserManager.AllowUserToResizeColumns = False
        Me.DGVUserManager.AllowUserToResizeRows = False
        Me.DGVUserManager.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DGVUserManager.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DGVUserManager.Location = New System.Drawing.Point(24, 165)
        Me.DGVUserManager.MultiSelect = False
        Me.DGVUserManager.Name = "DGVUserManager"
        Me.DGVUserManager.ReadOnly = True
        Me.DGVUserManager.RowHeadersVisible = False
        Me.DGVUserManager.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVUserManager.ShowCellErrors = False
        Me.DGVUserManager.ShowCellToolTips = False
        Me.DGVUserManager.ShowEditingIcon = False
        Me.DGVUserManager.ShowRowErrors = False
        Me.DGVUserManager.Size = New System.Drawing.Size(567, 232)
        Me.DGVUserManager.TabIndex = 15
        '
        'TxtBxSearch
        '
        Me.TxtBxSearch.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxSearch.Location = New System.Drawing.Point(24, 119)
        Me.TxtBxSearch.Name = "TxtBxSearch"
        Me.TxtBxSearch.Size = New System.Drawing.Size(244, 26)
        Me.TxtBxSearch.TabIndex = 8
        '
        'LblSearch
        '
        Me.LblSearch.AutoSize = True
        Me.LblSearch.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblSearch.Location = New System.Drawing.Point(23, 87)
        Me.LblSearch.Name = "LblSearch"
        Me.LblSearch.Size = New System.Drawing.Size(71, 19)
        Me.LblSearch.TabIndex = 6
        Me.LblSearch.Text = "Search by"
        '
        'CBSearchFor
        '
        Me.CBSearchFor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBSearchFor.FormattingEnabled = True
        Me.CBSearchFor.Items.AddRange(New Object() {"User ID", "Email", "Title", "First Name", "Last Name"})
        Me.CBSearchFor.Location = New System.Drawing.Point(96, 87)
        Me.CBSearchFor.MaxDropDownItems = 5
        Me.CBSearchFor.Name = "CBSearchFor"
        Me.CBSearchFor.Size = New System.Drawing.Size(253, 21)
        Me.CBSearchFor.TabIndex = 7
        '
        'BtnNewUser
        '
        Me.BtnNewUser.Location = New System.Drawing.Point(377, 85)
        Me.BtnNewUser.Name = "BtnNewUser"
        Me.BtnNewUser.Size = New System.Drawing.Size(87, 59)
        Me.BtnNewUser.TabIndex = 13
        Me.BtnNewUser.Text = "Create New User"
        Me.BtnNewUser.UseVisualStyleBackColor = True
        '
        'BtnSearch
        '
        Me.BtnSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.BtnSearch.Location = New System.Drawing.Point(274, 119)
        Me.BtnSearch.Name = "BtnSearch"
        Me.BtnSearch.Size = New System.Drawing.Size(75, 26)
        Me.BtnSearch.TabIndex = 9
        Me.BtnSearch.Text = "Search"
        Me.BtnSearch.UseVisualStyleBackColor = True
        '
        'RdBtnAllUsers
        '
        Me.RdBtnAllUsers.AutoSize = True
        Me.RdBtnAllUsers.Checked = True
        Me.RdBtnAllUsers.Location = New System.Drawing.Point(27, 64)
        Me.RdBtnAllUsers.Name = "RdBtnAllUsers"
        Me.RdBtnAllUsers.Size = New System.Drawing.Size(66, 17)
        Me.RdBtnAllUsers.TabIndex = 1
        Me.RdBtnAllUsers.TabStop = True
        Me.RdBtnAllUsers.Text = "All Users"
        Me.RdBtnAllUsers.UseVisualStyleBackColor = True
        '
        'RdBtnClients
        '
        Me.RdBtnClients.AutoSize = True
        Me.RdBtnClients.Location = New System.Drawing.Point(99, 64)
        Me.RdBtnClients.Name = "RdBtnClients"
        Me.RdBtnClients.Size = New System.Drawing.Size(56, 17)
        Me.RdBtnClients.TabIndex = 2
        Me.RdBtnClients.Text = "Clients"
        Me.RdBtnClients.UseVisualStyleBackColor = True
        '
        'RdBtnStaff
        '
        Me.RdBtnStaff.AutoSize = True
        Me.RdBtnStaff.Location = New System.Drawing.Point(161, 64)
        Me.RdBtnStaff.Name = "RdBtnStaff"
        Me.RdBtnStaff.Size = New System.Drawing.Size(47, 17)
        Me.RdBtnStaff.TabIndex = 3
        Me.RdBtnStaff.Text = "Staff"
        Me.RdBtnStaff.UseVisualStyleBackColor = True
        '
        'RdBtnAccLocked
        '
        Me.RdBtnAccLocked.AutoSize = True
        Me.RdBtnAccLocked.Location = New System.Drawing.Point(279, 57)
        Me.RdBtnAccLocked.Name = "RdBtnAccLocked"
        Me.RdBtnAccLocked.Size = New System.Drawing.Size(70, 30)
        Me.RdBtnAccLocked.TabIndex = 5
        Me.RdBtnAccLocked.Text = "Locked" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Accounts"
        Me.RdBtnAccLocked.UseVisualStyleBackColor = True
        '
        'RdBtnAdmins
        '
        Me.RdBtnAdmins.AutoSize = True
        Me.RdBtnAdmins.Location = New System.Drawing.Point(214, 64)
        Me.RdBtnAdmins.Name = "RdBtnAdmins"
        Me.RdBtnAdmins.Size = New System.Drawing.Size(59, 17)
        Me.RdBtnAdmins.TabIndex = 4
        Me.RdBtnAdmins.Text = "Admins"
        Me.RdBtnAdmins.UseVisualStyleBackColor = True
        '
        'BtnDisableUser
        '
        Me.BtnDisableUser.Location = New System.Drawing.Point(487, 85)
        Me.BtnDisableUser.Name = "BtnDisableUser"
        Me.BtnDisableUser.Size = New System.Drawing.Size(87, 59)
        Me.BtnDisableUser.TabIndex = 14
        Me.BtnDisableUser.Text = "Disable User"
        Me.BtnDisableUser.UseVisualStyleBackColor = True
        '
        'BtnRefresh
        '
        Me.BtnRefresh.Location = New System.Drawing.Point(377, 37)
        Me.BtnRefresh.Name = "BtnRefresh"
        Me.BtnRefresh.Size = New System.Drawing.Size(87, 35)
        Me.BtnRefresh.TabIndex = 11
        Me.BtnRefresh.Text = "Refresh"
        Me.BtnRefresh.UseVisualStyleBackColor = True
        '
        'LblRowCount
        '
        Me.LblRowCount.AutoSize = True
        Me.LblRowCount.Location = New System.Drawing.Point(24, 148)
        Me.LblRowCount.Name = "LblRowCount"
        Me.LblRowCount.Size = New System.Drawing.Size(64, 13)
        Me.LblRowCount.TabIndex = 10
        Me.LblRowCount.Text = "RowsFound"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(614, 24)
        Me.MenuStrip1.TabIndex = 16
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(44, 20)
        Me.ToolStripMenuItem1.Text = "Help"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(80, 22)
        Me.ToolStripMenuItem2.Text = "E"
        '
        'UserManager
        '
        Me.AcceptButton = Me.BtnSearch
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 417)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.RdBtnAllUsers)
        Me.Controls.Add(Me.LblRowCount)
        Me.Controls.Add(Me.BtnRefresh)
        Me.Controls.Add(Me.BtnDisableUser)
        Me.Controls.Add(Me.RdBtnAdmins)
        Me.Controls.Add(Me.RdBtnStaff)
        Me.Controls.Add(Me.RdBtnClients)
        Me.Controls.Add(Me.BtnSearch)
        Me.Controls.Add(Me.BtnNewUser)
        Me.Controls.Add(Me.CBSearchFor)
        Me.Controls.Add(Me.TxtBxSearch)
        Me.Controls.Add(Me.LblSearch)
        Me.Controls.Add(Me.DGVUserManager)
        Me.Controls.Add(Me.LblUserManager)
        Me.Controls.Add(Me.BtnAdminBack)
        Me.Controls.Add(Me.RdBtnAccLocked)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "UserManager"
        Me.Text = "UserManager"
        CType(Me.DGVUserManager, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LblUserManager As Label
    Friend WithEvents BtnAdminBack As Button
    Friend WithEvents DGVUserManager As DataGridView
    Friend WithEvents TxtBxSearch As TextBox
    Friend WithEvents LblSearch As Label
    Friend WithEvents CBSearchFor As ComboBox
    Friend WithEvents BtnNewUser As Button
    Friend WithEvents BtnSearch As Button
    Friend WithEvents RdBtnAllUsers As RadioButton
    Friend WithEvents RdBtnClients As RadioButton
    Friend WithEvents RdBtnStaff As RadioButton
    Friend WithEvents RdBtnAccLocked As RadioButton
    Friend WithEvents RdBtnAdmins As RadioButton
    Friend WithEvents BtnDisableUser As Button
    Friend WithEvents BtnRefresh As Button
    Friend WithEvents LblRowCount As Label
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
End Class
