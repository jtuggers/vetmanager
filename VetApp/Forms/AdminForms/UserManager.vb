﻿Imports MySql.Data.MySqlClient
Public Class UserManager

    Dim Staff As New Staff
    Dim Admin As New Admin
    Dim Validation As New Validation
    Dim SearchFor As String = "UserID"
    Dim LoadStatus As Integer = 0

    Private Sub UserManager_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        CBSearchFor.SelectedIndex = 0
        BtnRefresh.PerformClick()
    End Sub

    Private Sub BtnRefresh_Click(sender As Object, e As EventArgs) Handles BtnRefresh.Click
        Dim ds As New DataTable
        ds = Staff.GetUserList()
        DGVUserManager.DataSource = ds
        DGVUserManager.DataMember = ds.TableName

        For Each column As DataGridViewColumn In DGVUserManager.Columns
            column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next

        Dim ResizeColumn As DataGridViewColumn = DGVUserManager.Columns(0)
        ResizeColumn.Width = 55 'USERID

        ResizeColumn = DGVUserManager.Columns(1)
        ResizeColumn.Width = 150 'email

        ResizeColumn = DGVUserManager.Columns(2)
        ResizeColumn.Width = 36 'title

        ResizeColumn = DGVUserManager.Columns(3)
        ResizeColumn.Width = 80 'fname

        ResizeColumn = DGVUserManager.Columns(4)
        ResizeColumn.Width = 80 'lname

        ResizeColumn = DGVUserManager.Columns(5)
        ResizeColumn.Width = 72 'dob

        ResizeColumn = DGVUserManager.Columns(6)
        ResizeColumn.Width = 74 'al
        LblRowCount.Text = "Rows Found: " + DGVUserManager.RowCount.ToString()
        LoadStatus = 1
    End Sub



    Private Sub DGVUserManager_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DGVUserManager.CellMouseDoubleClick
        If (e.ColumnIndex >= 0) And (e.RowIndex >= 0) Then
            Dim selectedRow = DGVUserManager.Rows(e.RowIndex)
            Dim value As String = DGVUserManager.Rows(e.RowIndex).Cells("UserID").Value
            Globals.ModifyID = value
            ModifyUser.Show()
            Me.Close()
        End If


    End Sub


    Private Sub DGVUserManager_KeyDown(sender As Object, e As KeyEventArgs) Handles DGVUserManager.KeyDown
        If e.KeyCode = Keys.Enter Then
            If (DGVUserManager.CurrentCell.RowIndex >= 0) And (DGVUserManager.CurrentCell.ColumnIndex >= 0) Then
                Dim selectedRow = DGVUserManager.SelectedColumns
                Dim value As String = DGVUserManager.Rows(DGVUserManager.CurrentCell.RowIndex).Cells("UserID").Value
                Globals.ModifyID = value
                ModifyUser.Show()
                Me.Close()
            End If
        End If
    End Sub

    Private Sub DGVUserManager_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DGVUserManager.CellClick
        If (DGVUserManager.CurrentCell.RowIndex >= 0) And (DGVUserManager.CurrentCell.ColumnIndex >= 0) Then
            Dim selectedRow = DGVUserManager.SelectedColumns
            Dim value As String = DGVUserManager.Rows(DGVUserManager.CurrentCell.RowIndex).Cells("UserID").Value
            Globals.ModifyID = value
        End If
    End Sub

    Private Sub CBSearchFor_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CBSearchFor.SelectedIndexChanged
        SearchFor = CBSearchFor.Text
        BtnSearch.PerformClick()
    End Sub


    Dim value As String
    Dim result As String

    Private Sub BtnSearch_Click(sender As Object, e As EventArgs) Handles BtnSearch.Click
        If Not LoadStatus = 1 Then
            Return
        End If
        value = TxtBxSearch.Text
        value = value.Trim()
        If value = "" Then
            result = ""

        ElseIf SearchFor = "User ID" Then
            If (Len(value) <= 10) Then
                If Validation.CheckInt(value) = True Then
                    result = "CONVERT(UserID, 'System.String') LIKE '%" + TxtBxSearch.Text + "%'"
                Else
                    result = String.Format("UserID = '0'")
                End If
            Else
                result = String.Format("UserID = '0'")
            End If

        ElseIf SearchFor = "Email" Then
            value = TxtBxSearch.Text
            If Validation.CheckEmail(value) = True Then
                result = String.Format("Email LIKE '%{0}%'", value)
            Else
                result = String.Format("UserID = '0'")
            End If

        ElseIf SearchFor = "Title" Then
            If Validation.CheckString(value) = True Then
                result = String.Format("Title LIKE '%{0}%'", value)
            Else
                result = String.Format("UserID = '0'")
            End If

        ElseIf SearchFor = "First Name" Then
            If Validation.CheckString(value) = True Then
                result = String.Format("FirstName LIKE '%{0}%'", value)
            Else
                result = String.Format("UserID = '0'")
            End If

        ElseIf SearchFor = "Last Name" Then
            If Validation.CheckString(value) = True Then
                result = String.Format("LastName LIKE '%{0}%'", value)
            Else
                result = String.Format("UserID = '0'")
            End If
        End If

        If result = "" Then
            If RdBtnAllUsers.Checked = True Then
                result = ""
            ElseIf RdBtnClients.Checked Then
                result = "AccessLevel = '0'"
            ElseIf RdBtnStaff.Checked Then
                result = "AccessLevel = '1'"
            ElseIf RdBtnAdmins.Checked Then
                result = "AccessLevel = '2'"
            ElseIf RdBtnAccLocked.Checked Then
                result = "AccessLevel = '3'"
            End If
        Else
            If RdBtnAllUsers.Checked = True Then
                result = result + ""
            ElseIf RdBtnClients.Checked Then
                result = result + " AND AccessLevel = '0'"
            ElseIf RdBtnStaff.Checked Then
                result = result + " AND AccessLevel = '1'"
            ElseIf RdBtnAdmins.Checked Then
                result = result + " AND AccessLevel = '2'"
            ElseIf RdBtnAccLocked.Checked Then
                result = result + " AND AccessLevel = '3'"
            End If
        End If
        DGVUserManager.DataSource.DefaultView.RowFilter = result
        LblRowCount.Text = "Rows Found: " + DGVUserManager.RowCount.ToString()
    End Sub

    Private Sub TxtBxSearch_TextChanged(sender As Object, e As EventArgs) Handles TxtBxSearch.TextChanged
        BtnSearch.PerformClick()

    End Sub


    Private Sub BtnAdminBack_Click(sender As Object, e As EventArgs) Handles BtnAdminBack.Click
        AdminHome.Show()
        Me.Close()
    End Sub


    Private Sub BtnNewUser_Click(sender As Object, e As EventArgs) Handles BtnNewUser.Click
        AdminNewUser.Show()
        Me.Close()
    End Sub

    Private Sub MenuStrip1_ItemClicked(sender As Object, e As ToolStripItemClickedEventArgs)

    End Sub

    Private Sub RdBtnAllUsers_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnAllUsers.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnClients_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnClients.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnStaff_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnStaff.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnAccLocked_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnAccLocked.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub RdBtnAdmins_CheckedChanged(sender As Object, e As EventArgs) Handles RdBtnAdmins.CheckedChanged
        BtnSearch.PerformClick()
    End Sub

    Private Sub BtnSignOut_Click(sender As Object, e As EventArgs)
        Login.Show()
        Me.Close()
    End Sub

    Private Sub BtnDisableUser_Click(sender As Object, e As EventArgs) Handles BtnDisableUser.Click
        Try
            Admin.DisableUser()
        Catch
            MessageBox.Show("Account could not be disabled at this time.")
        End Try

    End Sub


End Class