﻿Public Class ViewAnimals

    Dim User As New User
    Private Sub BtnClose_Click(sender As Object, e As EventArgs) Handles BtnClose.Click
        Me.Close()
    End Sub


    Private Sub Comments_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim Details As List(Of String) = User.GetAnimals()
        For Each Item In Details
            ListBxHistory.Items.Add(Item)
        Next
    End Sub

    Private Sub ListBxHistory_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBxHistory.SelectedIndexChanged

        Dim Selected As String = ListBxHistory.GetItemText(ListBxHistory.SelectedItem)

        Dim Details() As String = User.GetCommentDetails(Selected)

        TxtBxCommentID.Text = Details(0)
        TxtBxComment.Text = Details(1)
        TxtBxAddedBy.Text = User.GetFullname(Details(2))
    End Sub

    Private Sub BtnAddComment_Click(sender As Object, e As EventArgs) Handles BtnAddComment.Click
        AddComment.ShowDialog()
    End Sub

End Class