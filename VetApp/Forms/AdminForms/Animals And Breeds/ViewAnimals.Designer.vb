﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ViewAnimals
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtBxAddedBy = New System.Windows.Forms.TextBox()
        Me.LblAddedUser = New System.Windows.Forms.Label()
        Me.TxtBxCommentID = New System.Windows.Forms.TextBox()
        Me.LblWeightID = New System.Windows.Forms.Label()
        Me.BtnAddComment = New System.Windows.Forms.Button()
        Me.LblComments = New System.Windows.Forms.Label()
        Me.BtnClose = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListBxHistory = New System.Windows.Forms.ListBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TxtBxAddedBy
        '
        Me.TxtBxAddedBy.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxAddedBy.Location = New System.Drawing.Point(251, 119)
        Me.TxtBxAddedBy.Name = "TxtBxAddedBy"
        Me.TxtBxAddedBy.ReadOnly = True
        Me.TxtBxAddedBy.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxAddedBy.TabIndex = 175
        '
        'LblAddedUser
        '
        Me.LblAddedUser.AutoSize = True
        Me.LblAddedUser.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAddedUser.Location = New System.Drawing.Point(197, 122)
        Me.LblAddedUser.Name = "LblAddedUser"
        Me.LblAddedUser.Size = New System.Drawing.Size(38, 13)
        Me.LblAddedUser.TabIndex = 174
        Me.LblAddedUser.Text = "Animal"
        '
        'TxtBxCommentID
        '
        Me.TxtBxCommentID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtBxCommentID.Location = New System.Drawing.Point(251, 81)
        Me.TxtBxCommentID.Name = "TxtBxCommentID"
        Me.TxtBxCommentID.ReadOnly = True
        Me.TxtBxCommentID.Size = New System.Drawing.Size(106, 20)
        Me.TxtBxCommentID.TabIndex = 173
        '
        'LblWeightID
        '
        Me.LblWeightID.AutoSize = True
        Me.LblWeightID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblWeightID.Location = New System.Drawing.Point(197, 84)
        Me.LblWeightID.Name = "LblWeightID"
        Me.LblWeightID.Size = New System.Drawing.Size(52, 13)
        Me.LblWeightID.TabIndex = 172
        Me.LblWeightID.Text = "Animal ID"
        '
        'BtnAddComment
        '
        Me.BtnAddComment.Location = New System.Drawing.Point(200, 155)
        Me.BtnAddComment.Name = "BtnAddComment"
        Me.BtnAddComment.Size = New System.Drawing.Size(78, 39)
        Me.BtnAddComment.TabIndex = 171
        Me.BtnAddComment.Text = "Add New Comment"
        Me.BtnAddComment.UseVisualStyleBackColor = True
        '
        'LblComments
        '
        Me.LblComments.AutoSize = True
        Me.LblComments.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblComments.Location = New System.Drawing.Point(28, 28)
        Me.LblComments.Name = "LblComments"
        Me.LblComments.Size = New System.Drawing.Size(104, 26)
        Me.LblComments.TabIndex = 170
        Me.LblComments.Text = "Comments"
        '
        'BtnClose
        '
        Me.BtnClose.Location = New System.Drawing.Point(279, 28)
        Me.BtnClose.Name = "BtnClose"
        Me.BtnClose.Size = New System.Drawing.Size(78, 35)
        Me.BtnClose.TabIndex = 169
        Me.BtnClose.Text = "Close"
        Me.BtnClose.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(398, 24)
        Me.MenuStrip1.TabIndex = 168
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.EToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'EToolStripMenuItem
        '
        Me.EToolStripMenuItem.Name = "EToolStripMenuItem"
        Me.EToolStripMenuItem.Size = New System.Drawing.Size(80, 22)
        Me.EToolStripMenuItem.Text = "E"
        '
        'ListBxHistory
        '
        Me.ListBxHistory.FormattingEnabled = True
        Me.ListBxHistory.Location = New System.Drawing.Point(16, 74)
        Me.ListBxHistory.Name = "ListBxHistory"
        Me.ListBxHistory.ScrollAlwaysVisible = True
        Me.ListBxHistory.Size = New System.Drawing.Size(173, 147)
        Me.ListBxHistory.TabIndex = 167
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(279, 155)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(78, 39)
        Me.Button1.TabIndex = 176
        Me.Button1.Text = "Add New Comment"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(200, 28)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(78, 35)
        Me.Button2.TabIndex = 177
        Me.Button2.Text = "Change Name"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'ViewAnimals
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(398, 241)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TxtBxAddedBy)
        Me.Controls.Add(Me.LblAddedUser)
        Me.Controls.Add(Me.TxtBxCommentID)
        Me.Controls.Add(Me.LblWeightID)
        Me.Controls.Add(Me.BtnAddComment)
        Me.Controls.Add(Me.LblComments)
        Me.Controls.Add(Me.BtnClose)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.ListBxHistory)
        Me.Name = "ViewAnimals"
        Me.Text = "ViewAnimals"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TxtBxAddedBy As TextBox
    Friend WithEvents LblAddedUser As Label
    Friend WithEvents TxtBxCommentID As TextBox
    Friend WithEvents LblWeightID As Label
    Friend WithEvents BtnAddComment As Button
    Friend WithEvents LblComments As Label
    Friend WithEvents BtnClose As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListBxHistory As ListBox
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class
