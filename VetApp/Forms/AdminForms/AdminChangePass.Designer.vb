﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AdminChangePass
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BtnSubmit = New System.Windows.Forms.Button()
        Me.TxtBxConfNewPass = New System.Windows.Forms.TextBox()
        Me.TxtBxNewPass = New System.Windows.Forms.TextBox()
        Me.LblConfNewPass = New System.Windows.Forms.Label()
        Me.LblNewPass = New System.Windows.Forms.Label()
        Me.LblModifyUser = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'BtnSubmit
        '
        Me.BtnSubmit.Location = New System.Drawing.Point(102, 160)
        Me.BtnSubmit.Name = "BtnSubmit"
        Me.BtnSubmit.Size = New System.Drawing.Size(189, 23)
        Me.BtnSubmit.TabIndex = 98
        Me.BtnSubmit.Text = "Submit"
        Me.BtnSubmit.UseVisualStyleBackColor = True
        '
        'TxtBxConfNewPass
        '
        Me.TxtBxConfNewPass.Location = New System.Drawing.Point(102, 118)
        Me.TxtBxConfNewPass.Name = "TxtBxConfNewPass"
        Me.TxtBxConfNewPass.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxConfNewPass.TabIndex = 97
        Me.TxtBxConfNewPass.UseSystemPasswordChar = True
        '
        'TxtBxNewPass
        '
        Me.TxtBxNewPass.Location = New System.Drawing.Point(102, 76)
        Me.TxtBxNewPass.Name = "TxtBxNewPass"
        Me.TxtBxNewPass.Size = New System.Drawing.Size(189, 20)
        Me.TxtBxNewPass.TabIndex = 96
        '
        'LblConfNewPass
        '
        Me.LblConfNewPass.AutoSize = True
        Me.LblConfNewPass.Location = New System.Drawing.Point(18, 121)
        Me.LblConfNewPass.Name = "LblConfNewPass"
        Me.LblConfNewPass.Size = New System.Drawing.Size(70, 26)
        Me.LblConfNewPass.TabIndex = 94
        Me.LblConfNewPass.Text = "Confirm New " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Password"
        '
        'LblNewPass
        '
        Me.LblNewPass.AutoSize = True
        Me.LblNewPass.Location = New System.Drawing.Point(18, 79)
        Me.LblNewPass.Name = "LblNewPass"
        Me.LblNewPass.Size = New System.Drawing.Size(78, 13)
        Me.LblNewPass.TabIndex = 93
        Me.LblNewPass.Text = "New Password"
        '
        'LblModifyUser
        '
        Me.LblModifyUser.AutoSize = True
        Me.LblModifyUser.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblModifyUser.Location = New System.Drawing.Point(16, 31)
        Me.LblModifyUser.Name = "LblModifyUser"
        Me.LblModifyUser.Size = New System.Drawing.Size(159, 26)
        Me.LblModifyUser.TabIndex = 91
        Me.LblModifyUser.Text = "Modify Password"
        '
        'AdminChangePass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(320, 202)
        Me.Controls.Add(Me.BtnSubmit)
        Me.Controls.Add(Me.TxtBxConfNewPass)
        Me.Controls.Add(Me.TxtBxNewPass)
        Me.Controls.Add(Me.LblConfNewPass)
        Me.Controls.Add(Me.LblNewPass)
        Me.Controls.Add(Me.LblModifyUser)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "AdminChangePass"
        Me.Text = "AdminChangePass"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents BtnSubmit As Button
    Friend WithEvents TxtBxConfNewPass As TextBox
    Friend WithEvents TxtBxNewPass As TextBox
    Friend WithEvents LblConfNewPass As Label
    Friend WithEvents LblNewPass As Label
    Friend WithEvents LblModifyUser As Label
End Class
