﻿Public Class DBConnSettings
    Dim User As New User
    Private Sub DBConnSettings_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BtnDBSubmit.Enabled = False
        TxtDBHostname.Text = My.Settings.DBHostname
        TxtDBUsername.Text = My.Settings.DBUsername
        TxtDBPassword.Text = My.Settings.DBPassword
        TxtDBDatabase.Text = My.Settings.DBDatabase
    End Sub

    Private Sub BtnDBSubmit_Click(sender As Object, e As EventArgs) Handles BtnDBSubmit.Click
        My.Settings.DBHostname = TxtDBHostname.Text
        My.Settings.DBUsername = TxtDBUsername.Text
        My.Settings.DBPassword = TxtDBPassword.Text
        My.Settings.DBDatabase = TxtDBDatabase.Text
        MessageBox.Show("Configuration Saved.", "Success!")
        Me.Close()

    End Sub

    Private Sub BtnDBTestConection_Click(sender As Object, e As EventArgs) Handles BtnDBTestConection.Click
        Try
            User.DBConnTest(TxtDBHostname.Text, TxtDBUsername.Text, TxtDBPassword.Text, TxtDBDatabase.Text)
            BtnDBSubmit.Enabled = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error")
        End Try
    End Sub

End Class