﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Encrypter
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TxtBxEmail = New System.Windows.Forms.TextBox()
        Me.TxtBxGenHash = New System.Windows.Forms.TextBox()
        Me.TxtBxGenSalt = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TxtBxPassword = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BtnGenerate = New System.Windows.Forms.Button()
        Me.BtnEncrypt = New System.Windows.Forms.Button()
        Me.TxtBxString = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'TxtBxEmail
        '
        Me.TxtBxEmail.Location = New System.Drawing.Point(69, 29)
        Me.TxtBxEmail.Name = "TxtBxEmail"
        Me.TxtBxEmail.Size = New System.Drawing.Size(214, 20)
        Me.TxtBxEmail.TabIndex = 0
        '
        'TxtBxGenHash
        '
        Me.TxtBxGenHash.Location = New System.Drawing.Point(69, 367)
        Me.TxtBxGenHash.Multiline = True
        Me.TxtBxGenHash.Name = "TxtBxGenHash"
        Me.TxtBxGenHash.ReadOnly = True
        Me.TxtBxGenHash.Size = New System.Drawing.Size(214, 62)
        Me.TxtBxGenHash.TabIndex = 5
        Me.TxtBxGenHash.TabStop = False
        '
        'TxtBxGenSalt
        '
        Me.TxtBxGenSalt.Location = New System.Drawing.Point(69, 187)
        Me.TxtBxGenSalt.Multiline = True
        Me.TxtBxGenSalt.Name = "TxtBxGenSalt"
        Me.TxtBxGenSalt.Size = New System.Drawing.Size(214, 155)
        Me.TxtBxGenSalt.TabIndex = 4
        Me.TxtBxGenSalt.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(31, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Email"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Password"
        '
        'TxtBxPassword
        '
        Me.TxtBxPassword.Location = New System.Drawing.Point(69, 75)
        Me.TxtBxPassword.Name = "TxtBxPassword"
        Me.TxtBxPassword.Size = New System.Drawing.Size(214, 20)
        Me.TxtBxPassword.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 241)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 26)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Generated" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Salt"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 385)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 26)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Generated" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "MD5 Hash" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'BtnGenerate
        '
        Me.BtnGenerate.Location = New System.Drawing.Point(69, 118)
        Me.BtnGenerate.Name = "BtnGenerate"
        Me.BtnGenerate.Size = New System.Drawing.Size(105, 43)
        Me.BtnGenerate.TabIndex = 3
        Me.BtnGenerate.Text = "Generate"
        Me.BtnGenerate.UseVisualStyleBackColor = True
        '
        'BtnEncrypt
        '
        Me.BtnEncrypt.Location = New System.Drawing.Point(180, 118)
        Me.BtnEncrypt.Name = "BtnEncrypt"
        Me.BtnEncrypt.Size = New System.Drawing.Size(105, 43)
        Me.BtnEncrypt.TabIndex = 10
        Me.BtnEncrypt.Text = "Encrypt"
        Me.BtnEncrypt.UseVisualStyleBackColor = True
        '
        'TxtBxString
        '
        Me.TxtBxString.Location = New System.Drawing.Point(301, 29)
        Me.TxtBxString.Multiline = True
        Me.TxtBxString.Name = "TxtBxString"
        Me.TxtBxString.Size = New System.Drawing.Size(276, 400)
        Me.TxtBxString.TabIndex = 11
        Me.TxtBxString.TabStop = False
        '
        'Encrypter
        '
        Me.AcceptButton = Me.BtnGenerate
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(604, 449)
        Me.Controls.Add(Me.TxtBxString)
        Me.Controls.Add(Me.BtnEncrypt)
        Me.Controls.Add(Me.BtnGenerate)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TxtBxPassword)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TxtBxGenSalt)
        Me.Controls.Add(Me.TxtBxGenHash)
        Me.Controls.Add(Me.TxtBxEmail)
        Me.Name = "Encrypter"
        Me.Text = "Password Encrypter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TxtBxEmail As TextBox
    Friend WithEvents TxtBxGenHash As TextBox
    Friend WithEvents TxtBxGenSalt As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TxtBxPassword As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents BtnGenerate As Button
    Friend WithEvents BtnEncrypt As Button
    Friend WithEvents TxtBxString As TextBox
End Class
