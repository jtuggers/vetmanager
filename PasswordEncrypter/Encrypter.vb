﻿Imports System.Security.Cryptography 'Imports the Cryptography libraries
Imports System.Text 'Imports the system text libraries
Public Class Encrypter
    Private Sub BtnGenerate_Click(sender As Object, e As EventArgs) Handles BtnGenerate.Click 'Wraps the code which is ran whenever the button BtnGenerate is clicked


        Dim AvailableChars As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()_+=][}{<>" 'Defines AvailableChars as a string, storing the given value
        Dim salt As String = Nothing 'Defines a variable "salt" as string, storing no text 
        Dim rand As New Random ' defines a new instance of the system class "Random"


        For i As Integer = 1 To 255 'For Loop, repeating 255 times. 255 = Length of the salt
            Dim x As Integer = rand.Next(0, AvailableChars.Length - 1) ' defines x as an integer, picking a random number between 0 and 80 (the number of characters in AvailableChars is 81)
            salt &= (AvailableChars.Substring(x, 1)) ' links the contents of the variable "salt" to a substring created from the string AvailableChars, using the index x, continuing for 1 character
        Next 'restarts the loop by returning to "For"

        Dim FullSalt As String = salt.ToString 'Defines the variable "FullSalt" as a string, storing the contents of "salt" converted to a string
        TxtBxGenSalt.Text = FullSalt 'Sets the textbox "TxtBxGenSalt" to the contents of the variable "FullSalt"



    End Sub

    Private Sub BtnEncrypt_Click(sender As Object, e As EventArgs) Handles BtnEncrypt.Click
        Dim FullSalt As String = TxtBxGenSalt.Text
        Dim Email As String = TxtBxEmail.Text 'Defines a veriable "Email" as a string, and stores the contents of of the text box TxtBxEmail
        Dim Password As String = TxtBxPassword.Text 'Defines a variable "Password" as a string, storing the contents of the text box TxtBxPassword 

        Dim combinedString As String 'Defines the variable "combinedString" as a string

        combinedString = (FullSalt + Email.ToLower + FullSalt + Password + FullSalt) 'Combines the strings "Fullsalt", "Email" and "Password" to form a longer string, stored in the variable "combinedString"
        TxtBxString.Text = combinedString
        Dim sha As New SHA256CryptoServiceProvider 'Defines a new instance of the SHA256 Libraries
        Dim Hash() As Byte 'Defines the array "hash" with datatype Byte
        Dim bytes() As Byte = ASCIIEncoding.ASCII.GetBytes(combinedString) 'Creates byte array from combinedString

        Hash = sha.ComputeHash(bytes) 'Generates a hash using the byte array "Bytes", storing it in the array "Hash"

        Dim FinalHash As String = Nothing 'Defines a variable "finalHash" storing nothing to prevent an error below
        For Each bt As Byte In Hash 'For loop for each value in the Hash array
            FinalHash &= bt.ToString("x2") 'Converts BT to a string, with ("x2") printing the byte in hexadecimal format.
        Next 'Restarts the loop

        TxtBxGenHash.Text = FinalHash 'Sets the textbox "TxtBxGenHash" to the contents of the variable "FullSalt"
    End Sub


End Class
